/*jslint node: true, mocha:true*/
"use strict";
const chai = require('chai')
    , expect = chai.expect
    , request = require('supertest')
    , seedTestData = require('./fakeEntries')
    , mongoose = require('mongoose')
    , path = require('path')
    ;


describe('PIPE BUILDER TESTS', () => {
    var stateManager = require('../server/stateManager');

    let app = require('../server');
    let Route;

    before((done) => {
        stateManager.clean();
        app.on('started', () => {
            Route = mongoose.model('Route');
            done();
        });
        seedTestData()
            .then(() => {
                app.start(path.resolve(__dirname, 'testConfig.json'));
            });
    });

    after((done) => {
        Route.remove({}, () => {
            app.close(() => done());
        });
    });

    it('should init services, plugins and build state object', () => {
        expect(stateManager.services.length).to.be.equal(0);
        expect(stateManager.systemServices.size).to.be.equal(2);
        expect(stateManager.servicesStore.size).to.be.equal(0);
        expect(stateManager.plugins.length).to.be.equal(8);
    });
});

