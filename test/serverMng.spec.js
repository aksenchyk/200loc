/*jslint node: true, mocha:true*/
"use strict";
const chai = require('chai')
    , expect = chai.expect
    , request = require('supertest')
    , express = require('express')
    , http = require('http')
    , mongoose = require('mongoose')
    , path = require('path')
    , seedTestData = require('./fakeEntries')
    ;

describe('SERVER UPDATE & RESTART TESTS', () => {
    var Route, GatewayUser;
    let testApp = express();
    let app = require('../server');
    testApp.use((req, res) => {
        res.json({ body: req.body, headers: req.headers, originalUrl: req.originalUrl, path: req.path });
    });
    const server = http.createServer(testApp);

    before((done) => {
        app.on('started', () => {
            Route = mongoose.model('Route');
            GatewayUser = mongoose.model('GatewayUser');
            done();
        });
        seedTestData()
            .then(() => {
                app.start(path.resolve(__dirname, 'testConfig.json'));
            });
    });

    after((done) => {
        Route.remove({}, () => {
            GatewayUser.remove({}, () => {
                app.close(() => {
                    server.close(done);
                });
            });
        });
    });

    before((done) => {
        server.listen(33333, (err) => done(err));
    });

    xit('should fetch 503 if server is updating, but request is coming', (done) => {
        request(app)
            .get('/testnotreturn/')
            .set('Accept', 'application/json')
            .expect(200)
            .end((err) => {
                expect(err).to.be.null;
                app.on('updating', () => {
                    request(app)
                        .get('/testnotreturn/')
                        .set('Accept', 'application/json')
                        .expect(503)
                        .end(done);
                });
                app.update();
            });
    });

    it('should return 200 when [GET] healthcheck', (done) => {
        request(app)
            .get('/healthcheck')
            .expect(200)
            .end(done);
    });

    it('should return 200 when [HEAD] healthcheck', (done) => {
        request(app)
            .head('/healthcheck')
            .expect(200)
            .end(done);
    });
});
