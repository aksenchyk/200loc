"use strict";
const mongoose = require('mongoose');

module.exports = function seedTestData() {
    let RouteConfig = mongoose.model('Route');
    return new Promise((resolve, reject) => {
        RouteConfig.create([
            {
                name: 'fakeRoute1',
                entry: '/pluginwithouthandler',
                plugins: [
                    {
                        name: "pluginwithouthandler",
                        settings: {}
                    }
                ],
                rules: [{
                    "path": "/*",
                    "plugins": [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'fakeRoute2',
                entry: '/test',
                plugins: [
                    {
                        name: "errPlugin",
                        settings: {
                            throwError: true,
                            errorCode: 404
                        }
                    }
                ],
                rules: [{
                    "path": "/*",
                    "plugins": [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'route1',
                entry: '/testnotreturn',
                plugins: [
                    {
                        name: "notReturnAnythingPlugin",
                        settings: {
                        }
                    }
                ],
                rules: [{
                    "path": "/*",
                    "plugins": [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'route2',
                entry: '/test2',
                plugins: [
                    {
                        name: "errPlugin",
                        settings: {
                            throwError: false,
                        }
                    },
                    {
                        name: "errPlugin",
                        settings: {
                            throwError: true,
                            errorCode: 500
                        }
                    }
                ],
                rules: [{
                    "path": "/*",
                    "plugins": [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'route3',
                entry: '/test3',
                plugins: [
                    {
                        name: "errPlugin",
                        settings: {
                            throwError: false,
                        }
                    }
                ],
                rules: [{
                    path: "/*",
                    plugins: [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'route4',
                entry: '/test4',
                plugins: [
                    {
                        name: "setDynamicPlugin",
                        settings: {

                        }
                    },
                    {
                        name: "simplePlugin",
                        settings: {
                            dynamic: "${dynamic}",
                        }
                    }
                ],
                rules: [{
                    "path": "/*",
                    "plugins": [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'route5',
                entry: '/test5',
                plugins: [
                    {
                        name: "simplePlugin",
                        settings: {
                            env: "env{SOME_HOST}",
                        }
                    }
                ],
                rules: [{
                    path: "/*",
                    plugins: [],
                    methods: ['GET', "POST"],
                }]
            },
            {
                name: 'route8',
                entry: '/test8',
                plugins: [
                    {
                        name: "setDynamicPlugin",
                        settings: {}
                    }
                ],
                rules: [
                    {
                        "path": "/some/path/*",
                        "plugins": [
                            {
                                name: "notReturnAnythingPlugin",
                                settings: {}
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    },
                    {
                        "path": "/some/path/simple",
                        "plugins": [
                            {
                                name: "notReturnAnythingPlugin",
                                settings: {}
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    },
                    {
                        "path": "/proxy/some/path/*",
                        "plugins": [
                            {
                                name: "setDynamicPlugin",
                                settings: {}
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    }
                ],
                proxy: {
                    target: 'http://localhost:33333/',
                    active: true
                }
            },
            {
                name: 'route9',
                entry: '/test9',
                plugins: [],
                rules: [
                    {
                        "path": "/proxy/some/path/*",
                        "plugins": [
                            {
                                name: "setDynamicPlugin",
                                settings: {}
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    }
                ],
                proxy: {
                    target: 'wrong://localhost:33333/',
                    active: true
                }
            },
            {
                name: 'route10',
                entry: '/test10',
                plugins: [],
                rules: [
                    {
                        "path": "/proxy/some/path/*",
                        "plugins": [
                            {
                                name: "setDynamicPlugin",
                                settings: {}
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    }
                ],
                proxy: {
                    target: 'http://localhost:33333/',
                    active: false
                }
            },
            {
                name: 'routerx',
                entry: '/testrx',
                plugins: [],
                rules: [
                    {
                        "path": "(test1|test2)",
                        "plugins": [],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    }
                ],
                proxy: {
                    target: 'http://localhost:33333/',
                    active: true
                }
            },
            {
                name: 'route11',
                entry: '/test11',
                plugins: [],
                rules: [
                    {
                        "path": "/proxy/some/path/*",
                        "plugins": [
                            {
                                name: "setDynamicPlugin",
                                settings: {}
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    }
                ],
                proxy: {
                    target: 'http://localhost:44444/',
                    active: true
                },
                health: {
                    healthCheck: [
                        {
                            "target": "http://localhost:44444",
                            "withPath": "/healthcheck",
                            "method": "HEAD",
                        }
                    ],
                    active: true,
                    interval: 100
                }
            },
            {
                name: 'route12',
                entry: '/test12',
                plugins: [
                    {
                        name: "pluginWrongContextUsage",
                        settings: {
                        }
                    }
                ],
                rules: [{
                    path: "/*",
                    plugins: [],
                    methods: ['GET', 'POST', "PUT", "DELETE"],
                }]
            },
            {
                name: 'route13',
                entry: '/test13',
                plugins: [],
                rules: [
                    {
                        "path": "/proxy/some/path/*",
                        "plugins": [],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                    }
                ],
                proxy: {
                    target: 'http://localhost:14785/',
                    active: true
                }
            },


            {
                name: 'route14',
                entry: '/test14',
                plugins: [],
                health: 'disabled',
                // test to rule to check policy working with query parameters
                rules: [
                    {
                        "path": "/test_1/*",
                        "plugins": [],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                        policy: '[{"satisfy": ["test"],"proxy": { "target": "http://localhost:1515"}}]',
                        conditions: [{ name: "test", source: "Query", key: "test", value: "test", operation: "eq" }],
                        proxy: {
                            target: 'http://localhost:33333/',
                            active: true
                        }
                    },
                    // test to rule to check policy working with header parameters
                    {
                        "path": "/test_2/*",
                        "plugins": [],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                        policy: '[{"satisfy": ["testHeader"],"proxy": { "target": "http://localhost:1515"}}]',
                        conditions: [{ name: "testHeader", source: "Header", key: "test", value: "test", operation: "eq" }],
                        proxy: {
                            target: 'http://localhost:33333/',
                            active: true
                        }
                    },
                    // test to rule to check policy working with context
                    {
                        "path": "/test_3/*",
                        "plugins": [
                            {
                                name: "setDynamicPlugin",
                                settings: {
                                }
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                        policy: '[{"satisfy": ["testHeader"],"proxy": { "target": "http://localhost:1515"}}]',
                        conditions: [{ name: "testHeader", source: "Context", key: "test", value: "value", operation: "eq" }],
                        proxy: {
                            target: 'http://localhost:33333/',
                            active: true
                        }
                    },
                    // test to rule to check policy handles use case when alternative server is not responding
                    {
                        "path": "/test_4/*",
                        "plugins": [
                            {
                                name: "setDynamicPlugin",
                                settings: {
                                }
                            }
                        ],
                        methods: ['GET', 'POST', "PUT", "DELETE"],
                        policy: '[{"satisfy": ["testHeader"],"proxy": { "target": "http://localhost:1616"}}]',
                        conditions: [{ name: "testHeader", source: "Context", key: "test", value: "value", operation: "eq" }],
                        proxy: {
                            target: 'http://localhost:33333/',
                            active: true
                        }
                    }
                ],
                proxy: {
                    target: 'http://localhost:44444/',
                    active: true
                }
            },
        ], (err, configs) => {
            if (err) {
                reject(err);
            }
            resolve(configs);
        });
    });
};
