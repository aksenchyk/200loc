/*jslint node: true, mocha:true*/
"use strict";
const request = require('supertest')
    , express = require('express')
    , http = require('http')
    , seedTestData = require('./fakeEntries')
    , mongoose = require('mongoose')
    , path = require('path')
    ;

xdescribe('HEALTHCHECKING TESTS', () => {
    let Route, timer, GatewayUser;
    let testApp = express();
    let health = 200;
    let app = require('../server');
    testApp.use('/proxy/some/path/any', (req, res) => {
        res.json({ body: req.body, headers: req.headers, originalUrl: req.originalUrl, path: req.path });
    });
    testApp.head('/healthcheck', (req, res) => {
        return res.status(health).end();
    });
    const server = http.createServer(testApp);


    before((done) => {
        app.on('started', () => {
            Route = mongoose.model('Route');
            GatewayUser = mongoose.model('GatewayUser');
            done();
        });
        seedTestData()
            .then(() => {
                app.start(path.resolve(__dirname, 'testConfig.json'));
            });
    });

    after((done) => {
        Route.remove({}, () => {
            GatewayUser.remove({}, () => {
                app.close(() => {
                    server.close(done);
                });
            });
        });
    });

    before((done) => server.listen(44444, (err) => done(err)));

    afterEach(() => {
        if (timer) {
            clearTimeout(timer);
        }
    });

    it('should start healthcheck for remote service', (done) => {
        health = 200;
        timer = setTimeout(() => {
            request(app)
                .get('/test11/proxy/some/path/any')
                .expect(200)
                .end(done);
        }, 200);
    });

    it('should open circuit breaker if remote service is not healthy', (done) => {
        health = 404;
        timer = setTimeout(() => {
            request(app)
                .get('/test11/proxy/some/path/any')
                .expect(503)
                .end(done);
        }, 400); // let cycle pass
    });
});
