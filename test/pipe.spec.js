/*jslint node: true, mocha:true*/
"use strict";
const chai = require('chai'),
    expect = chai.expect,
    pipeFactory = require('../server/gateway/pipe');

describe('PIPE TESTS', () => {

    describe('ROUTE PIPE OBJECT TESTS', () => {
        let pipe;
        beforeEach(() => {
            pipe = pipeFactory();
            pipe.insert(0, {
                some: 'value'
            });
        });

        it('data should be inserted', () => {
            expect(pipe.getItem("some", 0)).to.be.equal("value");
        });
    });

    describe('PIPE BUILD PROCESS TESTS', () => {
        let pipe, config1 = {
            some: 'value'
        }, config2 = {
            some2: 'value2'
        }, config3 = {
            some3: 'value3'
        };
        beforeEach(() => {
            pipe = pipeFactory();
            pipe.insert(0, config1);
            pipe.insert(1, config2);
            pipe.insert(2, config3);
        });

        it('should build pipe from plugins declaration', () => {
            expect(pipe._map.size).to.be.equal(3);
        });

        it('should contain prefixed keys-value pairs', () => {
            expect([...pipe._map.keys()])
                .to.deep.equal(['plugin_:0', 'plugin_:1', 'plugin_:2']);
            expect([...pipe._map.values()])
                .to.deep.equal([config1, config2, config3]);
        });

        it('should clean by key', () => {
            pipe.clean('plugin_:0');
            expect(pipe._map.size).to.be.equal(2);
        });

        it('should clean all', () => {
            pipe.clean();
            expect(pipe._map.size).to.be.equal(0);
        });
    });
});