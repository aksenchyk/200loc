/*jslint node: true, mocha:true*/
"use strict";
const chai = require('chai')
    , sinon = require('sinon')
    , expect = chai.expect
    , path = require('path')
    ;

describe('SERVER COMPONENTS LOADER TESTS', () => {
    let app = require('../server');
    let sandbox;

    beforeEach((done) => {
        sandbox = sinon.sandbox.create({ useFakeTimers: true });
        done();
    });

    afterEach((done) => {
        app.close(() => {
            done();
        });
    });
    afterEach(() => {
        sandbox.restore();
    });

    it('should throw an error if wrong plugin path in configuration file', (done) => {
        app.start(path.resolve(__dirname, 'badConfig_plugins.json'))
            .catch((err) => {
                expect(err.startsWith('No plugin found')).to.be.true;
                done();
            });
    });
    it('should throw if now config file set', (done) => {
        app.start(path.resolve(__dirname, 'nosuchFile.json'))
            .catch((err) => {
                expect(err).to.be.ok;
                done();
            });
    });
    it('should throw if one of required fields not set', (done) => {
        app.start(path.resolve(__dirname, 'badConfig_fields.json'))
            .catch((err) => {
                expect(err).to.be.ok;
                done();
            });
    });
});
