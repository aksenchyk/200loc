/*jslint node: true, mocha:true*/
"use strict";
const chai = require('chai')
    , expect = chai.expect
    , request = require('supertest')
    , express = require('express')
    , http = require('http')
    , seedTestData = require('./fakeEntries')
    , mongoose = require('mongoose')
    , path = require('path')
    ;

describe('GATEWAY TESTS', () => {
    let Route, GatewayUser;
    let testApp = express();
    let testAppAlternative = express();
    require('./support/env');
    let app = require('../server');
    testApp.use((req, res) => {
        res.json({ body: req.body, headers: req.headers, originalUrl: req.originalUrl, path: req.path });
    });
    testAppAlternative.use((req, res) => {
        res.status(201).end();
    });
    const server = http.createServer(testApp);
    before((done) => {
        server.listen(33333, (err) => done(err));
    });

    const testServerAlternative = http.createServer(testAppAlternative);
    before((done) => {
        testServerAlternative.listen(1515, (err) => done(err));
    });

    before((done) => {
        app.on('started', () => {
            Route = mongoose.model('Route');
            GatewayUser = mongoose.model('GatewayUser');
            done();
        });
        seedTestData()
            .then(() => {
                app.start(path.resolve(__dirname, 'testConfig.json'));
            });
    });

    after((done) => {
        Route.remove({}, () => {
            GatewayUser.remove({}, () => {
                app.close(() => {
                    server.close(done);
                });
            });
        });
    });

    it('should create default user', (done) => {
        GatewayUser.find()
            .then((users) => {
                expect(users.length).to.be.equal(1);
                done();
            })
            .catch(done);
    });

    it("should return 404 when wrong url", (done) => {
        request(app)
            .get('/no_route/like_this')
            .expect(404)
            .end(done);
    });

    it('should return error when plugin returns fail', (done) => {
        request(app)
            .get('/test')
            .set('Accept', 'application/json')
            .expect(404)
            .end(done);
    });

    it('should return 500 if plugin error', (done) => {
        request(app)
            .get('/test2/')
            .set('Accept', 'application/json')
            .expect(500)
            .end(done);
    });


    it('should pass if plugin does not call next callback', (done) => {
        request(app)
            .get('/testnotreturn/')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, {
                respond: 'ok'
            })
            .end(done);
    });

    it('should return default 404 when plugin passes request and no route found to handle it', (done) => {
        request(app)
            .get('/test3')
            .expect(404)
            .end(done);
    });

    it('should use dynamic parameters', (done) => {
        request(app)
            .get('/test4/')
            .expect(200, {
                respond: 'harry potter'
            })
            .end(done);
    });

    it('should return 404 if method is not allowed', (done) => {
        request(app)
            .patch('/test4/')
            .expect(404)
            .end(done);
    });

    it('should throw if plugin try to make unsupported operation', (done) => {
        request(app)
            .get('/test12/')
            .expect(500)
            .end(done);
    });

    it('should use environment variable', (done) => {
        process.env.SOME_HOST = 'tobi';
        request(app)
            .get('/test5/')
            .expect(200, {
                respond: 'tobi'
            })
            .end(done);
    });

    it('should pass request if plugin doesnt have handler and return 404', (done) => {
        request(app)
            .get('/pluginwithouthandler/')
            .expect(404)
            .end(done);
    });

    it('should be ok to route properly', (done) => {
        request(app)
            .get('/test8/some/path/any')
            .expect(200)
            .end(done);
    });

    it('should be NOT ok to route to unconfigured route', (done) => {
        request(app)
            .get('/test8/so_wrong_me/path/any')
            .expect(404)
            .end(done);
    });

    it('should be ok to proxy query to test server', (done) => {
        request(app)
            .get('/test8/proxy/some/path/any')
            .expect(200)
            .end((err, res) => {               
                expect(res.body.originalUrl).to.eql('/proxy/some/path/any');
                expect(res.body.path).to.eql('/proxy/some/path/any');
                done(err);
            });
    });

    

    it('should be not ok to proxy with 503 if proxy server target is wrong', (done) => {
        request(app)
            .get('/test9/proxy/some/path/any')
            .expect(503)
            .end(done);
    });

    it('should return warn 404 if proxy server is set to disabled state (not active)', (done) => {
        request(app)
            .get('/test10/proxy/some/path/any')
            .expect(404)
            .end(done);
    });

    it('should return 200 if child route set as regular expression and it fits', (done) => {
        request(app)
            .get('/testrx/test1')
            .expect(200)
            .end(done);
    });
    

    it('should return 503 if remote target server is not responding', (done) => {
        request(app)
            .get('/test13/proxy/some/path/any')
            .expect(503)
            .end(done);
    });

    it('should set proper cors when witelist *', (done) => {
        request(app)
            .post('/test8/proxy/some/path/any')
            .set('Origin', 'http://myhost.com')
            .expect(200)
            .end((err, res) => {
                expect(res.body.headers.origin).to.equal('http://myhost.com');
                done();
            });
    });

    it('should set proxy configuration from policy reading query params', (done) => {
        request(app)
            .post('/test14/test_1/?test=test')
            .expect(201)
            .end(done);
    });

    it('should ignore policy if not satisfyed', (done) => {
        request(app)
            .post('/test14/test_2/')
            .set("test", "wrongValue")
            .expect(200) // go to default server
            .end(done);
    });

    it('should set proxy configuration from policy reading header', (done) => {
        request(app)
            .post('/test14/test_2/')
            .set("test", "test")
            .expect(201) // go to overrridden value
            .end(done);
    });

    it('should set proxy configuration from policy reading context', (done) => {
        request(app)
            .post('/test14/test_3/')
            .expect(201)
            .end(done);
    });

    it('should set proxy configuration from policy reading context but return 503 if alternative proxy target is not reachable', (done) => {
        request(app)
            .post('/test14/test_4/')
            .expect(503)
            .end(done);
    });
});
