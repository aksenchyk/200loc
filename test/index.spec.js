"use strict";

const mongoose = require("mongoose")
    , persistance = require('../server/persistance')
    ;

require('./support/env');

before((done) => {
    persistance
        .init(process.env.MONGO_PERS_CONNECTION_STRING)
        .then(() => {
            done();
        })
        .catch((err) => done(err));
});
after((done) => {
    mongoose.connection.close((err) => done(err));
});