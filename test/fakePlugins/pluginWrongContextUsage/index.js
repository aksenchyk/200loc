/*jslint node: true */
"use strict";

module.exports = (function () {
    let cls = function (ctx) {
        this.handler = function (req, res) {
            ctx('$should:throwError');
            return res.status(200).end();
        };
    };
    return cls;
})();
