/*jslint node: true */
"use strict";

module.exports = (function () {

    let cls = function (ctx) {
        this.init = function () {
        };
        this.handler = function (req, res, next) {
            req.__ctx.set('test','value');
            req.__ctx.set('name','harry potter');       
            return next(null);
        };
    };
    return cls;
})();