/*jslint node: true */
"use strict";

module.exports = (function () {

    let cls = function (ctx) {
        this.init = function () {
        };
        this.handler = function (req, res) {            
            return req.__ctx.get('name')
                ? res.status(200).send({ respond: req.__ctx.get('name') })
                : res.status(200).send({ respond: ctx('$get:env') });
        };
    };

    return cls;
})();
