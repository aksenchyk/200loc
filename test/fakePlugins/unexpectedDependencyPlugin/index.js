/*jslint node: true */
"use strict";

module.exports = (function () {

    let cls = function (ctx) {
        this.handler = function (req, res) {
            ctx('$inject:testService').testMethod('testString', (err, result) => {          
                return res.status(200).send({ respond: result });
            });
        };
    };

    return cls;
})();