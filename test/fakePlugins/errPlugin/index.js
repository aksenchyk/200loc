/*jslint node: true */
"use strict";
var httpError = require("http-errors");

module.exports = (function () {
    let cls = function (ctx) {
        this.init = function () { };
        this.handler = function (req, res, next) {         
            return next(ctx('$get:throwError') ? httpError(+ctx('$get:errorCode') || 404, 'error') : null);
        };
    };
    return cls;
})();
