/*jslint node: true, mocha:true*/
"use strict";
const sinon = require('sinon')
    , path = require('path')
    ;

describe('SERVER COMPONENTS LOADER TESTS', () => {
    let app = require('../server');
    let sandbox, closeStub;
    beforeEach((done) => {
        sandbox = sinon.sandbox.create({ useFakeTimers: true });
        closeStub = sandbox.stub(app, 'close');
        done();
    });
    afterEach(() => {
        sandbox.restore();
    });
    it(`should call 'server.close()' when receiving a ${"'SIGTERM'"}`, (done) => {
        app.start(path.resolve(__dirname, 'testConfig.json'))
            .then(() => {
                process.once("SIGTERM", () => {
                    sinon.assert.callCount(closeStub, 3);
                    done();
                });
                process.emit('SIGTERM', 0);
            })
            .catch((err) => {
                done(err);
            });
    });    
});
