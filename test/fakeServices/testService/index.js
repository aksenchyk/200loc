'use strict';
let ServiceBase = require('../../../services/ServiceBase');

class TestService extends ServiceBase {
    constructor(app, serviceConfig) {
        super();
        Object.assign(this, { app, serviceConfig });
        this.testMethod = (param, clb) => {
            clb(null, param.toString());
        };
    };
}

module.exports = TestService;