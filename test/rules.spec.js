'use strict';

const express = require('express')
    , request = require('supertest')  
    , rules = require('../server/gateway/rules')()
    ;


describe('Proxy Routes', function () {
    var serverPort = 6010;
    let handler;
    let app = express();

    const proxyRules = [
        { path: "/test" },
        { path: "/tesomevaluest" },
        { path: "/testalmost/5" },
        { path: "/te?st" },
        { path: "/te*st" },
        { path: "/ab/" }
    ];

    rules.createSet(proxyRules);

    before(function (done) {
        app.use((req, res) => {
            if (rules.match(req)) {
                return res.status(200).end();
            } else {
                return res.status(404).end();
            }
        });
        handler = app.listen(serverPort, () => done());
    });

    after(function (done) {
        handler.close(() => done());
    });

    it('Should match /test', (done) => {
        request(app)
            .get('/test')
            .expect(200)
            .end((err, res) => {
                console.log(res.error)
                done(err);
            });

    });

    it('Should match /test/', (done) => {
        request(app)
            .get('/test/')
            .expect(200)
            .end(done);
    });

    it('Should match with query params', (done) => {
        request(app)
            .get('/test?param=HI')
            .expect(200)
            .end(done);
    });

    it('Should match /test/5', (done) => {
        request(app)
            .get('/test/5')
            .expect(200)
            .end(done);
    });

    it('Should match with * /test/*', (done) => {
        request(app)
            .get('/test/somevalue')
            .expect(200)
            .end(done);
    });

    it('Should match regexp /te?st', (done) => {
        request(app)
            .get('/tst')
            .expect(200)
            .end(done);
    });

    it('Should match regexp /ab/', (done) => {
        request(app)
            .get('/ab/')
            .expect(200)
            .end(done);
    });

    it('Should match regexp /ab/', (done) => {
        request(app)
            .get('/abw')
            .expect(404)
            .end(done);
    });
});