"use strict";

const EventEmitter = require('events')
    , uuid = require('uuid').v4;

class ServiceBase extends EventEmitter {
    constructor() {
        super();
        this.id = uuid();
    }
    run() {
        console.log('Run method is not implemented');
    }
    stop() {
        console.log("Service stop is not implemented");
    }
    check() {
        return Promise.resolve({
            status: "N/A",
            message: "Service doesn't provide status checking",
            error: false,
        });
    }
    summary() {
        return Promise.resolve({
            status: "N/A",
            message: "Service doesn't provide any data",
            error: false,
        });
    }
}

module.exports = ServiceBase;