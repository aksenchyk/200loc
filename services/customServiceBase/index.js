"use strict";

const ServiceBase = require('../ServiceBase')
    , debug = require('debug')('services:custom')
    , ServiceClient = require('../ServiceClientBase')
    ;

class CustomServiceBase extends ServiceBase {

    constructor(serviceConfig) {
        super();
        Object.assign(this, { serviceConfig });
        this.stop = () => {
            debug(`Stopping ${this.serviceConfig.name} client...`);
            if (this.serviceClient) {
                this.serviceClient.end();
                return Promise.resolve('ok');
            } else {
                return Promise.resolve('ok');
            }
        };
        this.serviceClient = new ServiceClient();

        this.run = () => {
            return new Promise((resolve) => {

                this.serviceClient.createClient({
                    healthcheckUrl: this.serviceConfig.healthcheckUrl || 'http://localhost',
                    auth: this.serviceConfig.authentication,
                    name: this.serviceConfig.name,
                    summaryUrl: this.serviceConfig.summaryUrl,
                });

                this.serviceClient.on('connected', () => {
                    return resolve({ message: 'healthy', error: true });
                });
                this.serviceClient.on('error', (err) => {
                    return resolve({ message: err, error: true });
                });
                this.serviceClient.on('disconnected', (err) => {
                    return resolve({ message: err, error: true });
                });
            });
        };
    }

    get state() {
        return this.serviceClient.state;
    }

    get healthy() {
        return this.serviceClient.state === 'connected';
    }

    summary() {
        let config = [];
        for (let k in this.serviceConfig) {
            config.push({ key: k, value: this.serviceConfig[k] });
        }
        return Promise.resolve({
            message: [],
            configuration: config,
            general: [{ key: 'version', value: '0.2.1' }]
        });
    }

    check() {
        return this.healthy ? Promise.resolve({ message: 'healthy' }) : Promise.resolve({ message: `server is not healthy`, error: true });
    }
}

module.exports = CustomServiceBase;
