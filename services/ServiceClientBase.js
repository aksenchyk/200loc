"use strict";

const EventEmitter = require('events').EventEmitter
    , axios = require('axios')
    , httpError = require('http-errors')
    , debug = require('debug')('services:custom')
    ;

/**
 * Custom remote user server client service, holds connection and provides healthchecking
 */
class CustomServiceClient extends EventEmitter {

    constructor() {
        super();
        this._state = 'closed';
    }

    end() {
        this.state = 'closed';
        this.removeAllListeners();
        if (this.__intervalHandler) {
            clearInterval(this.__intervalHandler);
        }
    }

    createClient(options = { url: 'http://localhost:4020', auth: '' }) {
        this.options = options;
        this.connectionString = options.url;
        this.proxyAuthHeaders = options.auth ? this.__proxyAuth(options.auth) : {};
        this.state = 'connecting';
        this.__startHealthCheckProcess();
    }

    __proxyAuth(authString) {
        let up = authString.split(":");
        if (up.length !== 2) {
            return {};
        } else {
            return {
                "authorization": "Basic " + new Buffer(authString).toString("base64")
            };
        }
    };

    __request(method, path, data, headers = {}) {
        if (this.state == 'closed') {
            return Promise.reject(httpError(403, `${this.options.name} service client is closed. Create client first`));
        }
        let options = {
            data: data ? data : null,
            headers: Object.assign({
                'content-type': 'application/json',
                'accept': 'application/json'
            }, this.proxyAuthHeaders, headers),
            method: method,
            url: path
        };
        return new Promise((resolve, reject) => {
            axios(options)
                .then((result) => {
                    if (this.state != 'connected') {
                        this.emit('connected');
                        this.state = 'connected';
                    }
                    resolve(result.data);
                })
                .catch((error) => {
                    if (error.response) {
                        const err = error.response.data && error.response.data.error ? error.response.data.error : { message: error.response.data };
                        err.statusCode = error.response.status;
                        err.source = this.options.name;
                        if (this.state != 'disconnected') {
                            this.emit('disconnected', err);
                            this.state = 'disconnected';
                        }
                    } else if (error.request) {
                        let err = httpError(503, `${error.message}; [source]: events service client [connection]: ${this.connectionString}. Source: gateway.plugins.protector.client`);
                        err.source = this.options.name;
                        if (this.state != 'disconnected') {
                            this.emit('disconnected', err);
                            this.state = 'disconnected';
                        }
                        reject(err);
                    } else {
                        reject(httpError(500, `Bad request to remote service`));
                    }
                });
        });
    }

    __startHealthCheckProcess() {
        this.__intervalHandler = setInterval(() => {
            this.__request('GET', this.options.healthcheckUrl, null, {})
                .catch(() => { });
        }, 5000);
    }

    get state() {
        return this._state;
    }
    set state(value) {
        if (this._state != value) {
            debug(`[${this.options.name}] state changed: [${this._state} --> ${value}]`);
            this._state = value;
        }
    }
}

module.exports = CustomServiceClient;