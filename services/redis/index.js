"use strict";

const redis = require('redis')
    , ServiceBase = require('../ServiceBase')
    , debug = require('debug')('services:redis')
    ;

class RedisService extends ServiceBase {

    constructor(app, serviceConfig = {}) {
        super();
        Object.assign(this, { app, serviceConfig });
        this._state = RedisService.starting;
        this.stop = () => {
            debug(`Stopping redis client...`);
            if (this.redisClient) {
                this.redisClient.end(true, (err, result) => {
                    return err ? Promise.reject(err) : Promise.resolve(result);
                });
            } else {
                return Promise.resolve('ok');
            }
        };

        this.redisClient = redis.createClient({ url: this.__getConnectionString() });
        this.run = () => {
            return new Promise((resolve) => {
                this.redisClient.on("ready", () => {
                    this.state = RedisService.ready;
                    return resolve({ message: 'healthy', error: false });
                });
                this.redisClient.on("error", (err) => {
                    debug(`Redis Db error occurs...`, err);
                    this.state = RedisService.stopped;
                    return resolve({ message: 'not healthy', error: true });
                });
                this.redisClient.on("connect", () => {
                    this.state = RedisService.connected;
                });
                this.redisClient.on("reconnecting", () => {
                    this.state = RedisService.reconnecting;
                    debug(`Trying to reconnect to the redis server on ${this.serviceConfig.url}`);
                });
            });
        };
    }
    __getConnectionString() {
        return this.serviceConfig.url || `redis://redis:6379`;
    }

    get healthy() {
        return this.state === RedisService.ready && this.redisClient.connected;
    }
    get client() {
        return this.redisClient;
    }

    get state() {
        return this._state;
    }
    set state(value) {
        if (value !== this._state) {
            debug(`[REDIS][ ${this.serviceConfig.url} ] state changed: [${this._state} --> ${value}]`);
            this._state = value;
        }
    }

    __getServiceInfo(verbose) {
        return new Promise((resolve) => {
            this.redisClient.info((err, reply) => {
                if (err) {
                    resolve({
                        message: verbose ? [{ key: 'error', value: JSON.stringify(err) }] : "not healthy",
                        general: [
                            { key: 'version', value: '0.0.1' }
                        ],
                        error: true
                    });
                }
                var lines = reply.toString().split('\r\n').sort();
                var summary = [];
                lines.forEach(function (line) {
                    var parts = line.split(':');
                    if (parts[1]) {
                        summary.push({ key: parts[0], value: parts[1] });
                    }
                });
                resolve({
                    message: verbose ? summary : "healthy",
                    general: [
                        { key: 'version', value: '0.0.1' }
                    ]
                });
            });
        });
    }

    summary() {
        let config = [];
        for (let k in this.serviceConfig) {
            config.push({ key: k, value: this.serviceConfig[k] });
        }
        return new Promise((resolve) => {
            this.healthy
                ? this.__getServiceInfo(true)
                    .then((info) => {
                        resolve(Object.assign(info, { configuration: config }));
                    })
                    .catch((err) => {
                        resolve({
                            message: [{ key: 'error', value: err.toString() }],
                            configuration: config,
                            general: [{ key: 'version', value: '0.0.1' }]
                        });
                    })
                : resolve({
                    message: [{ key: 'error', value: 'not reachable' }],
                    configuration: config, general: [{ key: 'version', value: '0.0.1' }]
                });
        });
    }

    check() {
        return this.healthy ? this.__getServiceInfo(false) : Promise.resolve({
            error: true,
            message: 'Not healthy'
        });
    }
}

RedisService.ready = 'healthy';
RedisService.starting = 'starting';
RedisService.reconnecting = 'reconnecting';
RedisService.stopped = 'stopped';
RedisService.connected = 'connected';

module.exports = RedisService;
