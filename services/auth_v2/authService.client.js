"use strict";

const EventEmitter = require('events').EventEmitter
    // , fetch = require('node-fetch')
    , axios = require('axios')
    , httpError = require('http-errors')
    , debug = require('debug')('services:authentication')
    ;

/**
 * Authentication server client service, holds connection and provides healthchecking
 */
class AuthClient extends EventEmitter {

    constructor() {
        super();
        this._state = 'closed';
    }

    end() {
        this.state = 'closed';
        this.removeAllListeners();
        if (this.__intervalHandler) {
            clearInterval(this.__intervalHandler);
        }
    }

    createClient(options = { url: 'http://localhost:4020', auth: '' }) {
        this.connectionString = options.url;
        this.proxyAuthHeaders = options.auth ? this.__proxyAuth(options.auth) : {};
        this.state = 'connecting';
        this.__startHealthCheckProcess();
    }

    __proxyAuth(authString) {
        let up = authString.split(":");
        if (up.length !== 2) {
            return {};
        } else {
            return {
                "x-proxy-auth": "Basic " + new Buffer(authString).toString("base64")
            };
        }
    };

    __request(method, path, data, headers = {}) {
        if (this.state === 'closed') {
            return Promise.reject(httpError(403, 'Authentication service client is closed. Create client first'));
        }
        let options = {
            baseURL: this.connectionString,
            data: data ? data : null,
            headers: Object.assign({
                'content-type': 'application/json',
                'accept': 'application/json'
            }, this.proxyAuthHeaders, headers),
            method: method,
            url: path
        };
        return new Promise((resolve, reject) => {
            axios(options)
                .then((result) => {
                    if (this.state !== 'connected') {
                        this.emit('connected');
                        this.state = 'connected';
                    }
                    resolve(result.data);
                })
                .catch((error) => {
                    if (error.response) {
                        const err = error.response.data && error.response.data.error ? error.response.data.error : error.response.data;
                        err.statusCode = error.response.status;
                        err.source = 'gateway.protector';
                        reject(err);
                    } else if (error.request) {
                        let err = httpError(503, `${error.message}; [source]: events service client [connection]: ${this.connectionString}. Source: gateway.plugins.protector.client`);
                        err.source = 'gateway.protector';
                        if (this.state !== 'disconnected') {
                            this.emit('disconnected', err);
                            this.state = 'disconnected';
                        }
                        reject(err);
                    } else {
                        reject(httpError(500, `Bad request to remote service`));
                    }
                });
        });
    }

    __startHealthCheckProcess() {
        this.__intervalHandler = setInterval(() => {
            this.__request('GET', '/healthcheck', null, {})
                .catch(() => { });
        }, 5000);
    }

    get state() {
        return this._state;
    }
    set state(value) {
        if (this._state !== value) {
            debug(`[AUTHENTICATION][ ${this.connectionString} ] state changed: [${this._state} --> ${value}]`);
            this._state = value;
        }
    }

    askUserPermisisons(token) {
        return this.__request('GET', '/users/askpermissions', null, { 'Authorization': token });
    }

    checkUserPermissions(permissions, token) {
        return this.__request('POST', '/users/checkpermissions', permissions, { 'Authorization': token });
    };

    checkTokenValidity(token) {
        return this.__request('GET', '/users/checktokenvalid', null, { 'Authorization': token });
    };
}

module.exports = AuthClient;