"use strict";

const ServiceBase = require('../ServiceBase')
    , debug = require('debug')('services:authentication')
    , trace = require('debug')('trace')
    , httpError = require('http-errors')
    , AuthServiceClient = require('./authService.client')
    ;

class AuthenticationService extends ServiceBase {

    constructor(app, serviceConfig) {
        super();
        Object.assign(this, { app, serviceConfig });
        this.stop = () => {
            debug(`Stopping auth_v2 client...`);
            if (this.authClient) {
                this.authClient.end();
                return Promise.resolve('ok');
            } else {
                return Promise.resolve('ok');
            }
        };
        this.authClient = new AuthServiceClient();

        this.run = () => {
            return new Promise((resolve) => {
                this.authClient.createClient({ url: this.serviceConfig.endpoint || 'http://localhost:4020', auth: this.serviceConfig.authentication });

                this.authClient.on('connected', () => {
                    return resolve({ message: 'healthy', error: true });
                });
                this.authClient.on('error', (err) => {
                    return resolve({ message: err, error: true });
                });
                this.authClient.on('disconnected', (err) => {
                    return resolve({ message: err, error: true });
                });
            });
        };
    }

    ensurePermissions(token, permissions) {
        if (this.healthy) {
            trace(`Query authentication server for permission ${permissions}`);
            return this.authClient.checkUserPermissions(permissions, token);
        } else {
            return Promise.reject(httpError(401, 'Authentication server is unreachable'));
        }
    }
    askpermissions(token) {
        if (this.healthy) {
            return this.authClient.askUserPermisisons(token);
        } else {
            return Promise.reject(httpError(401, 'Authentication server is unreachable'));
        }
    }
    ensureToken(token) {
        if (this.healthy) {
            trace('Query authentication server for token validity');
            return this.authClient.checkTokenValidity(token);
        } else {
            return Promise.reject(httpError(401, 'Authentication server is unreachable'));
        }
    }

    get state() {
        return this.authClient.state;
    }

    get healthy() {
        return this.authClient.state === 'connected';
    }

    summary() {
        let config = [];
        for (let k in this.serviceConfig) {
            config.push({ key: k, value: this.serviceConfig[k] });
        }
        return Promise.resolve({
            message: [],
            configuration: config,
            general: [{ key: 'version', value: '0.2.1' }]
        });
    }

    check() {
        return this.healthy ? Promise.resolve({ message: 'healthy' }) : Promise.resolve({ message: `server is not healthy`, error: true });
    }    
}

module.exports = AuthenticationService;
