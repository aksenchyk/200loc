webpackJsonp([0],{

/***/ "../../../../../src/app/+entries/entries-base/entries-base.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesBaseComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var EntriesBaseComponent = (function () {
    function EntriesBaseComponent() {
    }
    return EntriesBaseComponent;
}());
EntriesBaseComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: "\n    <router-outlet></router-outlet>\n    "
    })
], EntriesBaseComponent);

//# sourceMappingURL=entries-base.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-base/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_base_component__ = __webpack_require__("../../../../../src/app/+entries/entries-base/entries-base.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__entries_base_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list-details.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".__panel {\n  margin-top: 0;\n  margin-bottom: 0;\n  transition: all .2s ease 200ms; }\n  @media screen and (min-width: 767px) {\n    .__panel.expanded {\n      margin-top: .625rem;\n      margin-bottom: .625rem;\n      transition: all .2s ease 200ms;\n      box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.15), 0 0 1px 0 rgba(0, 0, 0, 0.1), 0 1px 3px 0 rgba(0, 0, 0, 0.05); } }\n  .__panel .__details {\n    overflow: hidden;\n    height: 210px;\n    background-color: white;\n    transition: height .2s ease; }\n    @media screen and (max-height: 600px) {\n      .__panel .__details {\n        height: 140px; } }\n    @media screen and (max-height: 800px) {\n      .__panel .__details {\n        height: 180px; } }\n    .__panel .__details.disabled {\n      height: 75px; }\n    .__panel .__details .__content {\n      height: 100%;\n      overflow: hidden;\n      position: relative;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      transition: all .2s ease 200ms;\n      padding: 10px; }\n    .__panel .__details .__healthblocks {\n      -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n              justify-content: space-between;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 0 120px;\n              flex: 0 0 120px; }\n    .__panel .__details .__graph {\n      position: relative;\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      margin: 0 0 0 .325rem;\n      padding: .325rem; }\n    .__panel .__details .__statuses {\n      position: relative;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 30px;\n              flex: 0 1 30px;\n      margin-left: .325rem;\n      -ms-flex: 0 1 21%;\n          flex: 0 1 21%;\n      min-width: 0 !important;\n      padding-left: 20px; }\n      .__panel .__details .__statuses.toggled {\n        -webkit-box-flex: 0;\n            -ms-flex: 0 1 20px;\n                flex: 0 1 20px; }\n    .__panel .__details .__health {\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 49%;\n              flex: 0 1 49%;\n      position: relative;\n      color: #ea392b; }\n      .__panel .__details .__health:before {\n        position: absolute;\n        top: 0;\n        bottom: 0;\n        left: 0;\n        width: 4px;\n        height: 100%;\n        content: \"\";\n        background-color: #ea392b; }\n      .__panel .__details .__health.healthy {\n        text-transform: uppercase;\n        color: #27a93e; }\n        .__panel .__details .__health.healthy:before {\n          background-color: #27a93e; }\n      .__panel .__details .__health.disabled {\n        color: #555;\n        background: #fafafa; }\n        .__panel .__details .__health.disabled:before {\n          background-color: transparent; }\n    .__panel .__details .__average {\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 49%;\n              flex: 0 1 49%; }\n  .__panel .__header {\n    background-color: white; }\n  .__panel .__toggleStatusesButton {\n    -webkit-transform-origin: 50% 50%;\n            transform-origin: 50% 50%;\n    height: 20px;\n    width: 20px;\n    position: absolute;\n    left: 0;\n    top: 50%;\n    display: block;\n    padding: 0;\n    line-height: 0;\n    transition: all .3s linear; }\n    .__panel .__toggleStatusesButton > svg {\n      fill: #a6b0b7; }\n    .__panel .__toggleStatusesButton:hover > svg {\n      fill: #959da2; }\n    .__panel .__toggleStatusesButton.right {\n      -webkit-transform: rotate(90deg);\n              transform: rotate(90deg);\n      display: inline-block; }\n    .__panel .__toggleStatusesButton.left {\n      -webkit-transform: rotate(-90deg);\n              transform: rotate(-90deg);\n      display: inline-block; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list-details.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div #panel class=\"__panel\" \r\n    [class.expanded]=\"expand\" \r\n    [class.error]='configO?.errors.length > 0' \r\n    (clickedOut)='close()' \r\n    [ignoreSelector]=\"['ngb-modal-window']\"\r\n>\r\n    <div class=\"__header\" [@headerCollapse]=\"state\">\r\n        <ng-content select=\"[header-content]\"></ng-content>\r\n    </div>\r\n    <div #detailsContainer class=\"__details\" [@detailsCollapse]=\"state\" [ngClass]=\"{disabled:!configO.active}\">\r\n        <div class=\"__content\" *ngIf='configO.active;else disabledContent' >\r\n            <div style='width:1px;height:100px;'></div>\r\n            <loader [active]='loading' [delay]='300'></loader>\r\n            <div *ngIf='expand' class='entry__status __healthblocks'>\r\n                <div class='entry__status-card entry__status-card--health __health' [ngClass]='status.message'>\r\n                    <span>{{status.message}}</span>\r\n                </div>\r\n                <div class='entry__status-card entry__status-card--average __average'>                    \r\n                    <span>{{average}}</span>\r\n                </div> \r\n            </div>\r\n            <div *ngIf='expand' class='entry__status-card entry__status-card--loadchart __graph' style='display: flex'>                    \r\n                <canvas baseChart \r\n                    [datasets]=\"loadChartData\"                  \r\n                    [labels]=\"loadChartLabels\"\r\n                    [colors]=\"lineChartColors\"\r\n                    [options]=\"loadChartOptions\"\r\n                    [legend]=\"loadChartLegend\"\r\n                    [chartType]=\"loadChartType\"\r\n                >\r\n                </canvas>                 \r\n            </div>\r\n            <div *ngIf='expand' class='entry__status-card entry__status-card--loadchart __statuses' [class.toggled]='!showStatuses'>    \r\n                <button class='button--icon indicator-button __toggleStatusesButton' [ngClass]=\"{'right':showStatuses, 'left':!showStatuses}\" (click)=\"showStatuses = !showStatuses\">\r\n                    <svg _ngcontent-c5=\"\" xml:space=\"preserve\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" height=\"20px\" id=\"Layer_1\" style=\"enable-background:new 0 0 512 512;\" version=\"1.1\" viewBox=\"0 0 512 512\" width=\"20px\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                        <path _ngcontent-c5=\"\" d=\"M402 311c0-5-2-9-5-13l-128-128c-4-4-8-5-13-5-5 0-9 1-13 5l-128 128c-3 4-5 8-5 13 0 5 2 9 5 13 4 3 8 5 13 5l256 0c5 0 9-2 13-5 3-4 5-8 5-13z\"></path>\r\n                    </svg>    \r\n                </button>                    \r\n                <canvas baseChart \r\n                    [data]=\"statusesChartData\"\r\n                    [labels]=\"statusesChartLabels\"\r\n                    [options]=\"statusesChartOptions\"\r\n                    [chartType]=\"statusesChartType\"\r\n                    [colors]=\"statusesChartColors\">\r\n                </canvas>                                \r\n            </div>\r\n        </div>\r\n        <ng-template #disabledContent>\r\n            <div class=\"__content\" style='text-align:center;padding:25px'>\r\n                <h3> Entry is disabled, Traffic is off...</h3>\r\n            </div>\r\n        </ng-template>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_charts_ng2_charts__ = __webpack_require__("../../../../ng2-charts/ng2-charts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesDetailsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var EntriesDetailsComponent = (function () {
    function EntriesDetailsComponent(_renderer, _elementRef, _router, modalService, _route, _statsActions, ref, _statsService, _store) {
        this._renderer = _renderer;
        this._elementRef = _elementRef;
        this._router = _router;
        this.modalService = modalService;
        this._route = _route;
        this._statsActions = _statsActions;
        this.ref = ref;
        this._statsService = _statsService;
        this._store = _store;
        this._expand = false;
        this._disabled = false;
        this.state = 'collapsed';
        this.dialogOpened = false;
        this.showStatuses = false;
        this.loading = false;
        this.status = { error: false, message: 'N/A' };
        this.average = 0;
        this.expanded = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.collapsed = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.produceLabels = function () {
            var coeff = 1000;
            var now = Date.now();
            var rounded = coeff * Math.round(now / coeff);
            var aggregation = [];
            for (var i = 59; i >= 0; i--) {
                aggregation.push(rounded - i * 1000);
            }
            return aggregation;
        };
        this.produceStatusesColors = function (data) {
            if (data === void 0) { data = []; }
            var colors = data.map(function (d) {
                var color = 'grey';
                switch (d) {
                    case "404": {
                        color = "#FF9C00";
                        break;
                    }
                    case "401": {
                        color = "#ff8300";
                        break;
                    }
                    case "400": {
                        color = "#ff6c00";
                        break;
                    }
                    case "200": {
                        color = "#7BB31A";
                        break;
                    }
                    default: {
                        if (+d >= 200) {
                            color = 'green';
                        }
                        if (+d >= 400) {
                            color = '#EEDB00';
                        }
                        if (+d >= 500) {
                            color = '#CE0000';
                        }
                        break;
                    }
                }
                return color;
            });
            return colors;
        };
        this.loadChartLabels = this.produceLabels();
        this.statusesChartOptions = {
            scaleShowVerticalLines: false,
            maintainAspectRatio: false,
            responsive: true,
            fonts: {
                defaultFontFamily: 'Open Sans'
            },
            animation: {
                duration: 0,
            },
        };
        this.min = Date.now();
        this.loadChartOptions = {
            scaleShowVerticalLines: false,
            scaleBeginAtZero: true,
            maintainAspectRatio: false,
            responsive: true,
            defaultFontFamily: 'Open Sans',
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            suggestedMin: 0,
                            callback: function (value) {
                                return value % 1 === 0 ? +value : "";
                            },
                            fontColor: "#555",
                            fontSize: 11,
                        },
                    }],
                xAxes: [{
                        type: 'time',
                        ticks: {
                            beginAtZero: true,
                            //  fontColor: "#555",
                            //  fontSize: 11,
                            // fontColor: "transparent",
                            display: false
                        },
                        unit: 'second',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'second': 'hh:mm:ss'
                            },
                            tooltipFormat: 'hh:mm:ss',
                        },
                        steps: 61,
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 1
                }
            },
            animation: {
                duration: 0,
            },
        };
        this.lineChartColors = [{
                backgroundColor: '#DEE7EF',
                borderColor: "#9CAAC6",
                pointBackgroundColor: '#5A79A5',
                pointBorderColor: '#9CAAC6',
                pointHoverBackgroundColor: '#000063',
                pointHoverBorderColor: '#000063'
            }];
        this.loadChartType = "line";
        this.statusesChartType = 'doughnut';
        this.loadChartLegend = true;
        this.statusesChartData = [1];
        this.statusesChartLabels = ['n/a'];
        this.statusesChartColors = [{ backgroundColor: this.produceStatusesColors(['n/a']).slice() }];
        this.loadChartData = [
            { data: [], label: 'Req/s' }
        ];
    }
    EntriesDetailsComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this._store
            .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_6__core_reducers__["e" /* getStats */])())
            .subscribe(function (result) {
            _this.loadChartData = [{ data: result.load, label: 'Req/s' }];
            _this.loadChartLabels.length = 0;
            var labels = _this.produceLabels();
            (_a = _this.loadChartLabels).push.apply(_a, labels);
            var statusesLabels = [];
            var statusesData = [];
            Object.keys(result.statuses || {})
                .forEach(function (status) {
                statusesData.push(+result.statuses[status]);
                statusesLabels.push(status);
            });
            _this.statusesChartLabels.length = 0;
            if (statusesLabels.length > 0) {
                (_b = _this.statusesChartLabels).push.apply(_b, statusesLabels);
            }
            else {
                _this.statusesChartLabels.push('n/a');
            }
            _this.statusesChartData.length = 0;
            if (statusesData.length > 0) {
                (_c = _this.statusesChartData).push.apply(_c, statusesData);
            }
            else {
                _this.statusesChartData.push(1);
            }
            _this.statusesChartColors[0].backgroundColor.length = 0;
            (_d = _this.statusesChartColors[0].backgroundColor).push.apply(_d, _this.produceStatusesColors(_this.statusesChartLabels));
            var chrartsArr = _this.charts.toArray();
            if (chrartsArr[1] && chrartsArr[1].chart) {
                chrartsArr[1].chart.update();
            }
            ;
            _this.average = result.average || 0;
            var _a, _b, _c, _d;
        });
    };
    Object.defineProperty(EntriesDetailsComponent.prototype, "expand", {
        get: function () {
            return this._expand;
        },
        set: function (value) {
            this._expand = value;
            this.state = value ? 'expanded' : 'collapsed';
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(EntriesDetailsComponent.prototype, "disabled", {
        get: function () {
            return this._disabled;
        },
        set: function (disabled) {
            if (disabled && this._expand) {
                this._expand = false;
                this._onCollapsed();
            }
            this._disabled = disabled;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    EntriesDetailsComponent.prototype.clickEvent = function () {
        this._setExpand(!this.expand);
    };
    ;
    EntriesDetailsComponent.prototype.ngOnDestroy = function () {
        this.__stopStatsTimer();
    };
    EntriesDetailsComponent.prototype.__startStatsTimer = function () {
        var _this = this;
        this.intervalHandler = setInterval(function () {
            _this._statsService
                .stats('stats', { id: _this.configO.id })
                .subscribe(function (result) {
                _this._store.dispatch(_this._statsActions.setStats(result.stats || {}));
            }, function (err) {
                console.log(err);
            });
        }, 1000);
    };
    EntriesDetailsComponent.prototype.__stopStatsTimer = function () {
        if (this.intervalHandler) {
            clearInterval(this.intervalHandler);
        }
    };
    /**
     * Toggle expand state of [TdExpansionPanelComponent]
     * retuns 'true' if successful, else 'false'.
     */
    EntriesDetailsComponent.prototype.toggle = function () {
        var _this = this;
        this._setExpand(!this.expand);
        if (this.expand && this.configO && this.configO.errors.length > 0) {
            return;
        }
        if (this.expand && this.configO) {
            this._store
                .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_6__core_reducers__["f" /* getHealth */])())
                .map(function (config) { return config.find((function (c) { return c.id == _this.configO.id; })); })
                .subscribe(function (config) {
                _this.status = config || { message: 'n/a' };
            }, function (err) {
                _this.status = {
                    message: err,
                    error: true
                };
            });
        }
    };
    /**
     * Opens [TdExpansionPanelComponent]
     * retuns 'true' if successful, else 'false'.
     */
    EntriesDetailsComponent.prototype.open = function () {
        return this._setExpand(true);
    };
    /**
     * Closes [TdExpansionPanelComponent]
     * retuns 'true' if successful, else 'false'.
     */
    EntriesDetailsComponent.prototype.close = function () {
        if (this.dialogOpened)
            return;
        return this._setExpand(false);
    };
    /**
     * Method to change expand state internally and emit the [onExpanded] event if 'true' or [onCollapsed]
     * event if 'false'. (Blocked if [disabled] is 'true')
     */
    EntriesDetailsComponent.prototype._setExpand = function (newExpand) {
        if (this._disabled) {
            this.expand = false;
        }
        if (this.expand !== newExpand) {
            this.expand = newExpand;
            if (newExpand) {
                this._onExpanded();
            }
            else {
                this._onCollapsed();
            }
            return true;
        }
        return false;
    };
    ;
    EntriesDetailsComponent.prototype._onExpanded = function () {
        if (this.configO && this.configO.active) {
            this.__startStatsTimer();
        }
        this.expanded.emit(undefined);
    };
    ;
    EntriesDetailsComponent.prototype._onCollapsed = function () {
        this.__stopStatsTimer();
        this.collapsed.emit(undefined);
    };
    ;
    EntriesDetailsComponent.prototype.openModal = function () {
        var _this = this;
        this.dialogOpened = true;
        var modalRef = this.modalService
            .open(this.content, { windowClass: 'statistic-modal' });
        modalRef.result.then(function (result) {
            console.log('DIALOG CLOSING');
            _this.dialogOpened = false;
        }, function (reason) {
            console.log('DIALOG CLOSING2');
            setTimeout(function () {
                _this.dialogOpened = false;
            }, 100);
        });
    };
    return EntriesDetailsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('panel'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], EntriesDetailsComponent.prototype, "container", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('detailsContainer'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object)
], EntriesDetailsComponent.prototype, "detailsContainer", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('content'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _c || Object)
], EntriesDetailsComponent.prototype, "content", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])(__WEBPACK_IMPORTED_MODULE_4_ng2_charts_ng2_charts__["BaseChartDirective"]),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"]) === "function" && _d || Object)
], EntriesDetailsComponent.prototype, "charts", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__core__["l" /* ApiConfig */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["l" /* ApiConfig */]) === "function" && _e || Object)
], EntriesDetailsComponent.prototype, "configO", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], EntriesDetailsComponent.prototype, "label", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], EntriesDetailsComponent.prototype, "sublabel", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('disabled'),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], EntriesDetailsComponent.prototype, "disabled", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _f || Object)
], EntriesDetailsComponent.prototype, "expanded", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _g || Object)
], EntriesDetailsComponent.prototype, "collapsed", void 0);
EntriesDetailsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'entries-details',
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-list/entries-list-details.component.scss")],
        template: __webpack_require__("../../../../../src/app/+entries/entries-list/entries-list-details.component.tmpl.html"),
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('detailsCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '*'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '0px'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('0.2s ease-out', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ height: '*' }))
                ]),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('0.2s ease-in', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ height: '0px' }))
                ])
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('headerCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    background: '#fafafa'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    background: '#fff'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms linear')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms linear'))
            ])
        ]
    }),
    __metadata("design:paramtypes", [typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_2__core__["m" /* StatsActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["m" /* StatsActions */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_2__core__["n" /* StatsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["n" /* StatsService */]) === "function" && _q || Object, typeof (_r = typeof __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */]) === "function" && _r || Object])
], EntriesDetailsComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r;
//# sourceMappingURL=entries-list-details.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list-indicator.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class='entry__indicator' [class.up]='up'>\r\n    <div #d *ngIf=\"type=='healthy'\" class='entry__indicator-marker entry__indicator-marker--green'>\r\n        <button class=\"button--icon indicator-button\" [@iconState]=\"state\" (click)='onClick.emit($event)'> \r\n              <svg height=\"20px\" id=\"Layer_1\" style=\"enable-background:new 0 0 512 512;\" \r\n                version=\"1.1\" viewBox=\"0 0 512 512\" \r\n                width=\"20px\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" \r\n                xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n                <path d=\"M402 311c0-5-2-9-5-13l-128-128c-4-4-8-5-13-5-5 0-9 1-13 5l-128 128c-3 4-5 8-5 13 0 5 2 9 5 13 4 3 8 5 13 5l256 0c5 0 9-2 13-5 3-4 5-8 5-13z\"/>\r\n              </svg>\r\n        </button>\r\n    </div>\r\n    <div *ngIf=\"type=='error'\" class='entry__indicator-marker entry__indicator-marker--red'>\r\n        <button class=\"button--icon indicator-button\" [@iconState]=\"state\" (click)='onClick.emit($event)'> \r\n              <svg height=\"20px\" id=\"Layer_1\" style=\"enable-background:new 0 0 512 512;\" \r\n                version=\"1.1\" viewBox=\"0 0 512 512\" \r\n                width=\"20px\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" \r\n                xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n                <path d=\"M402 311c0-5-2-9-5-13l-128-128c-4-4-8-5-13-5-5 0-9 1-13 5l-128 128c-3 4-5 8-5 13 0 5 2 9 5 13 4 3 8 5 13 5l256 0c5 0 9-2 13-5 3-4 5-8 5-13z\"/>\r\n              </svg>\r\n        </button>\r\n    </div>\r\n    <div *ngIf=\"type=='disabled'\" class='entry__indicator-marker entry__indicator-marker--grey'>\r\n        <button class=\"button--icon indicator-button\" [@iconState]=\"state\" (click)='onClick.emit($event)'> \r\n              <svg height=\"20px\" id=\"Layer_1\" style=\"enable-background:new 0 0 512 512;\" \r\n                version=\"1.1\" viewBox=\"0 0 512 512\" \r\n                width=\"20px\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" \r\n                xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n                <path d=\"M402 311c0-5-2-9-5-13l-128-128c-4-4-8-5-13-5-5 0-9 1-13 5l-128 128c-3 4-5 8-5 13 0 5 2 9 5 13 4 3 8 5 13 5l256 0c5 0 9-2 13-5 3-4 5-8 5-13z\"/>\r\n              </svg>\r\n        </button>\r\n    </div>\r\n    <div *ngIf=\"type=='blank'\" class='entry__indicator-marker entry__indicator-marker--grey'>\r\n        <i class='icon-android-radio-button-off' title='No services used'></i>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list-indicator.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesIndicatorComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EntriesIndicatorComponent = (function () {
    function EntriesIndicatorComponent() {
        this.messages = [];
        this.errors = [];
        this.type = 'blank';
        this.onClick = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this._up = false;
        this.state = 'down';
    }
    Object.defineProperty(EntriesIndicatorComponent.prototype, "up", {
        get: function () {
            return this._up;
        },
        set: function (value) {
            this.state = value ? 'up' : 'down';
            this._up = value;
        },
        enumerable: true,
        configurable: true
    });
    return EntriesIndicatorComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], EntriesIndicatorComponent.prototype, "messages", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], EntriesIndicatorComponent.prototype, "errors", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], EntriesIndicatorComponent.prototype, "type", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EntriesIndicatorComponent.prototype, "onClick", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Object])
], EntriesIndicatorComponent.prototype, "up", null);
EntriesIndicatorComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'entry-indicator',
        template: __webpack_require__("../../../../../src/app/+entries/entries-list/entries-list-indicator.component.tmpl.html"),
        changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush,
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('iconState', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('up', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(0deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('down', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(180deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('up => down', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.4s ease-in')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('down => up', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.4s ease-out')),
            ])
        ],
        styles: [
            "\n        :host {\n            width: 20px;\n            height: 20px;\n            display: inline-block;\n        }\n\n        .indicator-button {\n            transform-origin: 50% 50%;\n            height: 20px;\n            width: 20px;\n        }\n\n        "
        ]
    })
], EntriesIndicatorComponent);

var _a;
//# sourceMappingURL=entries-list-indicator.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"l-entries\" scrollSpy #spy='spy' flexy [bottom]='50' *locAuthOnly >\r\n    <div class=\"l__page-header\">\r\n        <span class=\"title\">Entries</span>\r\n    </div>\r\n    <div class='l__page-body'>\r\n        <div class=\"entries\">\r\n            <div class=\"entries__table\">\r\n                <div class='entries__header' [@initial]=\"state\">\r\n                    <div class=\"entries__header-search\">\r\n                        <div class=\"input__search\">\r\n                            <input #searchInput placeholder='Filter by name' [formControl]=\"searchControl\" [ngModel]='searchParams.name'>\r\n                            <div class='hightlite-underline'>\r\n                                <span class=\"hightlite-bar\"></span>\r\n                            </div>\r\n                            <i class=\"icon-search\"></i>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"entries__header-operations\">\r\n                        <button title=\"Restore config\" class='entries__header-item' (click)='showMenu=!showMenu'>\r\n                            <i class=\"icon-android-menu\" aria-hidden=\"true\"></i>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n                <div class='entries__config-menu' [ngClass]='{\"shown\": showMenu }'>\r\n                    <input #uploadEl type=\"file\" name=\"config\" id=\"config\" ng2FileSelect [uploader]=\"uploader\" class=\"entries__config-input\">\r\n                    <label for=\"config\" class='button button--borderless entries__config-button entries__config-button--upload'>\r\n                        <i class=\"icon-arrow-down-circle\" aria-hidden=\"true\"></i> Import\r\n                    </label>\r\n                    <button title=\"Export\" class='button button--borderless entries__config-button' (click)='onExportClick()'>\r\n                        <i class=\"icon-link-external\" aria-hidden=\"true\"></i> Export\r\n                    </button>\r\n                    <button title=\"Add new\" class='button button--borderless entries__config-button' (click)='onAddClick()'>\r\n                        <i class=\"icon-android-add-circle\" aria-hidden=\"true\"></i> Add New\r\n                    </button>\r\n                </div>\r\n                <div class=\"entries__table\" [style.minHeight.px]='!initialComplete || configs?.length == 0 ? 150: 0'>\r\n                    <loader [active]='loading' [delay]='300' [overlay]='configs?.length > 0'></loader>\r\n                    <div class=\"entries__table-container\">\r\n                        <entries-details #details *ngFor='let config of configs; let c = count;' [configO]='config'>\r\n                            <div header-content class='entry'>\r\n                                <div class='m__table-row'>\r\n                                    <div class=\"m__table-cell auto\">\r\n                                        <entry-indicator [type]='config.type' [up]='details.expand' (onClick)='details.toggle();'></entry-indicator>\r\n                                    </div>\r\n                                    <!--Name cell-->\r\n                                    <div class=\"m__table-cell hidden-sm-down\">\r\n                                        <span class='entry__description'>{{ config.name }}</span>\r\n                                    </div>\r\n                                    <!--Etry path cell-->\r\n                                    <div class=\"m__table-cell\" style=\"flex: 2 0 auto;\">\r\n                                        <a class=\"__entry-link\" [routerLink]=\"['./master', {t:'general'}]\" [queryParams]='{ id: config.id }' [title]='config.entry'>\r\n                                            ~{{ config.entry }}\r\n                                        </a>\r\n                                    </div>\r\n                                    <!--Groups being applied to the entry (TBD)-->\r\n                                    <div class=\"m__table-cell auto hidden-sm-down\">\r\n                                        <a [routerLink]=\"['./master', {t:'routes'}]\" [queryParams]='{ id: config.id }' *ngIf='config.active' class=\"m-badge m-badge--info\"\r\n                                            [title]='getRulesPath(config)'>{{ getRulesCount(config) }}</a>\r\n                                        <span *ngIf='!config.active' class=\"m-badge m-badge--default\" title='Disabled'>D</span>\r\n                                    </div>\r\n                                    <div class=\"m__table-cell auto\">\r\n                                        <div class=\"entry__actions\">\r\n                                            <ng-template #sidebarTmpl>\r\n                                                <div class=\"m-sidebar__header\">\r\n                                                    <a style=\"color:inherit; margin: auto;\" [routerLink]=\"['./master', { t:'genegal'}]\" [queryParams]='{ id: config.id }' (click)=\"setActive(false)\"\r\n                                                        class=\"m-header__name\">\r\n                                                        {{ config.name }}\r\n                                                    </a>\r\n                                                </div>\r\n                                                <ul class=\"m-sidebar__menu m-sidebar__menu--edit-entry\">\r\n                                                    <li>\r\n                                                        <a class=\"m-sidebar__menu-item\" (click)=\"editApi(config); sidebarMore.close();\">\r\n                                                            <i class=\"icon-android-create\"></i>Edit entry</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a class=\"m-sidebar__menu-item\" (click)=\"onClone(config); sidebarMore.close();\">\r\n                                                            <i class=\"icon-clone\"></i>Clone entry</a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a class=\"m-sidebar__menu-item m-sidebar__menu-item--remove\" (click)=\"onRemove(config); sidebarMore.close();\">\r\n                                                            <i class=\"icon-delete-garbage-streamline\"></i>Remove entry</a>\r\n                                                    </li>\r\n                                                </ul>\r\n                                                <div class=\"m-sidebar__content\">\r\n                                                    <div class='__entry-sidebar'>\r\n                                                        <label (click)='activate.toggleValue()'>{{config.active ? 'Disable': 'Enable'}} entry</label>\r\n                                                        <loc-switch #activate [value]='config.active' [disabled]='loading' (change)='toggleActive(config, $event); config.active = $event'\r\n                                                            [disabled]='config.errors && config.errors.length > 0'>\r\n                                                        </loc-switch>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </ng-template>\r\n                                            <button class=\"entry__actions-item\" (click)=\"sidebarMore.open($event, sidebarTmpl)\">\r\n                                                <span class=\"icon-android-more-vertical\"></span>\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                    <!--End actions cell-->\r\n                                </div>\r\n                            </div>\r\n                        </entries-details>\r\n                    </div>\r\n                    <!--End api table-->\r\n                    <h3 *ngIf='configs.length == 0 && !loading'>No entry points</h3>\r\n                </div>\r\n                <div class='entries__footer' [class.hidden]='count <= maxItems || !initialComplete'>\r\n                    <loc-paging-bar [firstLast]=\"false\" [pageSizeAll]=\"false\" [pageSize]=\"maxItems\" [total]=\"count\"></loc-paging-bar>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"spy.scrollFlow$ | async\"><button>SomeButton</button></div>\r\n</div>\r\n\r\n<ng-template #importPreview let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"l-entries__import\">\r\n        <header class=\"l-entries__import-header\">\r\n            <h1>Restore</h1>\r\n        </header>\r\n        <div class=\"row l-entries__import-body\">\r\n            <section class=\"l-entries__import-form l-entries__import-form--routes\">\r\n                <div class='col-sm-12'>\r\n                    <header class='l-entries__import-commands'>\r\n                        <button title=\"Export\" class='button button--borderless l-entries__import-button' (click)='selectAll()'>\r\n                            <i class=\"icon-checkmark-circled\" aria-hidden=\"true\"></i> Select all\r\n                        </button>\r\n                    </header>\r\n                    <ul class=\"l-entries__import-route-list\">\r\n                        <li *ngFor=\"let route of importData.routes\" [class.selected]=\"route.active\" (click)='route.active=!route.active'>\r\n                            <span>{{route.name}}</span>\r\n                            <loc-switch class='health__switch' [(ngModel)]='route.active' [type]=\"'rectangle'\" style='margin: auto 0;'></loc-switch>\r\n                        </li>\r\n                    </ul>\r\n                    <span *ngIf=\"importData.routes.length === 0\">No routes available</span>\r\n                    <footer *ngIf=\"importData.routes.length > 0\">\r\n                        <span class=\"route-count\">\r\n                            <strong>{{importData.routes.length}}</strong> {{importData.routes.length == 1 ? 'item' : 'items'}} left</span>\r\n                    </footer>\r\n                </div>\r\n            </section>\r\n        </div>\r\n        <footer class=\"l-entries__import-footer\">\r\n            <button type=\"button\" class=\"button button--primary\" [disabled]='isDisabledModalImportInsertButton()' (click)=\"insertSelected(importData);\"\r\n                style='flex: 0 1 auto;'>Insert</button>\r\n            <button type=\"button\" class=\"button button--primary\" (click)=\"d()\" style='flex: 0 1 auto;'>Cancel</button>\r\n        </footer>\r\n    </div>\r\n</ng-template>\r\n\r\n<loc-side-bar #sidebarMore='locSidebar' position='right'></loc-side-bar>\r\n<ng-template ngbModalContainer></ng-template>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_components__ = __webpack_require__("../../../../../src/app/shared/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_file_saver__ = __webpack_require__("../../../../file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var EntriesListComponent = (function () {
    function EntriesListComponent(router, route, _serviceApi, _authService, _ngZone, modalService, _apiConfigApi, _store) {
        var _this = this;
        this.router = router;
        this.route = route;
        this._serviceApi = _serviceApi;
        this._authService = _authService;
        this._ngZone = _ngZone;
        this.modalService = modalService;
        this._apiConfigApi = _apiConfigApi;
        this._store = _store;
        this.configs = [];
        this.loading = false;
        this.sidebarActive = false;
        this.serviceStatusArray = [];
        this.initialComplete = false;
        this.state = 'start';
        this.searchParams = {};
        this.maxItems = 10;
        this.showMenu = false;
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* FormControl */]();
        this.importData = { routes: [] };
        var header = { 'name': "Authorization", 'value': _authService.user ? _authService.user.accessToken : '' };
        this.uploader = new __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__["FileUploader"]({
            url: '/api/upload',
            method: 'post',
            headers: [header],
            autoUpload: true
        });
        this.uploader.onSuccessItem = function (item, response, status) {
            _this.uploader.clearQueue();
            _this.modalRef = _this.modalService
                .open(_this.content, { windowClass: 'import-modal' });
            _this.modalRef.result.then(function (result) {
                _this.importData = { routes: [] };
            }, function (reason) {
                _this.importData = { routes: [] };
            });
            try {
                _this.importData = JSON.parse(response);
                _this.importData.routes = _this.importData.routes.map(function (r) {
                    r.active = false;
                    return r;
                });
            }
            catch (error) {
                console.error(error);
            }
        };
    }
    EntriesListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.searchParams = {
            name: this.route.snapshot.queryParams['name'] || '',
            page: +this.route.snapshot.queryParams['page'] || 1
        };
        this.route.queryParams
            .do(function (qParams) { _this.loading = true; })
            .map(function (qParams) {
            var q = {
                limit: _this.maxItems,
                skip: +qParams.page && +qParams.page > 0 ? _this.maxItems * (+qParams.page - 1) : 0
            };
            if (qParams && qParams.name && qParams.name.trim) {
                q.where = { name: "" + qParams.name.trim() };
            }
            return q;
        })
            .switchMap(function (query) { return _this._doSearchQuery(query); })
            .do(function () { _this.initialComplete = true; _this.state = 'complete'; })
            .subscribe(function (result) {
            _this.configs = result.configs;
            _this.count = result.count;
            _this.loading = false;
            if (_this.locPager) {
                _this.locPager.total = _this.count;
                _this.locPager.navigateToPage(_this.searchParams.page || 1, false);
            }
        }, function (err) {
            console.error(err);
            _this.loading = false;
        });
        this.updateIntervalHandler = this._store
            .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10__core_reducers__["f" /* getHealth */])())
            .subscribe(function (state) {
            _this.serviceStatusArray = state;
            _this.configs = _this.configs.map(function (c) { return _this.setStatus(c); });
        }, function (err) { });
    };
    EntriesListComponent.prototype._doSearchQuery = function (query) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].zip(this._apiConfigApi.find(query), this._apiConfigApi.count(!!query.where ? query.where : {}), function (configs, conuntResult) {
            _this.configs = configs.map(function (c) { return _this.setStatus(c); });
            return {
                configs: configs,
                count: conuntResult.count
            };
        });
    };
    EntriesListComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.uploader.onAfterAddingFile = (function (item) {
            _this.uploadElRef.nativeElement.value = '';
        });
        this.searchControl
            .valueChanges
            .distinctUntilChanged()
            .do(function () { return _this.loading = true; })
            .debounceTime(500)
            .map(function (value) { return { name: value, page: 1 }; })
            .subscribe(function (q) {
            _this.doSearch(q);
        });
        if (this.locPager)
            this.locPager
                .pageChanged
                .distinctUntilChanged()
                .do(function () { return _this.loading = true; })
                .map(function (value) { return { page: value.page }; })
                .subscribe(function (q) {
                _this.doSearch(q);
            });
    };
    EntriesListComponent.prototype.doSearch = function (q) {
        var req = Object.assign(this.searchParams, q);
        console.log(req);
        this.router.navigate(['/entries'], { queryParams: req });
    };
    EntriesListComponent.prototype.ngOnDestroy = function () {
        if (this.updateIntervalHandler) {
            clearInterval(this.updateIntervalHandler);
        }
    };
    EntriesListComponent.prototype.onRemove = function (config) {
        var _this = this;
        this._apiConfigApi
            .deleteById(config.id)
            .subscribe(function (res) {
            var ind = _this.configs.indexOf(config);
            _this.configs.splice(ind, 1);
            _this.__onSearch();
        }, function (err) {
            console.log(err);
        });
    };
    EntriesListComponent.prototype.setStatus = function (config) {
        var messages = new Array();
        var st = this.serviceStatusArray.find(function (s) { return s.id == config.id; });
        if (st) {
            messages.push(st);
            var type = "error";
            if ((st.message === 'disabled' || st.message === 'healthy')) {
                type = st.message;
            }
            return Object.assign(config, { type: type });
        }
        else {
            return Object.assign(config, { type: 'disabled' });
        }
    };
    EntriesListComponent.prototype.editApi = function (config) {
        this.router.navigate(['./master'], { queryParams: { id: config.id }, relativeTo: this.route });
    };
    EntriesListComponent.prototype.toggleActive = function (config, value) {
        var _this = this;
        this.loading = true;
        this._apiConfigApi.updateOrCreate(Object.assign({}, config, {
            active: value,
            loaded: null,
            status: null
        }))
            .subscribe(function (result) {
            _this.loading = false;
            _this.__onSearch();
        }, function (err) {
            console.error(err);
        });
    };
    EntriesListComponent.prototype.onDetails = function (config) {
    };
    EntriesListComponent.prototype.onAddClick = function () {
        this.router.navigate(['./master'], { relativeTo: this.route });
    };
    EntriesListComponent.prototype.__onSearch = function () {
        var _this = this;
        this.loading = true;
        __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].of(this.searchParams)
            .map(function (qParams) {
            var q = {
                limit: _this.maxItems,
                skip: +qParams.page && +qParams.page > 0 ? _this.maxItems * (+qParams.page - 1) : 0
            };
            if (qParams && qParams.name && qParams.name.trim) {
                q.where = { name: { regexp: "/^" + qParams.name.trim() + "/i" } };
            }
            return q;
        })
            .switchMap(function (q) { return _this._doSearchQuery(q); })
            .subscribe(function (result) {
            _this.configs = result.configs;
            _this.count = result.count;
            _this.loading = false;
            if (_this.locPager) {
                _this.locPager.total = _this.count;
                _this.locPager.navigateToPage(_this.searchParams.page || 1, false);
            }
        }, function (err) {
            console.error(err);
            _this.loading = false;
        });
    };
    EntriesListComponent.prototype.getConfigMethodsString = function (config) {
        return (config.methods || []).join(', ');
    };
    EntriesListComponent.prototype.getRulesCount = function (config) {
        return config.rules ? config.rules.length : '-';
    };
    EntriesListComponent.prototype.getRulesPath = function (config) {
        return config.rules && config.rules.length > 0 ? config.rules.map(function (r) { return r.path + "\n"; }) : 'n/a';
    };
    EntriesListComponent.prototype.onClone = function (config) {
        var _this = this;
        this.loading = true;
        this._apiConfigApi.clone(config.id)
            .subscribe(function (cloned) {
            _this.loading = false;
            _this.router.navigate(['./master'], { queryParams: { id: cloned._id }, relativeTo: _this.route });
        }, function (err) {
            _this.loading = false;
            console.error(err);
        });
    };
    EntriesListComponent.prototype.onExportClick = function () {
        var _this = this;
        this.loading = true;
        this._apiConfigApi.export()
            .subscribe(function (result) {
            _this.loading = false;
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_7_file_saver__["saveAs"])(result, 'loc.json');
        }, function (err) {
            _this.loading = false;
            console.error(err);
        });
    };
    EntriesListComponent.prototype.toggleRouteReadyToBeInserted = function (route) {
        route.active = !route.active;
    };
    EntriesListComponent.prototype.isDisabledModalImportInsertButton = function () {
        return !(this.importData.routes.find(function (r) { return r.active; }));
    };
    EntriesListComponent.prototype.selectAll = function () {
        this.importData.routes.forEach(function (r) {
            r.active = true;
        });
    };
    EntriesListComponent.prototype.insertSelected = function (importData) {
        var _this = this;
        var routesToImport = (importData.routes || []).filter(function (r) { return r.active; });
        this.loading = true;
        this._apiConfigApi.import({ routes: routesToImport })
            .subscribe(function (result) {
            console.log(result);
            _this.loading = false;
            if (_this.modalRef) {
                _this.modalRef.close();
                _this.__onSearch();
            }
        }, function (err) {
            _this.loading = false;
            console.error(err);
        });
    };
    EntriesListComponent.prototype.showSideMenu = function (config) {
        this.sidebarActive = true;
    };
    return EntriesListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('searchInput'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], EntriesListComponent.prototype, "searchInput", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["e" /* PagerComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["e" /* PagerComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["e" /* PagerComponent */]) === "function" && _b || Object)
], EntriesListComponent.prototype, "pager", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__shared_components__["c" /* LocPagerComponent */]),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__shared_components__["c" /* LocPagerComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_components__["c" /* LocPagerComponent */]) === "function" && _c || Object)
], EntriesListComponent.prototype, "locPager", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('importPreview'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _d || Object)
], EntriesListComponent.prototype, "content", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('uploadEl'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _e || Object)
], EntriesListComponent.prototype, "uploadElRef", void 0);
EntriesListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'api-list',
        template: __webpack_require__("../../../../../src/app/+entries/entries-list/entries-list.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-list/entries-list.scss")],
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('initial', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('start', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    visibility: 'hidden',
                    opacity: 0
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('complete', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    visibility: 'visible',
                    opacity: 1
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('start => complete', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('300ms linear')
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4__core__["j" /* ServiceApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["j" /* ServiceApi */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_4__core__["e" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["e" /* LoopBackAuth */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_4__core__["o" /* ApiConfigApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["o" /* ApiConfigApi */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_11__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_11__ngrx_store__["b" /* Store */]) === "function" && _o || Object])
], EntriesListComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
//# sourceMappingURL=entries-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/entries-list.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".__entry-sidebar {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start; }\n  .__entry-sidebar > loc-switch {\n    margin: auto; }\n  .__entry-sidebar > label {\n    color: #555; }\n\n.__entry-link {\n  text-align: left;\n  white-space: nowrap;\n  overflow: hidden !important;\n  text-overflow: ellipsis;\n  display: inline-block;\n  max-width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-list/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_list_component__ = __webpack_require__("../../../../../src/app/+entries/entries-list/entries-list.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__entries_list_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__entries_list_indicator_component__ = __webpack_require__("../../../../../src/app/+entries/entries-list/entries-list-indicator.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entries_list_details_component__ = __webpack_require__("../../../../../src/app/+entries/entries-list/entries-list-details.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENTRIES_LIST_COMPONENTS; });





var ENTRIES_LIST_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__entries_list_component__["a" /* EntriesListComponent */],
    __WEBPACK_IMPORTED_MODULE_1__entries_list_indicator_component__["a" /* EntriesIndicatorComponent */],
    __WEBPACK_IMPORTED_MODULE_2__entries_list_details_component__["a" /* EntriesDetailsComponent */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-base/entries-wizard-base.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"l-wizard\">\r\n    <div class=\"l__page-header\">\r\n        <span class=\"title\"><a routerLink='/entries'>Entries</a> / wizard</span>\r\n    </div>\r\n    <section class=\"l__page-body\">\r\n        <ui-tabs #tab (save)='onDone()'>\r\n            <ui-pane id='general' title='Config' [valid]='(stepGeneral.validation | async)'>\r\n                <step-general #stepGeneral (next)=\"tab.goTo($event)\" (save)='onDone()' [submitted]='submitted'> </step-general>\r\n            </ui-pane>\r\n            <ui-pane id='plugins' title='Routes' [valid]='(stepPlugins.validation | async)'>\r\n                <step-plugins #stepPlugins (next)=\"tab.goTo($event)\" (save)='onDone()' [submitted]='submitted'></step-plugins>\r\n            </ui-pane>           \r\n            <ui-pane id='healthcheck' title='Health' [valid]='true'>\r\n                <step-healthcheck (next)=\"tab.goTo($event)\" (save)=\"onDone()\"></step-healthcheck>\r\n            </ui-pane>\r\n            <ui-pane id='preview' title='Test' [valid]='true'>\r\n                <step-preview (next)=\"tab.goTo($event)\" (save)='onDone()'></step-preview>\r\n            </ui-pane>\r\n        </ui-tabs>\r\n    </section>\r\n    <ng-template ngbModalContainer></ng-template>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-base/entries-wizard-base.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("../../../../../src/app/+entries/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesWizardBaseComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var EntriesWizardBaseComponent = (function () {
    function EntriesWizardBaseComponent(router, _activatedRoute, _configActions, _masterActions, apiConfigApi, store) {
        this.router = router;
        this._activatedRoute = _activatedRoute;
        this._configActions = _configActions;
        this._masterActions = _masterActions;
        this.apiConfigApi = apiConfigApi;
        this.store = store;
        this.loading = true;
        this.submitted = false;
        this.validation = {};
        this.config = {};
    }
    EntriesWizardBaseComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.validationSub_n = this.store
            .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_6__core_reducers__["j" /* getValidationState */])())
            .subscribe(function (validation) {
            _this.validation = validation;
        });
        this.masterStateSub_n = this.store
            .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_6__core_reducers__["g" /* getMasterState */])())
            .subscribe(function (config) {
            _this.config = config;
        });
        this.queryRouteSub = this._activatedRoute
            .params
            .subscribe(function (params) {
            Promise.resolve().then(function () {
                var tab = params["t"];
                if (tab === 'routes') {
                    _this.tab.goTo('plugins');
                }
                else {
                    _this.tab.goTo('general');
                }
            });
        });
    };
    EntriesWizardBaseComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.queryRouteSub = this._activatedRoute
            .queryParams
            .flatMap(function (params) {
            _this.id = params["id"];
            return _this.id
                ? _this.apiConfigApi.findById(_this.id)
                : __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"].of(new __WEBPACK_IMPORTED_MODULE_2__core__["u" /* Config */]());
        })
            .subscribe(function (config) {
            _this.store.dispatch(_this._configActions.setConfig(config));
            _this.store.dispatch(_this._masterActions.setConfig(config));
        });
    };
    EntriesWizardBaseComponent.prototype.ngOnDestroy = function () {
        if (this.queryRouteSub)
            this.queryRouteSub.unsubscribe();
        if (this.masterStateSub_n)
            this.masterStateSub_n.unsubscribe();
        if (this.validationSub_n)
            this.validationSub_n.unsubscribe();
    };
    EntriesWizardBaseComponent.prototype.onDone = function () {
        var _this = this;
        this.submitted = true;
        for (var KEY in this.validation) {
            if (!this.validation[KEY]) {
                this.tab.goTo(KEY);
                return;
            }
        }
        var plugins = this.config.plugins.slice();
        plugins = plugins.map(function (plugin) {
            return {
                displayName: plugin.displayName,
                description: plugin.description || '',
                name: plugin.name,
                order: plugin.order,
                settings: plugin.settings,
                dependencies: plugin.dependencies
            };
        });
        var proxy = this.config.proxy;
        var rules = this.config.rules;
        var health = this.config.health;
        var conditions = this.config.conditions;
        var apiConfig = Object.assign(this.config, { plugins: plugins, proxy: proxy, rules: rules, health: health, conditions: conditions });
        console.log("Saving configuration", apiConfig);
        this.apiConfigApi
            .updateOrCreate(apiConfig)
            .subscribe(function (result) {
            _this.router.navigate(['/']);
        }, function (err) {
            // notify
        });
    };
    return EntriesWizardBaseComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3__shared__["b" /* UiTabs */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__shared__["b" /* UiTabs */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__shared__["b" /* UiTabs */]) === "function" && _a || Object)
], EntriesWizardBaseComponent.prototype, "tab", void 0);
EntriesWizardBaseComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "api-master",
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-base/entries-wizard-base.component.tmpl.html")
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_7__core_actions__["g" /* ConfigActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_actions__["g" /* ConfigActions */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_actions__["f" /* MasterActions */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__core__["o" /* ApiConfigApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["o" /* ApiConfigApi */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */]) === "function" && _g || Object])
], EntriesWizardBaseComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=entries-wizard-base.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-base/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_wizard_base_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-base/entries-wizard-base.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__entries_wizard_base_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-general/entries-wizard-step-general.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"l-step\">\r\n    <div class=\"l-step__card\">\r\n        <div class='l-step__header'>\r\n            <h2>General details</h2>\r\n            <button class='button button--icon help'>\r\n                <i class=\"icon-help-circled\"></i>\r\n            </button>\r\n        </div>\r\n        <loader [active]='loading'></loader>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-10 col-lg-8 col-sm-12\">\r\n                <form [formGroup]=\"form\" class=\"form-horizontal\">                   \r\n                    <div class='m-form__group'>\r\n                        <loc-input-text type=\"text\" label=\"Name\" [(ngModel)]='apiConfig.name' placeholder=\"Name\" formControlName=\"name\" required>\r\n                        </loc-input-text>\r\n                        <show-error *ngIf=\"submitted\" [control]=\"form.get('name')\" [options]=\"{'required': 'Name field is required'}\"></show-error>\r\n                    </div>\r\n                    <div class='m-form__group'>\r\n                        <loc-input-text type=\"text\" label=\"Url\" [(ngModel)]='apiConfig.entry' placeholder=\"Url\" formControlName=\"entry\" required>\r\n                        </loc-input-text>\r\n                        <show-error *ngIf=\"submitted\" [control]=\"form.get('entry')\" [options]=\"{'required': 'Entry field is required', 'startWith': 'Should start with /'}\"></show-error>\r\n                    </div>\r\n                    <div class='m-form__group'>\r\n                        <loc-input-textArea label=\"Description\" [(ngModel)]='apiConfig.description' placeholder=\"Description\" formControlName=\"description\">\r\n                        </loc-input-textArea>\r\n                    </div>                  \r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class='l-steps__buttons'>\r\n        <button type=\"button\" (click)=\"onSubmit()\" class=\"button button--primary l-master__next\">\r\n            <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>  Next         \r\n        </button>\r\n        <button type=\"button\" (click)=\"onSave()\" class=\"button button--primary l-master__next\">\r\n            <i class=\"icon-floppy-o\" aria-hidden=\"true\"></i>  Save\r\n        </button>\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-general/entries-wizard-step-general.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesWizardStepGeneral; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EntriesWizardStepGeneral = (function () {
    function EntriesWizardStepGeneral(_store, _masterActions, _validationActions, _fb) {
        this._store = _store;
        this._masterActions = _masterActions;
        this._validationActions = _validationActions;
        this._fb = _fb;
        this.next = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.save = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.validation = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["BehaviorSubject"](true);
        this.apiConfig = {
            methods: []
        };
        this.loading = false;
        this.submitted = false;
        this.valid = false;
        this.form = _fb.group({
            name: ["", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required],
            entry: ["", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required],
            description: [""]
        });
    }
    EntriesWizardStepGeneral.prototype.ngOnInit = function () {
        var _this = this;
        this.validation.next(true);
        this.loading = true;
        this.form
            .valueChanges
            .distinctUntilChanged()
            .subscribe(function (value) {
            for (var KEY in value) {
                if (value[KEY] == undefined)
                    return;
            }
            _this._store.dispatch(_this._masterActions.setGeneralInfoData(value));
        });
        this.form
            .statusChanges
            .subscribe(function (value) {
            Promise.resolve().then(function () {
                console.log(_this.form.valid);
                if (_this.valid !== _this.form.valid) {
                    _this.valid = _this.form.valid;
                    _this._store.dispatch(_this._validationActions.setValidity({ general: _this.valid }));
                    _this.validation.next(_this.valid);
                }
            });
        });
        this.configStateSub_n = this._store
            .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4__core_reducers__["i" /* getConfigState */])())
            .subscribe(function (config) {
            if (!config)
                return;
            Promise.resolve()
                .then(function () {
                _this.loading = false;
                _this.apiConfig = config;
            });
        });
    };
    EntriesWizardStepGeneral.prototype.ngOnDestroy = function () {
        if (this.configStateSub_n) {
            this.configStateSub_n.unsubscribe();
        }
    };
    EntriesWizardStepGeneral.prototype.onSubmit = function () {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ general: this.form.valid }));
        this.validation.next(this.form.valid);
        if (this.form.valid) {
            this.next.emit('plugins');
        }
    };
    EntriesWizardStepGeneral.prototype.onSave = function () {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ general: this.form.valid }));
        this.validation.next(this.form.valid);
        if (this.form.valid) {
            this.save.emit('Done');
        }
    };
    return EntriesWizardStepGeneral;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EntriesWizardStepGeneral.prototype, "next", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], EntriesWizardStepGeneral.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_rxjs__["BehaviorSubject"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_rxjs__["BehaviorSubject"]) === "function" && _c || Object)
], EntriesWizardStepGeneral.prototype, "validation", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], EntriesWizardStepGeneral.prototype, "submitted", void 0);
EntriesWizardStepGeneral = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'step-general',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-general/entries-wizard-step-general.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-general/stepGeneral.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["b" /* Store */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_actions__["f" /* MasterActions */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__core_actions__["h" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core_actions__["h" /* ValidationActions */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _g || Object])
], EntriesWizardStepGeneral);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=entries-wizard-step-general.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-general/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_general_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-general/entries-wizard-step-general.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_general_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-general/stepGeneral.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "form > .form-group:last-child {\n  margin-bottom: 0 !important; }\n\n:host {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/entries-wizard-step-healthcheck.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"l-step\">\r\n    <div class=\"l-step__card\">\r\n        <div class='l-step__header'>\r\n            <h2>Healthcheck settings</h2>\r\n            <button class='button button--icon help'>\r\n                <i class=\"icon-help-circled\"></i>\r\n            </button>\r\n        </div>\r\n        <loader [active]='loading'></loader>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-9 health\">\r\n                <healthcheck-rule *ngFor='let rule of healthcheck' [healthCheckRule]='rule' (change)='onChange($event)' (remove)='onRemoveRule($event)'></healthcheck-rule>\r\n                <button class='button button--secondary' (click)='onAddNew()'>\r\n                     Add \r\n                </button>\r\n            </div>\r\n            <div class='col-sm-3'>\r\n                <div class='row'>\r\n                    <div class=\"col-sm-12\" style='position: relative;'>\r\n                        <form [formGroup]='healthForm' class='health__form' role='form'>\r\n                            <div class=\"form-group\">\r\n                                <span>Active</span>\r\n                                <loc-switch class='health__switch' [(ngModel)]='active' formControlName=\"active\">\r\n                                </loc-switch>\r\n                            </div>\r\n                            <div class=\"form-group\">\r\n                                <div class='m-form__group'>\r\n                                    <loc-input-text type=\"number\" label=\"Interval\" formControlName=\"interval\" [(ngModel)]='interval' id='interval' type=\"number\"\r\n                                        required>\r\n                                    </loc-input-text>\r\n                                    <show-error *ngIf=\"healthForm.dirty\" [control]=\"healthForm.get('interval')\" [options]=\"{'required': 'Field is required'}\">\r\n                                    </show-error>\r\n                                </div>\r\n                            </div>\r\n                        </form>\r\n                        <loader [active]='checking' [delay]='200'></loader>\r\n                        <div class='health__checkresult' [ngClass]='{ok: healthcheckresult && healthcheckresult.healthy, nok: healthcheckresult && !healthcheckresult.healthy}'>{{ healthcheckresult?.healthy ? 'OK' : 'NOT HEALTHY' }}</div>\r\n                        <button class='button button--secondary health__trynow' (click)='onTryNow()'>\r\n                            Try now\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class='l-steps__buttons'>\r\n        <button type=\"button\" (click)=\"onSubmit()\" class=\"button button--primary l-master__next\">\r\n            <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>  Next         \r\n        </button>\r\n        <button type=\"button\" (click)=\"onSave()\" class=\"button button--primary l-master__next\">\r\n        <i class=\"icon-floppy-o\" aria-hidden=\"true\"></i>  Save\r\n    </button>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/entries-wizard-step-healthcheck.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesWizardStepHealthcheck; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EntriesWizardStepHealthcheck = (function () {
    function EntriesWizardStepHealthcheck(_store, _masterActions, _validationActions, _configService, _fb) {
        this._store = _store;
        this._masterActions = _masterActions;
        this._validationActions = _validationActions;
        this._configService = _configService;
        this._fb = _fb;
        this.save = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.next = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.validation = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.healthcheck = [];
        this.loading = false;
        this.submitted = false;
        this.checking = false;
        this.healthForm = this._fb.group({
            interval: [],
            active: []
        });
    }
    EntriesWizardStepHealthcheck.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = true;
        this.healthForm.valueChanges
            .subscribe(function (value) {
            _this._apply();
        });
    };
    EntriesWizardStepHealthcheck.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.configStateSub_n = this._store
            .let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__core_reducers__["i" /* getConfigState */])())
            .subscribe(function (config) {
            if (!config)
                return;
            Promise.resolve().then(function () {
                _this.loading = false;
                _this.config = config;
                _this.healthcheck = config.health ? config.health.healthCheck || [] : [];
                _this.active = config.health ? config.health.active : false;
                _this.interval = config.health ? config.health.interval : 5000;
            });
        });
    };
    EntriesWizardStepHealthcheck.prototype.ngOnDestroy = function () {
        if (this.configStateSub_n) {
            this.configStateSub_n.unsubscribe();
        }
    };
    EntriesWizardStepHealthcheck.prototype.onRemoveRule = function (rule) {
        var ind = this.healthcheck.indexOf(rule);
        this.healthcheck.splice(ind, 1);
        this._apply();
    };
    EntriesWizardStepHealthcheck.prototype.onSave = function () {
        this.submitted = true;
        this.save.emit();
    };
    EntriesWizardStepHealthcheck.prototype.onSubmit = function () {
        this.submitted = true;
        this.next.emit('preview');
    };
    EntriesWizardStepHealthcheck.prototype.onChange = function () {
        this._apply();
    };
    EntriesWizardStepHealthcheck.prototype._apply = function () {
        this._store.dispatch(this._masterActions.setHealthCheckData(Object.assign({}, { healthCheck: this.healthcheck.slice(), active: this.active, interval: this.interval })));
        this._store.dispatch(this._validationActions.setValidity({ health: true }));
    };
    EntriesWizardStepHealthcheck.prototype.onTryNow = function () {
        var _this = this;
        this.checking = true;
        this._configService.healthcheck({ healthCheck: this.healthcheck })
            .subscribe(function (result) {
            _this.checking = false;
            _this.healthcheckresult = result;
        }, function (err) {
            _this.checking = false;
            console.error(err);
        });
    };
    EntriesWizardStepHealthcheck.prototype.onAddNew = function () {
        this.healthcheck.push(new __WEBPACK_IMPORTED_MODULE_4__core__["h" /* HealthCheck */]({ target: this.config.proxy.target || 'localhost', withPath: '/healthcheck', method: 'HEAD', interval: 2000 }));
    };
    return EntriesWizardStepHealthcheck;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EntriesWizardStepHealthcheck.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], EntriesWizardStepHealthcheck.prototype, "next", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], EntriesWizardStepHealthcheck.prototype, "validation", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], EntriesWizardStepHealthcheck.prototype, "submitted", void 0);
EntriesWizardStepHealthcheck = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'step-healthcheck',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/entries-wizard-step-healthcheck.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/stephealthcheck.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["b" /* Store */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__core__["s" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["s" /* MasterActions */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__core__["t" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["t" /* ValidationActions */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4__core__["o" /* ApiConfigApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["o" /* ApiConfigApi */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _h || Object])
], EntriesWizardStepHealthcheck);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=entries-wizard-step-healthcheck.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/healthcheck-rule.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div #panel class=\"__panel\" [class.expanded]=\"state=='expanded'\">\r\n    <div class=\"__header\" [@headerCollapse]=\"state\">\r\n        <button class=\"button--icon indicator-button __header__indicator\" [@iconState]=\"state\" (click)='toggle()'> \r\n              <svg height=\"20px\" id=\"Layer_1\" style=\"enable-background:new 0 0 512 512;\" \r\n                version=\"1.1\" viewBox=\"0 0 512 512\" \r\n                width=\"20px\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" \r\n                xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n                <path d=\"M402 311c0-5-2-9-5-13l-128-128c-4-4-8-5-13-5-5 0-9 1-13 5l-128 128c-3 4-5 8-5 13 0 5 2 9 5 13 4 3 8 5 13 5l256 0c5 0 9-2 13-5 3-4 5-8 5-13z\"/>\r\n              </svg>\r\n        </button>\r\n        <a href (click)='toggle()'>{{healthCheckRule.method}}  {{healthCheckRule.target}}{{healthCheckRule.withPath}} </a>\r\n        <button class='button button--icon __header__trash' (click)='remove.emit(healthCheckRule)'> \r\n                                <span class=\"icon-delete-garbage-streamline red-on-hover\"></span>\r\n                            </button>\r\n    </div>\r\n    <div #ruleFormContainer class=\"__details\" [@detailsCollapse]=\"state\">\r\n        <form [formGroup]='healthRuleForm' class='health__form' role='form'>\r\n            <div class=\"row\">\r\n                <div class=\"form-group col-xs-3 col-md-3\">\r\n                    <loc-select [items]=\"['GET','HEAD']\" label='Method' formControlName=\"method\" id=\"method\" [(ngModel)]='healthCheckRule.method'\r\n                        required>\r\n                    </loc-select>\r\n                </div>\r\n                <div class=\"form-group col-xs-9 col-md-9\">\r\n                    <loc-input-text label='Target' formControlName=\"target\" [(ngModel)]='healthCheckRule.target' id='target' required validateUrl>\r\n                    </loc-input-text>\r\n\r\n                    <show-error *ngIf=\"healthRuleForm.dirty\" [control]=\"healthRuleForm.get('target')\" [options]=\"{'required': 'Field is required', 'validateUrl': 'Wrong url format'}\">\r\n                    </show-error>\r\n                </div>\r\n                <div class=\"form-group col-xs-6 col-md-6\">\r\n                    <loc-input-text label=\"Timeout\" formControlName=\"timeout\" [(ngModel)]='healthCheckRule.timeout' id='timeout' type=\"number\" required>\r\n                    </loc-input-text>\r\n                    <show-error *ngIf=\"healthRuleForm.dirty\" [control]=\"healthRuleForm.get('timeout')\" [options]=\"{'required': 'Field is required'}\">\r\n                    </show-error>\r\n                </div>\r\n                <div class=\"form-group col-xs-6 col-md-6\">\r\n                    <loc-input-text label='Path' formControlName=\"withPath\" [(ngModel)]='healthCheckRule.withPath' id='withPath' required>\r\n                    </loc-input-text>\r\n                    <show-error *ngIf=\"healthRuleForm.dirty\" [control]=\"healthRuleForm.get('withPath')\" [options]=\"{'required': 'Field is required'}\">\r\n                    </show-error>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/healthcheck-rule.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_models__ = __webpack_require__("../../../../../src/app/core/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthcheckRule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HealthcheckRule = (function () {
    function HealthcheckRule(_store, _masterActions, _validationActions, _fb) {
        this._store = _store;
        this._masterActions = _masterActions;
        this._validationActions = _validationActions;
        this._fb = _fb;
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.remove = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.state = 'collapsed';
        this.healthRuleForm = this._fb.group({
            target: [],
            withPath: [],
            method: [],
            timeout: []
        });
    }
    Object.defineProperty(HealthcheckRule.prototype, "healthCheckRule", {
        get: function () {
            return this._healthCheckRule;
        },
        set: function (v) {
            if (v)
                this._healthCheckRule = v;
        },
        enumerable: true,
        configurable: true
    });
    HealthcheckRule.prototype.ngOnInit = function () {
        var _this = this;
        this.healthRuleForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe(function (value) {
            for (var KEY in value) {
                if (value[KEY] == undefined)
                    return;
            }
            if (_this.healthRuleForm.valid) {
                _this.change.emit(value);
            }
        });
    };
    HealthcheckRule.prototype.close = function () {
    };
    HealthcheckRule.prototype.toggle = function () {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    };
    return HealthcheckRule;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], HealthcheckRule.prototype, "change", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], HealthcheckRule.prototype, "remove", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_models__["c" /* HealthCheck */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_models__["c" /* HealthCheck */]) === "function" && _c || Object),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_models__["c" /* HealthCheck */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_models__["c" /* HealthCheck */]) === "function" && _d || Object])
], HealthcheckRule.prototype, "healthCheckRule", null);
HealthcheckRule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'healthcheck-rule',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/healthcheck-rule.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/healthcheck-rule.scss")],
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('detailsCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '*',
                    visibility: 'visible',
                    opacity: '1'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '0px',
                    visibility: 'hidden',
                    opacity: '0'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms cubic-bezier(0.4,0.0,0.2,1)')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms cubic-bezier(0.4,0.0,0.2,1)'))
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('headerCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    background: '#fafafa',
                    borderColor: 'red'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    background: '#fff'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('* => *', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms linear'))
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('iconState', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(0deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(180deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.4s ease-in')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.4s ease-out')),
            ])
        ]
    }),
    __metadata("design:paramtypes", [typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["b" /* Store */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_actions__["f" /* MasterActions */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4__core_actions__["h" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_actions__["h" /* ValidationActions */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _h || Object])
], HealthcheckRule);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=healthcheck-rule.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/healthcheck-rule.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".health__form {\n  padding-right: 15px;\n  padding-left: 15px;\n  padding-top: 10px; }\n\n.__panel {\n  border: 1px dotted #e8e8e8;\n  margin-bottom: 15px; }\n  .__panel.expanded {\n    border-color: #cccccc; }\n    .__panel.expanded .__details {\n      padding-top: 10px; }\n\n.__header {\n  position: relative;\n  padding: 10px 10px 10px 35px; }\n  .__header__trash {\n    right: 10px;\n    position: absolute;\n    top: 10px;\n    font-size: 1.2rem; }\n  .__header__indicator {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    color: #a6b0b7;\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg); }\n    .__header__indicator > svg {\n      fill: #a6b0b7; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_healthcheck_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/entries-wizard-step-healthcheck.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__healthcheck_rule_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/healthcheck-rule.component.ts");
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HEALTHCHECK_COMPONENTS; });



var HEALTHCHECK_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_1__healthcheck_rule_component__["a" /* HealthcheckRule */], __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_healthcheck_component__["a" /* EntriesWizardStepHealthcheck */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-healthcheck/stephealthcheck.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n\n.health__switch {\n  float: right;\n  margin-right: 5px; }\n\n.health__checkresult {\n  padding: 20px;\n  color: white;\n  text-align: center;\n  font-size: 1.5rem;\n  margin-bottom: 15px;\n  display: none; }\n  .health__checkresult.ok {\n    background: green;\n    display: block; }\n  .health__checkresult.nok {\n    background: red;\n    display: block; }\n\n.health__trynow {\n  width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-option.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<loc-dropdown [title]='__getTitle()'\n    (remove)=\"remove.emit(condition)\">\n    <form [formGroup]='optionForm' class='health__form' role='form'>\n        <div class=\"col-xs-12\">\n            <div class=\"row\">\n                <div class=\"form-group col-xs-12 col-sm-3\">\n                    <loc-input-text label=\"Id\" formControlName=\"name\" [(ngModel)]='condition.name' id='name' type=\"text\" required>\n                    </loc-input-text>\n                    <show-error *ngIf=\"optionForm.dirty\" [control]=\"optionForm.get('name')\" [options]=\"{'required': 'Field is required'}\">\n                    </show-error>\n                </div>\n                <div class=\"form-group col-xs-12 col-sm-9\">\n                    <loc-select [items]=\"['Header','Query','Context']\" label='Data source' formControlName=\"source\" id=\"source\" [(ngModel)]='condition.source'\n                        required>\n                    </loc-select>\n                    <show-error *ngIf=\"optionForm.dirty\" [control]=\"optionForm.get('source')\" [options]=\"{'required': 'Field is required'}\">\n                    </show-error>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-xs-12\">\n            <div class=\"row\">\n                <div class=\"form-group col-xs-12 col-sm-4\">\n                    <loc-input-text label=\"Key\" formControlName=\"key\" [(ngModel)]='condition.key' id='key' type=\"text\" required>\n                    </loc-input-text>\n                    <show-error *ngIf=\"optionForm.dirty\" [control]=\"optionForm.get('key')\" [options]=\"{'required': 'Field is required'}\">\n                    </show-error>\n                </div>\n                <div class=\"form-group col-xs-12 col-sm-4\">\n                    <loc-select [items]=\"['eq','neq']\" label='Operation' formControlName=\"operation\" id=\"method\" [(ngModel)]='condition.operation'\n                        required>\n                    </loc-select>\n                </div>\n                <div class=\"form-group col-xs-12 col-sm-4\">\n                    <loc-input-text label=\"Value\" formControlName=\"value\" [(ngModel)]='condition.value' id='key' type=\"text\" required>\n                    </loc-input-text>\n                    <show-error *ngIf=\"optionForm.dirty\" [control]=\"optionForm.get('value')\" [options]=\"{'required': 'Field is required'}\">\n                    </show-error>\n                </div>\n            </div>\n        </div>\n    </form>\n</loc-dropdown>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-option.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_models__ = __webpack_require__("../../../../../src/app/core/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntryOptionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EntryOptionComponent = (function () {
    function EntryOptionComponent(_store, _masterActions, _validationActions, _fb) {
        this._store = _store;
        this._masterActions = _masterActions;
        this._validationActions = _validationActions;
        this._fb = _fb;
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.remove = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.state = 'collapsed';
        this.optionForm = this._fb.group({
            source: [],
            key: [],
            value: [],
            operation: [],
            name: []
        });
    }
    Object.defineProperty(EntryOptionComponent.prototype, "condition", {
        get: function () {
            return this._condition;
        },
        set: function (v) {
            if (v)
                this._condition = v;
        },
        enumerable: true,
        configurable: true
    });
    EntryOptionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.optionForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe(function (value) {
            for (var KEY in value) {
                if (value[KEY] == undefined)
                    return;
            }
            if (_this.optionForm.valid) {
                _this.change.emit(value);
            }
        });
    };
    EntryOptionComponent.prototype.close = function () {
    };
    EntryOptionComponent.prototype.toggle = function () {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    };
    EntryOptionComponent.prototype.__getTitle = function () {
        if (this.condition && this.condition.source && this.condition.key && this.condition.value) {
            var base = this.condition.source + " " + this.condition.key + " ";
            return this.condition.operation == "eq" ? base + "equal " + this.condition.value : base + "not equal " + this.condition.value;
        }
        else {
            return "Set condition";
        }
    };
    return EntryOptionComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EntryOptionComponent.prototype, "change", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], EntryOptionComponent.prototype, "remove", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__core_models__["k" /* EntryOption */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_models__["k" /* EntryOption */]) === "function" && _c || Object),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core_models__["k" /* EntryOption */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core_models__["k" /* EntryOption */]) === "function" && _d || Object])
], EntryOptionComponent.prototype, "condition", null);
EntryOptionComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'entry-condition',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-option.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-option.scss")],
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('detailsCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '*',
                    visibility: 'visible',
                    opacity: '1'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '0px',
                    visibility: 'hidden',
                    opacity: '0'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms cubic-bezier(0.4,0.0,0.2,1)')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms cubic-bezier(0.4,0.0,0.2,1)'))
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('headerCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    background: '#fafafa',
                    borderColor: 'red'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    background: '#fff'
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('* => *', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('250ms linear'))
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('iconState', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(0deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(180deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.4s ease-in')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.4s ease-out')),
            ])
        ]
    }),
    __metadata("design:paramtypes", [typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["b" /* Store */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_actions__["f" /* MasterActions */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4__core_actions__["h" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core_actions__["h" /* ValidationActions */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _h || Object])
], EntryOptionComponent);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=entries-option.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-option.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins-rule.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_models__ = __webpack_require__("../../../../../src/app/core/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared__ = __webpack_require__("../../../../../src/app/+entries/shared/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StepPluginsRule; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StepPluginsRule = (function () {
    function StepPluginsRule() {
        this._allPlugins = [];
        this.onSave = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.state = 'collapsed';
        this.tab = 'proxy';
        this.selected = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.deleted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.added = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.activated = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.pipePlugins = [];
    }
    Object.defineProperty(StepPluginsRule.prototype, "allPlugins", {
        get: function () {
            return this._allPlugins;
        },
        set: function (value) {
            if (value && Array.isArray(value)) {
                this._allPlugins = value.slice();
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(StepPluginsRule.prototype, "rule", {
        get: function () {
            return this._rule;
        },
        set: function (value) {
            if (value) {
                this._rule = Object.assign({}, value);
                if (this._rule.plugins && this._rule.plugins.length > 0) {
                    this.pipePlugins = this._rule.plugins.map(function (p) { return new __WEBPACK_IMPORTED_MODULE_1__core_models__["j" /* Plugin */](p); });
                    this.setPluginActive(this.pipePlugins[0]);
                }
                if (!this._rule.proxy) {
                    this._rule.proxy = new __WEBPACK_IMPORTED_MODULE_1__core_models__["i" /* Proxy */]({
                        target: 'http://localhost:80',
                        active: false,
                        prependPath: true,
                        secure: false,
                        ignorePath: false,
                        changeOrigin: false,
                        toProxy: false,
                        xfwd: false,
                        auth: '',
                        ws: false,
                        proxyTimeout: 1000
                    });
                }
                if (!this._rule.methods) {
                    this._rule.methods = [];
                }
                if (!this._rule.plugins) {
                    this._rule.plugins = [];
                }
                if (!this._rule.conditions) {
                    this._rule.conditions = [];
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    StepPluginsRule.prototype.toggle = function () {
        if (this.state == 'collapsed') {
            this.state = 'expanded';
            return;
        }
        if (this.state == 'expanded') {
            this.state = 'collapsed';
        }
    };
    StepPluginsRule.prototype.savePath = function () { };
    StepPluginsRule.prototype.transferDataSuccess = function (event) {
        var plugin = new __WEBPACK_IMPORTED_MODULE_1__core_models__["j" /* Plugin */](Object.assign({}, event.dragData, { settings: {}, dependencies: {} }));
        this.pipePlugins = this.pipePlugins.concat([plugin]);
        this.setPluginActive(plugin);
    };
    StepPluginsRule.prototype.setPluginActive = function (plugin) {
        this.editable = Object.assign({}, plugin);
        this.activePlugin = plugin;
    };
    StepPluginsRule.prototype.deletePlugin = function (plugin) {
        var ind = this.pipePlugins.indexOf(plugin);
        this.pipePlugins.splice(ind, 1);
        if (this.pipePlugins.length > 0) {
            this.setPluginActive(this.pipePlugins[0]);
        }
        else {
            this.activePlugin = undefined;
            this.editable = undefined;
        }
    };
    StepPluginsRule.prototype.pluginValueChanged = function (value) {
        this.activePlugin.settings = __assign({}, value.settings);
        this.activePlugin.dependencies = __assign({}, value.dependencies);
        this.rule.plugins = this.pipePlugins;
    };
    StepPluginsRule.prototype.pluginValidationChanged = function (isValid) {
        //    this.activePlugin.valid = isValid;
    };
    // save() {
    //     this.rule.plugins = this.pipePlugins;
    //     this.onSave.emit(this.rule);
    // }
    StepPluginsRule.prototype.setFlowView = function () {
        this.tab = 'flow';
    };
    StepPluginsRule.prototype.setProxyView = function () {
        this.tab = 'proxy';
    };
    StepPluginsRule.prototype.setConditionsView = function () {
        this.tab = 'conditions';
    };
    StepPluginsRule.prototype.setPolicyView = function () {
        this.tab = 'policy';
    };
    StepPluginsRule.prototype.onAddNewConditions = function () {
        this._rule.conditions.push(new __WEBPACK_IMPORTED_MODULE_1__core_models__["k" /* EntryOption */]());
    };
    StepPluginsRule.prototype.onDeleteCondition = function (condition) {
        var ind = this._rule.conditions.indexOf(condition);
        if (ind != -1)
            this._rule.conditions.splice(ind, 1);
    };
    StepPluginsRule.prototype.onConditionsChanged = function (event) {
    };
    StepPluginsRule.prototype.onPolicyChanged = function (policy) {
        this._rule.policy = policy;
        console.log(policy);
    };
    return StepPluginsRule;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], StepPluginsRule.prototype, "activePlugins", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], StepPluginsRule.prototype, "allPlugins", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], StepPluginsRule.prototype, "basicPath", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], StepPluginsRule.prototype, "onSave", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], StepPluginsRule.prototype, "selected", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], StepPluginsRule.prototype, "deleted", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _d || Object)
], StepPluginsRule.prototype, "added", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _e || Object)
], StepPluginsRule.prototype, "activated", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__shared__["a" /* PluginSet */]),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__shared__["a" /* PluginSet */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__shared__["a" /* PluginSet */]) === "function" && _f || Object)
], StepPluginsRule.prototype, "pluginsSet", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], StepPluginsRule.prototype, "rule", null);
StepPluginsRule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'plugins-set-rule',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins-rule.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/set-plugins-rule.scss"), __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/plugins-rule-editor.scss")],
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('iconState', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(0deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ transform: 'rotate(45deg)' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.15s ease-in')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.15s ease-out')),
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('buttonState', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ boxShadow: 'rgba(0, 0, 0, 0.05) 0px 0px 15px 1px inset' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({ boxShadow: 'none' })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('expanded => collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.15s ease-out')),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('collapsed => expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('.15s ease-in')),
            ]),
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('detailsCollapse', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('expanded', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '*',
                    opacity: 1
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('collapsed', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    height: '0px',
                    opacity: 0
                })),
            ]),
        ]
    }),
    __metadata("design:paramtypes", [])
], StepPluginsRule);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=entries-wizard-step-plugins-rule.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins-rule.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class='l-rule'>\r\n    <div class=\"row tab\" style='flex:0 1 auto;'>\r\n        <div class='col-sm-12'>\r\n            <ul class=\"nav nav-pills nav-justified custom-nav-pills __commands\">\r\n                <li [class.active]=\"tab=='proxy'\" class=\"blue-custom-slide\" (click)='setProxyView()'>\r\n                    <span class=\"strong\">General</span>\r\n                </li>\r\n                <li [class.active]=\"tab=='flow'\" class=\"orange-custom-slide\" (click)='setFlowView()'>\r\n                    <span class=\"strong\">Flow</span>\r\n                </li>\r\n                <li [class.active]=\"tab=='conditions'\" class=\"orange-custom-slide\" (click)='setConditionsView()'>\r\n                    <span class=\"strong\">Conditions</span>\r\n                </li>\r\n                <li [class.active]=\"tab=='policy'\" class=\"orange-custom-slide\" (click)='setPolicyView()'>\r\n                    <span class=\"strong\">Policy</span>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"tab=='proxy'\" style='flex:1; overflow: auto;'>\r\n        <div class=\"plugins col-sm-12\">\r\n            <div class=\"proxy__container\">\r\n                <rule-form [(rule)]='rule' #proxyForm></rule-form>\r\n            </div>\r\n        </div>       \r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"tab=='flow'\" style='flex:1; overflow: auto;'>\r\n        <div class=\"plugins col-sm-12\">\r\n            <div class=\"plugins__container\">\r\n                <div class=\"plugins__header\">\r\n                    <button class='button button--light plugins__add-button' [@buttonState]='state' (click)='toggle()'>                                    \r\n                            <svg [@iconState]='state' xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"20\" height=\"20\" viewBox=\"0 0 20 20\">\r\n                                <path fill=\"#a6b0b7\" d=\"M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z\"/>\r\n                            </svg>                \r\n                    </button>\r\n                    <div class=\"plugins__details\" [@detailsCollapse]=\"state\">\r\n                        <div class=\"plugins__item\" *ngFor=\"let plugin of allPlugins; let i = index\" dnd-draggable [dragEnabled]=\"true\" [dragData]=\"plugin\">\r\n                            <div class=\"plugins__item-content\">\r\n                                <span>{{ (plugin.displayName || plugin.name) | capitalize }}</span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"plugins__body\">\r\n                    <div class='col-sm-6 flow' dnd-droppable (onDropSuccess)=\"transferDataSuccess($event)\">\r\n                        <div class=\"flow__container\" [class.filled]='pipePlugins?.length > 0'>\r\n                            <div class=\"flow__path\">\r\n                                <h3>~{{basicPath}}{{rule.path}}</h3>\r\n                            </div>\r\n                            <div class=\"flow__plugins-list\" dnd-sortable-container [sortableData]=\"pipePlugins\">\r\n                                <div *ngFor=\"let plugin of pipePlugins; let i = index\" class='flow-item' [class.active]='plugin===activePlugin' dnd-sortable\r\n                                    [sortableIndex]=\"i\" (click)='setPluginActive(plugin)'>\r\n                                    <div class='flow-item__number2'>\r\n                                        <i class=\"icon-angle-double-down flow-item__icon\"></i>\r\n                                        <button class='button button--icon flow-item__remove-button' (click)='deletePlugin(plugin)'> \r\n                                            <span class=\"icon-delete-garbage-streamline red-on-hover\"></span>\r\n                                        </button>\r\n                                    </div>\r\n                                    <div class=\"flow-item__content2\">\r\n                                        <span>{{ (plugin.shortName || plugin.name) | capitalize}}</span>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <h3 class=\"flow__title\" [class.filled]='pipePlugins?.length > 0'>Drop plugins here</h3>\r\n                        </div>\r\n\r\n                    </div>\r\n                    <div class=\"col-sm-6 form\">\r\n                        <div class=\"form__container\" *ngIf=\"!!editable\">\r\n                            <h3>{{ (editable?.displayName || editable?.name) | capitalize }}</h3>\r\n                            <plugin-form #pForm [plugin]=\"editable\" (onChange)='pluginValueChanged($event)' (validation)='pluginValidationChanged($event)'>\r\n                            </plugin-form>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n  \r\n\r\n    <div class=\"row\" *ngIf=\"tab=='conditions'\" style='flex:1; overflow: auto;'>\r\n        <div class=\"plugins col-sm-12\">\r\n            <div class=\"conditions__container\">\r\n                <entry-condition *ngFor='let condition of rule?.conditions' [condition]='condition' (change)='onConditionsChanged($event)'\r\n                    (remove)='onDeleteCondition(condition)'></entry-condition>\r\n                <button class='button button--secondary' (click)='onAddNewConditions()'>Add</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n      \r\n    <div class=\"row\" *ngIf=\"tab=='policy'\" style='flex:1; overflow: auto;'>\r\n        <div class=\"plugins col-sm-12\">\r\n            <div class=\"conditions__container\">\r\n              <loc-input-ace [ngModel]=\"rule.policy\" (change)='onPolicyChanged($event)'></loc-input-ace>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"l-step\">\r\n    <div class=\"l-step__card\">\r\n        <div class='l-step__header'>\r\n            <h2>Routing table</h2>\r\n            <button class='button button--icon help'>\r\n                <i class=\"icon-help-circled\"></i>\r\n            </button>\r\n        </div>\r\n        <loader [active]='loading'></loader>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div style=\"width: 100%;\" class=\"alert alert-warning alert-sm\" *ngIf=\"showError\" id=\"app-alert\">\r\n                    <button type=\"button\" class=\"ml-lg close\" (click)=\"showError = false;\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>{{validationErrorText}}\r\n                </div>\r\n                <div class=\"image-container\">\r\n                    <img src=\"/assets/images/cloud.png\" alt=\"\" style=\"width: 50px;\">\r\n                </div>\r\n                <div class='rules-container'>\r\n                    <div class=\"rule rule--common\" style=\"position: relative;\">\r\n                        <a href (click)='editCommon()'>~{{basicPath}}/{{basicRule.path}}</a>\r\n                    </div>\r\n                    <div class='rule' *ngFor='let rule of rules'>\r\n                        <div class='rule__header2'>\r\n                            <inline-edit  title='Edit {{ rule.path }}' #edit [(ngModel)]='rule.path' [ngModelOptions]=\"{standalone: true}\" (onSave)='savePath(rule, $event)'></inline-edit>\r\n                            <div class='rule__button-set'>\r\n                                <button class='button button--icon darken-on-hover' (click)='editRule(rule)' title='Edit config'> \r\n                                    <span class=\"icon-ios-compose-outline\"></span>\r\n                                </button>\r\n                                <button class='button button--icon red-on-hover' style='font-size: 1.05rem;' (click)='deleteRule(rule)' title='Remove child route'>\r\n                                    <span class=\"icon-delete-garbage-streamline\"></span>\r\n                                </button>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <button type=\"button\" class=\"button button--secondary rules-container__add-button\" (click)=\"addRule()\">\r\n                        <span class='icon-plus-1'></span>    \r\n                        <span>Add child</span> \r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <ng-template #pluginsModal let-c=\"close\">\r\n        <div class=\"modal-header\">\r\n            <h4 class=\"modal-title\">Edit rule configuration</h4>\r\n            <button type=\"button\" class=\"close\" (click)=\"c()\" aria-label=\"Close\">\r\n                  <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <div class=\"pipeModal-container\">\r\n                <plugins-set-rule #ruleComponent [rule]='underEdit' [basicPath]='basicPath' [allPlugins]='plugins'></plugins-set-rule>\r\n            </div>\r\n        </div>\r\n        <div class='modal-footer'>\r\n            <button type=\"button\" (click)=\"c()\" class=\"button button--secondary\">             \r\n                Cancel\r\n            </button>\r\n            <button type=\"button\" (click)=\"c(ruleComponent.rule)\" class=\"button button--primary\">             \r\n                Save\r\n            </button>\r\n        </div>\r\n    </ng-template>\r\n\r\n    <div class='l-steps__buttons'>\r\n        <button type=\"button\" (click)=\"onSubmit()\" class=\"button button--primary l-master__next\">\r\n            <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i>  Next         \r\n        </button>\r\n        <button type=\"button\" (click)=\"onSave()\" class=\"button button--primary l-master__next\">\r\n            <i class=\"icon-floppy-o\" aria-hidden=\"true\"></i>  Save\r\n        </button>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_models__ = __webpack_require__("../../../../../src/app/core/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesWizardStepPlugins; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var EntriesWizardStepPlugins = (function () {
    function EntriesWizardStepPlugins(_masterActions, modalService, _validationActions, _apiConfigApi, _store) {
        this._masterActions = _masterActions;
        this.modalService = modalService;
        this._validationActions = _validationActions;
        this._apiConfigApi = _apiConfigApi;
        this._store = _store;
        this.next = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.save = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        //@Output()
        //validation: EventEmitter<any> = new EventEmitter();
        this.validation = new __WEBPACK_IMPORTED_MODULE_5_rxjs__["BehaviorSubject"](true);
        this.proxy = {};
        this.plugins = [];
        this.rules = [];
        this.conditions = [];
        this.submitted = false;
        this.underEdit = {};
        this.showError = false;
        this.basicRule = {
            path: '*',
            plugins: [],
            conditions: []
        };
    }
    EntriesWizardStepPlugins.prototype.ngOnInit = function () {
        var _this = this;
        this._store.let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__core_reducers__["g" /* getMasterState */])())
            .subscribe(function (config) {
            Promise.resolve()
                .then(function () {
                _this.basicPath = config.entry;
            });
        });
        this.loading = true;
        this.configStateSub_n =
            this._store.let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__core_reducers__["h" /* getPlugins */])())
                .do(function (plugins) { return _this.plugins = plugins || []; })
                .flatMap(function () { return _this._store.let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__core_reducers__["i" /* getConfigState */])()); })
                .subscribe(function (config) {
                if (!config)
                    return;
                Promise.resolve()
                    .then(function () {
                    _this.proxy = config.proxy || _this.proxy;
                    // Setup rules
                    var rules = config.rules.slice();
                    (rules.plugins || [])
                        .map(function (plugin) {
                        var p = _this.plugins.find(function (plug) { return plug.name === plugin.name; });
                        var plCp = Object.assign({}, p);
                        return new __WEBPACK_IMPORTED_MODULE_1__core_models__["j" /* Plugin */](Object.assign({}, plCp, { settings: plugin.settings, dependencies: plugin.dependencies }));
                    });
                    // Setup plugins for main flow                    
                    var plugins = config.plugins.slice();
                    _this.basicRule.plugins = [];
                    _this.basicRule.conditions = config.conditions.slice();
                    if (plugins && Array.isArray(plugins) && plugins.length > 0) {
                        plugins.forEach(function (plugin) {
                            if (plugin) {
                                var p = _this.plugins.find(function (plug) { return plug.name === plugin.name; });
                                var plCp = Object.assign({}, p);
                                var pluginInst = new __WEBPACK_IMPORTED_MODULE_1__core_models__["j" /* Plugin */](Object.assign({}, plCp, { order: _this.basicRule.plugins.length + 1 }, { settings: plugin.settings, dependencies: plugin.dependencies }));
                                _this.basicRule.plugins.push(pluginInst);
                            }
                        });
                        _this.stagePlugins();
                    }
                    _this.rules = rules;
                    _this.validate();
                    _this.loading = false;
                });
            });
    };
    EntriesWizardStepPlugins.prototype.selectPlugin = function (plugin) {
        if (plugin)
            this.activePlugin = plugin;
    };
    EntriesWizardStepPlugins.prototype.selectRulePlugin = function (plugin) {
        if (plugin)
            this.activePlugin = plugin;
    };
    EntriesWizardStepPlugins.prototype.ngOnDestroy = function () {
        if (this.configStateSub_n) {
            this.configStateSub_n.unsubscribe();
        }
        if (this.pluginsSub_n) {
            this.pluginsSub_n.unsubscribe();
        }
        if (this.proxySub_n) {
            this.proxySub_n.unsubscribe();
        }
        this.rules = [];
    };
    Object.defineProperty(EntriesWizardStepPlugins.prototype, "_valid", {
        get: function () {
            var valid = this.rules && this.rules.length > 0;
            return valid;
        },
        enumerable: true,
        configurable: true
    });
    EntriesWizardStepPlugins.prototype.stagePlugins = function () {
        this._store.dispatch(this._masterActions.setPluginsData({ plugins: this.basicRule.plugins, proxy: this.proxy, rules: this.rules, conditions: this.conditions, policy: this.policy }));
    };
    EntriesWizardStepPlugins.prototype.selectActiveRule = function (rule) {
        this.activeRule = rule;
    };
    EntriesWizardStepPlugins.prototype.onSubmit = function () {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ plugins: this._valid }));
        if (this._valid) {
            this.stagePlugins();
            this.next.next('preview');
        }
        else {
            this.validate();
        }
    };
    EntriesWizardStepPlugins.prototype.onSave = function () {
        this.submitted = true;
        this._store.dispatch(this._validationActions.setValidity({ plugins: this._valid }));
        if (this._valid) {
            this.stagePlugins();
            this.save.next('done');
        }
    };
    EntriesWizardStepPlugins.prototype.addRule = function () {
        this.rules.push(new __WEBPACK_IMPORTED_MODULE_1__core_models__["h" /* Rule */]({
            path: '/*',
            plugins: [],
            policy: "[]\n            \n            \n            \n            \n            "
        }));
        this.stagePlugins();
        this.validate();
    };
    EntriesWizardStepPlugins.prototype.editRule = function (rule) {
        var _this = this;
        this.underEdit = Object.assign({}, rule);
        this._modalRef = this.modalService
            .open(this.editPluginsContent, { windowClass: 'plugin-modal2' });
        this._modalRef.result.then(function (result) {
            if (result) {
                console.log(result);
                rule.plugins = result.plugins;
                rule.proxy = result.proxy;
                rule.methods = result.methods;
                rule.conditions = result.conditions;
                rule.policy = result.policy;
                _this.stagePlugins();
                _this.underEdit = undefined;
            }
        });
        this.validate();
    };
    EntriesWizardStepPlugins.prototype.savePath = function (rule, value) {
        rule.path = value;
    };
    EntriesWizardStepPlugins.prototype.editCommon = function () {
        var _this = this;
        this.underEdit = Object.assign({}, this.basicRule, { proxy: this.proxy, base: true });
        this._modalRef = this.modalService
            .open(this.editPluginsContent, { windowClass: 'plugin-modal2' });
        this._modalRef.result.then(function (result) {
            if (result) {
                console.log("COMMON:", result);
                _this.basicRule.plugins = result.plugins;
                _this.proxy = result.proxy; // todo here
                _this.conditions = result.conditions; // todo here
                _this.policy = result.policy;
                _this.stagePlugins();
                _this.underEdit = undefined;
            }
        });
        this.validate();
    };
    EntriesWizardStepPlugins.prototype.deleteRule = function (rule) {
        var ind = this.rules.indexOf(rule);
        this.rules.splice(ind, 1);
        this.stagePlugins();
        this.validate();
    };
    EntriesWizardStepPlugins.prototype.validate = function () {
        this._store.dispatch(this._validationActions.setValidity({ plugins: this._valid }));
        this.validation.next(this._valid);
    };
    return EntriesWizardStepPlugins;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EntriesWizardStepPlugins.prototype, "next", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], EntriesWizardStepPlugins.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5_rxjs__["BehaviorSubject"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_rxjs__["BehaviorSubject"]) === "function" && _c || Object)
], EntriesWizardStepPlugins.prototype, "validation", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], EntriesWizardStepPlugins.prototype, "submitted", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('pluginsModal'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _d || Object)
], EntriesWizardStepPlugins.prototype, "editPluginsContent", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('proxyModal'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _e || Object)
], EntriesWizardStepPlugins.prototype, "editProxyContent", void 0);
EntriesWizardStepPlugins = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'step-plugins',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/stepPlugins.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_actions__["f" /* MasterActions */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_3__core_actions__["h" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_actions__["h" /* ValidationActions */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_4__core__["o" /* ApiConfigApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["o" /* ApiConfigApi */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["b" /* Store */]) === "function" && _k || Object])
], EntriesWizardStepPlugins);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=entries-wizard-step-plugins.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_plugins_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__entries_wizard_step_plugins_rule_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-wizard-step-plugins-rule.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entries_option_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/entries-option.component.ts");
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return STEP_PLUGINS_COMPONENTS; });






var STEP_PLUGINS_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_plugins_component__["a" /* EntriesWizardStepPlugins */],
    __WEBPACK_IMPORTED_MODULE_1__entries_wizard_step_plugins_rule_component__["a" /* StepPluginsRule */],
    __WEBPACK_IMPORTED_MODULE_2__entries_option_component__["a" /* EntryOptionComponent */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/plugins-rule-editor.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".l-rule {\n  padding: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  height: 100%; }\n  .l-rule .plugins {\n    list-style: none;\n    -webkit-box-flex: 0;\n        -ms-flex: 0 0 auto;\n            flex: 0 0 auto; }\n    .l-rule .plugins__container {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      padding: .625rem; }\n    .l-rule .plugins__details {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: justify;\n          -ms-flex-pack: justify;\n              justify-content: space-between;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap;\n      width: calc(100% + 10px);\n      padding: 10px 0 0;\n      margin-left: -5px;\n      margin-right: -5px; }\n      .l-rule .plugins__details:after {\n        content: \"\";\n        -webkit-box-flex: 1;\n            -ms-flex: auto;\n                flex: auto; }\n    .l-rule .plugins__body {\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex; }\n    .l-rule .plugins__header {\n      width: 100%;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 auto;\n              flex: 0 1 auto;\n      margin-bottom: .625rem; }\n    .l-rule .plugins__add-button {\n      width: 100%;\n      padding: 10px 0 6px; }\n    .l-rule .plugins__tab-button {\n      width: 200px;\n      border-radius: 0; }\n      .l-rule .plugins__tab-button.active {\n        background: #007ee5;\n        border-color: #5b7e9a;\n        color: white; }\n    .l-rule .plugins__item {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: start;\n          -ms-flex-pack: start;\n              justify-content: flex-start;\n      position: relative;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 33.333%;\n              flex: 0 1 33.333%;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: row;\n              flex-direction: row;\n      padding: 0 .3rem;\n      margin-bottom: .3rem; }\n      .l-rule .plugins__item-content {\n        padding: 10px 15px;\n        color: #373a3c;\n        background: #fafafa;\n        font-size: 16px;\n        text-align: inherit;\n        width: 100%;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        border: dotted 1px #dedede; }\n        .l-rule .plugins__item-content:hover {\n          background: rgba(229, 229, 229, 0.3); }\n      .l-rule .plugins__item > span {\n        overflow: hidden;\n        white-space: nowrap;\n        text-overflow: ellipsis;\n        max-width: 120px; }\n      .l-rule .plugins__item.active {\n        background: #E5E5E5 !important; }\n    .l-rule .plugins__icon {\n      color: #6e7e89; }\n  .l-rule .conditions__container {\n    width: 100%; }\n  .l-rule .flow__plugins-list {\n    text-align: center;\n    padding-right: 60px;\n    margin-bottom: 100px; }\n    @media screen and (max-width: 767px) {\n      .l-rule .flow__plugins-list {\n        padding-right: 0; } }\n    @media screen and (max-width: 991px) {\n      .l-rule .flow__plugins-list {\n        padding-right: .625rem; } }\n  .l-rule .flow__container {\n    width: 100%; }\n  .l-rule .flow__title {\n    color: #6e7e89;\n    text-transform: uppercase;\n    font-size: 1.8rem;\n    z-index: -1;\n    text-align: center;\n    width: 100%;\n    -webkit-transform: translateY(-55px);\n            transform: translateY(-55px); }\n    .l-rule .flow__title.filled {\n      color: #6e7e89;\n      text-transform: uppercase;\n      font-size: 1.4rem;\n      width: 80%;\n      width: 80%; }\n  .l-rule .flow__path {\n    margin: 0 0 20px;\n    width: 100%; }\n    .l-rule .flow__path > h3 {\n      text-align: left;\n      white-space: nowrap;\n      overflow: hidden !important;\n      text-overflow: ellipsis;\n      display: inline-block;\n      max-width: 100%; }\n  .l-rule .flow-item {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    padding: 0;\n    margin-bottom: .625rem; }\n    .l-rule .flow-item__remove-button {\n      display: none;\n      font-size: 1.2rem; }\n    .l-rule .flow-item__number2 {\n      border: 1px dotted #cfd8dc;\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      display: -ms-flexbox;\n      color: rgba(69, 90, 100, 0.7);\n      position: relative;\n      font-weight: 600;\n      background: #fafafa;\n      text-align: center;\n      font-size: 1.3rem;\n      margin-right: .625rem;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 0 15%;\n              flex: 0 0 15%;\n      display: -webkit-box;\n      display: flex; }\n      .l-rule .flow-item__number2 > button {\n        -webkit-transform: translateY(2px);\n                transform: translateY(2px); }\n      .l-rule .flow-item__number2 > i {\n        -webkit-transform: translateY(4px);\n                transform: translateY(4px); }\n      .l-rule .flow-item__number2 > i,\n      .l-rule .flow-item__number2 button {\n        margin: auto; }\n    .l-rule .flow-item__content2 {\n      border: 1px dotted #cfd8dc;\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      display: -ms-flexbox;\n      padding: .625rem;\n      color: rgba(69, 90, 100, 0.7);\n      position: relative;\n      font-weight: 600;\n      background: #fafafa;\n      text-align: center; }\n    .l-rule .flow-item.active .flow-item__remove-button {\n      display: block; }\n    .l-rule .flow-item.active .flow-item__content2,\n    .l-rule .flow-item.active .flow-item__number2 {\n      border-style: solid; }\n    .l-rule .flow-item.active .flow-item__icon {\n      display: none; }\n    .l-rule .flow-item > button {\n      font-size: 1.2rem;\n      -webkit-transform: translateY(2px);\n              transform: translateY(2px); }\n  .l-rule .flow__container,\n  .l-rule .form__container {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1; }\n  .l-rule .plugins,\n  .l-rule .proxy {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n    .l-rule .plugins__container,\n    .l-rule .proxy__container {\n      border: 1px dotted rgba(110, 126, 137, 0.31);\n      border-radius: 2px;\n      padding: .625rem;\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      overflow: auto; }\n  .l-rule .flow,\n  .l-rule .form {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    margin-top: .625rem; }\n  @media screen and (min-width: 767px) {\n    .l-rule .flow {\n      padding-right: .325rem; } }\n  @media screen and (min-width: 767px) {\n    .l-rule .form {\n      padding-left: .325rem; } }\n  .l-rule .tab {\n    margin-bottom: 15px; }\n  .l-rule .__commands {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n    .l-rule .__commands > li {\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 50%;\n              flex: 0 1 50%;\n      text-align: center;\n      padding: .625rem;\n      border-bottom: solid 1px #ccc;\n      background-color: #FFFFFF;\n      color: #666;\n      font-size: larger;\n      cursor: pointer; }\n      .l-rule .__commands > li.active {\n        border-bottom: solid 2px #d26911;\n        color: #555; }\n      .l-rule .__commands > li span {\n        text-decoration: none;\n        color: inherit; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/set-plugins-rule.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".rule {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n  padding-left: 20px;\n  margin-bottom: 5px;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n  .rule__header {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between; }\n    .rule__header > inline-edit {\n      max-width: calc(100% - 30px); }\n  .rule__expand-button {\n    position: absolute;\n    left: 0;\n    top: -4px; }\n  .rule__delete-button {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 auto;\n            flex: 0 1 auto;\n    margin-left: auto; }\n  .rule__container {\n    overflow: hidden; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-plugins/stepPlugins.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n\n.plugins-container {\n  width: 80%;\n  margin: 0 auto; }\n\n.image-container > img {\n  -webkit-transform: translate(10px, -5px);\n          transform: translate(10px, -5px); }\n\n.image-container::after {\n  content: '';\n  background: url(\"/assets/images/lines.png\") no-repeat 0 0;\n  background-position: -108px -30px;\n  width: 30px;\n  display: inline-block;\n  height: 20px;\n  position: absolute;\n  top: 50px;\n  left: 50px; }\n\n.image-container.empty::after {\n  background-position: -107px 0;\n  width: 2px;\n  height: 30px;\n  position: absolute;\n  left: 50%;\n  bottom: -30px; }\n\n.plugin-props {\n  position: relative;\n  margin-top: 10px; }\n  .plugin-props .delete-button {\n    position: absolute;\n    right: 0;\n    top: 0; }\n\n.alert-sm {\n  padding: 10px 12px;\n  font-size: 13px; }\n  .alert-sm .ml-lg {\n    margin-left: 20px;\n    font-size: 18px; }\n  .alert-sm .fw-semi-bold {\n    font-weight: 600; }\n\n.rules-container {\n  margin: 0 auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  margin-top: 30px; }\n  .rules-container__add-button {\n    padding: 2px;\n    width: 150px;\n    font-size: .9rem;\n    margin-left: 35px;\n    margin-top: 12px; }\n    .rules-container__add-button:active {\n      box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.2); }\n    .rules-container__add-button > .icon-plus-1 {\n      -webkit-transform: translateY(2px);\n              transform: translateY(2px);\n      display: inline-block; }\n\n.proxy {\n  position: relative; }\n  .proxy__container {\n    margin: 0 auto;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    margin-top: 10px;\n    margin-left: 70px; }\n  .proxy:not(.disabled):before {\n    content: '';\n    background: url(\"/assets/images/lines.png\") no-repeat 0 0;\n    background-position: 0 -30px;\n    width: 30px;\n    display: inline-block;\n    height: 30px;\n    position: absolute;\n    top: -5px;\n    left: 15px; }\n  .proxy__label > a {\n    text-decoration: none;\n    outline: none; }\n  .proxy.disabled .proxy__badge {\n    background: #a6b0b7; }\n  .proxy__badge {\n    background: #455a64;\n    margin-right: 15px;\n    color: white;\n    font-weight: 500;\n    width: 35px;\n    padding: 5px 10px 2px;\n    display: inline-block;\n    transition: 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) all; }\n  .proxy__switch {\n    margin-right: 5px; }\n\n.rule {\n  margin-left: 65px;\n  position: relative; }\n  .rule:before {\n    content: '';\n    background: url(\"/assets/images/lines.png\") no-repeat 0 0;\n    background-position: -70px 0;\n    width: 30px;\n    display: inline-block;\n    height: 20px;\n    position: absolute;\n    left: -35px;\n    top: 8px; }\n  .rule--common {\n    margin-left: 0 !important;\n    margin-bottom: 5px !important; }\n    .rule--common:before {\n      content: none; }\n    .rule--common > a {\n      outline: none;\n      text-decoration: none; }\n  .rule__header2 {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    margin-bottom: 5px; }\n  .rule__button-set {\n    margin-left: auto;\n    margin-left: 15px;\n    -webkit-transform: translateY(7px);\n            transform: translateY(7px); }\n    .rule__button-set > * {\n      display: inline-block;\n      font-size: 1.4rem; }\n  .rule inline-edit {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 0 auto;\n            flex: 1 0 auto; }\n\n.pipeModal-container {\n  overflow: auto;\n  padding: 5px;\n  max-height: 100%;\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  padding: 0 15px; }\n  .pipeModal-container button {\n    margin-top: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-preview/entries-wizard-step-preview.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"l-step\">\r\n    <div class=\"l-step__card\">\r\n        <div class='l-step__header'>\r\n            <h2>Quick test</h2>\r\n            <button class='button button--icon help'>\r\n                <i class=\"icon-help-circled\"></i>\r\n            </button>\r\n        </div>\r\n        <loader [active]='loading'></loader>\r\n        <div class=\"l-preview\">\r\n            <div class='row __commands-header'>\r\n                <div class=\"col-xs-12 col-sm-2 __methods\">\r\n                    <loc-select [items]=\"['GET','POST','PUT','DELETE','PATCH', 'HEAD']\" textField='name' label='Method' [ngModel]='selectedMethod' (change)='methodChange($event)'>\r\n                    </loc-select>\r\n                </div>\r\n                <div class=\"col-xs-12 col-sm-8 __entry\">\r\n                    <span>URL: ~{{config.entry}}</span>\r\n                    <loc-input-text label='Url' [(ngModel)]='requestUrl'></loc-input-text>\r\n                </div>                \r\n                <div class=\"col-xs-12 col-sm-2\">\r\n                    <button type=\"button\" (click)=\"send()\" class=\"button button--secondary __send\">         \r\n                        SEND\r\n                    </button>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-12 __options-tab\">\r\n                    <ngb-tabset #tabset class='l-steps__preview-tabset' (tabChange)='tabChanged($event)'>\r\n                        <ngb-tab title=\"Headers\" id='headers'>\r\n                            <ng-template ngbTabContent>\r\n                                <div style='padding:5px 0;'>\r\n                                    <key-value-control [(ngModel)]='headers'></key-value-control>\r\n                                </div>\r\n                            </ng-template>\r\n                        </ngb-tab>\r\n                        <ngb-tab title=\"Query\" id='queryparams'>\r\n                            <ng-template ngbTabContent>\r\n                                <div style='padding:5px 0;'>\r\n                                    <key-value-control [(ngModel)]='params'></key-value-control>\r\n                                </div>\r\n                            </ng-template>\r\n                        </ngb-tab>\r\n                        <ngb-tab title=\"Body\" [disabled]=\"bodyDisabled\" id='body'>\r\n                            <ng-template ngbTabContent>\r\n                                <div style='padding:5px 0;'>\r\n                                    <div ace-editor [text]=\"text\" [mode]=\"'json'\" [theme]=\"'eclipse'\" [options]=\"aceOptions\" [readOnly]=\"false\" [autoUpdateContent]=\"true\"\r\n                                        (textChanged)=\"onChange($event)\" style=\"min-height:250px;\">\r\n                                    </div>\r\n                                </div>\r\n                            </ng-template>\r\n                        </ngb-tab>\r\n                    </ngb-tabset>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ng-template #contentPreview let-c=\"close\" let-d=\"dismiss\">\r\n            <div class=\"l-steps__resultModal-container\">\r\n                <div [ngSwitch]=\"testresult\" class='l-steps__result'>\r\n                    <ng-template ngSwitchCase=\"json\">\r\n                        <div class='l-steps__result-json'>\r\n                            <pre [innerHTML]='prettifyJson(result) | safeHtml'>                            \r\n                            </pre>\r\n                        </div>\r\n                    </ng-template>\r\n                    <ng-template ngSwitchCase=\"html\">\r\n                        <div class=\"l-steps__result-html\">\r\n                            <iframe [srcdoc]='result | safeHtml'></iframe>\r\n                        </div>\r\n                    </ng-template>\r\n                    <ng-template ngSwitchDefault>\r\n                        <div class='l-steps__result-text'>\r\n                            <pre [innerHTML]='result | safeHtml'>                               \r\n                           </pre>\r\n                        </div>\r\n                    </ng-template>\r\n                </div>\r\n                <button type=\"button\" class=\"button button--primary\" (click)=\"c()\" style='flex: 0 1 auto;'>Ok</button>\r\n            </div>\r\n        </ng-template>\r\n    </div>\r\n    <div class='l-steps__buttons'>       \r\n        <button type=\"button\" (click)=\"save.emit()\" class=\"button button--primary l-master__next\">\r\n            <i class=\"icon-floppy-o\" aria-hidden=\"true\"></i>  Save\r\n        </button>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-preview/entries-wizard-step-preview.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_reducers__ = __webpack_require__("../../../../../src/app/core/reducers/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesWizardStepPreview; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EntriesWizardStepPreview = (function () {
    function EntriesWizardStepPreview(_masterActions, _validationActions, modalService, _store, _apiConfig) {
        this._masterActions = _masterActions;
        this._validationActions = _validationActions;
        this.modalService = modalService;
        this._store = _store;
        this._apiConfig = _apiConfig;
        this.next = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.save = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.loading = false;
        this.submitted = false;
        this._text = "";
        this.body = {};
        this.aceOptions = { maxLines: 1000, printMargin: false };
        this.methods = [];
        this.bodyDisabled = false;
        this.headers = [];
        this.params = [];
        this.config = {};
        this._requestUrl = '/';
        this.testresult = 'text';
    }
    Object.defineProperty(EntriesWizardStepPreview.prototype, "selectedMethod", {
        get: function () {
            return this._selectedMethod;
        },
        set: function (value) {
            if (value == 'POST' || value == "PUT" || value == 'PATCH') {
                this.bodyDisabled = false;
            }
            else {
                this.bodyDisabled = true;
                if (this.tabSet) {
                    this.tabSet.select('headers');
                }
            }
            this._selectedMethod = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EntriesWizardStepPreview.prototype, "requestUrl", {
        get: function () {
            return this._requestUrl;
        },
        set: function (value) {
            this._requestUrl = value;
        },
        enumerable: true,
        configurable: true
    });
    EntriesWizardStepPreview.prototype.ngOnInit = function () {
        this.text = JSON.stringify(this.body || {});
    };
    Object.defineProperty(EntriesWizardStepPreview.prototype, "text", {
        get: function () {
            return this._text;
        },
        set: function (v) {
            this._text = v;
        },
        enumerable: true,
        configurable: true
    });
    EntriesWizardStepPreview.prototype.ngAfterViewInit = function () {
        var _this = this;
        this._store.let(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__core_reducers__["g" /* getMasterState */])())
            .subscribe(function (config) {
            Promise.resolve().then(function () {
                _this.config = config;
                if (config.methods)
                    _this.selectedMethod = config.methods[0];
            });
        });
    };
    EntriesWizardStepPreview.prototype.onChange = function (code) {
        try {
            this.body = JSON.parse(code);
        }
        catch (error) {
            console.warn('Bad json');
        }
    };
    EntriesWizardStepPreview.prototype.tabChanged = function (event) {
        this.text = JSON.stringify(this.body || {});
    };
    EntriesWizardStepPreview.prototype.methodChange = function (method) {
        this.selectedMethod = method;
    };
    EntriesWizardStepPreview.prototype.send = function () {
        var _this = this;
        this.testresult = 'text';
        console.log(this.params);
        var queryParams = {};
        if (this.params && this.params.length > 0) {
            this.params.forEach(function (pr) {
                queryParams[pr.key] = pr.value;
            });
        }
        ;
        this._apiConfig.testApiConfig(this.selectedMethod, this.requestUrl, this.config, this.headers, queryParams, this.body)
            .subscribe(function (res) {
            _this.modalRef = _this.modalService
                .open(_this.content, { windowClass: 'test-results-modal' });
            _this.modalRef.result.then(function (result) {
            }, function (reason) {
            });
            _this.result = res.body;
            if (typeof _this.result == 'string' && _this.result.toLowerCase().startsWith('<!doctype')) {
                _this.testresult = 'html';
            }
            else {
                var json = void 0;
                try {
                    _this.result = JSON.parse(_this.result);
                    _this.testresult = 'json';
                }
                catch (error) {
                    _this.testresult = 'text';
                }
            }
        }, function (err) {
            console.error(err);
        });
    };
    EntriesWizardStepPreview.prototype.prettifyJson = function (json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 4);
            console.log('JSON', json);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                }
                else {
                    cls = 'string';
                }
            }
            else if (/true|false/.test(match)) {
                cls = 'boolean';
            }
            else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    };
    EntriesWizardStepPreview.prototype.onOk = function () {
        if (this.modalRef) {
            this.modalRef.close();
        }
    };
    return EntriesWizardStepPreview;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EntriesWizardStepPreview.prototype, "next", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], EntriesWizardStepPreview.prototype, "save", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('close'),
    __metadata("design:type", Object)
], EntriesWizardStepPreview.prototype, "close", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('contentPreview'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _c || Object)
], EntriesWizardStepPreview.prototype, "content", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('tabset'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["c" /* NgbTabset */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["c" /* NgbTabset */]) === "function" && _d || Object)
], EntriesWizardStepPreview.prototype, "tabSet", void 0);
EntriesWizardStepPreview = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'step-preview',
        template: __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-preview/entries-wizard-step-preview.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/entries-wizard-step-preview/stepPreview.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_actions__["f" /* MasterActions */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__core_actions__["h" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core_actions__["h" /* ValidationActions */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_1__core__["o" /* ApiConfigApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core__["o" /* ApiConfigApi */]) === "function" && _j || Object])
], EntriesWizardStepPreview);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=entries-wizard-step-preview.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-preview/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_preview_component__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-preview/entries-wizard-step-preview.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__entries_wizard_step_preview_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries-wizard-step-preview/stepPreview.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".l-step__card,\n:host {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n\n:host /deep/ .nav-link.active {\n  border-bottom: 2px solid #d26911 !important; }\n\n:host /deep/ .nav-item {\n  border: none !important; }\n  :host /deep/ .nav-item > * {\n    border: none !important; }\n  :host /deep/ .nav-item:not(.active) {\n    border: none !important; }\n    :host /deep/ .nav-item:not(.active):hover {\n      color: #455a64 !important; }\n    :host /deep/ .nav-item:not(.active) > * {\n      color: rgba(69, 90, 100, 0.7) !important; }\n\n.method-block {\n  margin-top: 5px; }\n\n.send-button {\n  background: #d26911;\n  color: white;\n  outline: none; }\n\n.l-preview {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1; }\n  .l-preview .__entry {\n    margin-bottom: .625rem;\n    margin-top: .625rem;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: end;\n        -ms-flex-pack: end;\n            justify-content: flex-end; }\n    .l-preview .__entry > loc-input-text {\n      margin-left: 10px;\n      -webkit-box-flex: 0;\n          -ms-flex: 0 1 auto;\n              flex: 0 1 auto; }\n    .l-preview .__entry > span {\n      display: inline-block;\n      white-space: nowrap;\n      overflow: hidden !important;\n      text-overflow: ellipsis;\n      display: inline-block;\n      -webkit-box-flex: 1;\n          -ms-flex: 1 0 auto;\n              flex: 1 0 auto;\n      margin: auto .625rem auto auto;\n      max-width: 220px;\n      text-align: right;\n      margin-top: 5px; }\n  .l-preview .__methods {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    margin-bottom: .625rem;\n    margin-top: .625rem;\n    width: 100%; }\n  .l-preview .__send {\n    margin-bottom: .625rem;\n    margin-top: .625rem;\n    width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/entries.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entries_base__ = __webpack_require__("../../../../../src/app/+entries/entries-base/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__entries_list__ = __webpack_require__("../../../../../src/app/+entries/entries-list/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__entries_wizard_base__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-base/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__entries_wizard_step_general__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-general/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__entries_wizard_step_plugins__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-plugins/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__entries_wizard_step_preview__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-preview/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__entries_wizard_step_healthcheck__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-step-healthcheck/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_components_loc_pages__ = __webpack_require__("../../../../../src/app/shared/components/loc-pages/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__entries_routing_module__ = __webpack_require__("../../../../../src/app/+entries/entries.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__shared_entries_shared_module__ = __webpack_require__("../../../../../src/app/+entries/shared/entries.shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntriesModule", function() { return EntriesModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var ENTRIES_DECLARATIONS = [
    __WEBPACK_IMPORTED_MODULE_2__entries_base__["a" /* EntriesBaseComponent */],
    __WEBPACK_IMPORTED_MODULE_4__entries_wizard_base__["a" /* EntriesWizardBaseComponent */],
    __WEBPACK_IMPORTED_MODULE_5__entries_wizard_step_general__["a" /* EntriesWizardStepGeneral */]
].concat(__WEBPACK_IMPORTED_MODULE_8__entries_wizard_step_healthcheck__["a" /* HEALTHCHECK_COMPONENTS */], __WEBPACK_IMPORTED_MODULE_6__entries_wizard_step_plugins__["a" /* STEP_PLUGINS_COMPONENTS */], [
    __WEBPACK_IMPORTED_MODULE_7__entries_wizard_step_preview__["a" /* EntriesWizardStepPreview */]
], __WEBPACK_IMPORTED_MODULE_3__entries_list__["a" /* ENTRIES_LIST_COMPONENTS */]);



var EntriesModule = (function () {
    function EntriesModule() {
    }
    return EntriesModule;
}());
EntriesModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: ENTRIES_DECLARATIONS.slice(),
        imports: [
            __WEBPACK_IMPORTED_MODULE_13__shared__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_12__shared_entries_shared_module__["a" /* EntriesSharedModule */],
            __WEBPACK_IMPORTED_MODULE_11__entries_routing_module__["a" /* EntriesRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap__["a" /* PaginationModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__["FileUploadModule"],
            __WEBPACK_IMPORTED_MODULE_10__shared_components_loc_pages__["a" /* PageLayoutModule */]
        ]
    })
], EntriesModule);

//# sourceMappingURL=entries.module.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/entries.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_wizard_base__ = __webpack_require__("../../../../../src/app/+entries/entries-wizard-base/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__entries_list__ = __webpack_require__("../../../../../src/app/+entries/entries-list/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entries_base__ = __webpack_require__("../../../../../src/app/+entries/entries-base/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__entries_base__["a" /* EntriesBaseComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_5__core__["f" /* IsAuthenticatedGuard */]],
        children: [
            {
                path: 'master',
                component: __WEBPACK_IMPORTED_MODULE_0__entries_wizard_base__["a" /* EntriesWizardBaseComponent */]
            },
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_1__entries_list__["b" /* EntriesListComponent */]
            }
        ]
    },
];
var EntriesRoutingModule = (function () {
    function EntriesRoutingModule() {
    }
    return EntriesRoutingModule;
}());
EntriesRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forChild(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */]],
    })
], EntriesRoutingModule);

//# sourceMappingURL=entries.routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__key_value__ = __webpack_require__("../../../../../src/app/+entries/shared/components/key-value/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__option_input__ = __webpack_require__("../../../../../src/app/+entries/shared/components/option-input/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plugin_form__ = __webpack_require__("../../../../../src/app/+entries/shared/components/plugin-form/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__proxy_form__ = __webpack_require__("../../../../../src/app/+entries/shared/components/proxy-form/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__step_tabs__ = __webpack_require__("../../../../../src/app/+entries/shared/components/step-tabs/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__toggle_group__ = __webpack_require__("../../../../../src/app/+entries/shared/components/toggle-group/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__plugins_set__ = __webpack_require__("../../../../../src/app/+entries/shared/components/plugins-set/index.ts");
/* unused harmony namespace reexport */
/* unused harmony namespace reexport */
/* unused harmony namespace reexport */
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_4__step_tabs__["b"]; });
/* unused harmony namespace reexport */
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_6__plugins_set__["a"]; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENTRIES_COMPONENTS; });







var ENTRIES_COMPONENTS = __WEBPACK_IMPORTED_MODULE_0__key_value__["a" /* KEY_VALUE_COMPONENTS */].concat(__WEBPACK_IMPORTED_MODULE_4__step_tabs__["a" /* STEP_TABS_COMPONENTS */], [
    __WEBPACK_IMPORTED_MODULE_5__toggle_group__["a" /* ToggleGroup */],
    __WEBPACK_IMPORTED_MODULE_2__plugin_form__["a" /* PluginForm */],
    __WEBPACK_IMPORTED_MODULE_3__proxy_form__["a" /* ProxyForm */],
    __WEBPACK_IMPORTED_MODULE_1__option_input__["a" /* OptionInput */],
    __WEBPACK_IMPORTED_MODULE_6__plugins_set__["a" /* PluginSet */]
]);






//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/key-value/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__key_value_item__ = __webpack_require__("../../../../../src/app/+entries/shared/components/key-value/key-value-item.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__key_value_item_list__ = __webpack_require__("../../../../../src/app/+entries/shared/components/key-value/key-value-item-list.ts");
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KEY_VALUE_COMPONENTS; });




var KEY_VALUE_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__key_value_item__["a" /* KeyValueItem */],
    __WEBPACK_IMPORTED_MODULE_1__key_value_item_list__["a" /* KeyValueItemsList */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/key-value/key-value-item-list.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeyValueItemsList; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var KeyValueItemsList = KeyValueItemsList_1 = (function () {
    function KeyValueItemsList() {
        this._headers = [];
        /**
         * ControlValueAccessor members
         */
        this.onTouched = function () {
        };
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    Object.defineProperty(KeyValueItemsList.prototype, "headers", {
        get: function () {
            return this._headers;
        },
        set: function (value) {
            this._headers = value;
        },
        enumerable: true,
        configurable: true
    });
    KeyValueItemsList.prototype.addItem = function () {
        for (var _i = 0, _a = this.headers; _i < _a.length; _i++) {
            var header = _a[_i];
            header.focused = false;
        }
        this.headers.push({ key: '', value: '', focused: true });
        this.emitChanges();
    };
    KeyValueItemsList.prototype.deleteItem = function (index) {
        this.headers.splice(index, 1);
        this.emitChanges();
    };
    KeyValueItemsList.prototype.emitChanges = function () {
        console.log('_____', this.headers);
        var validHeaders = this.headers.filter(function (h) { return !!h.key && !!h.value; });
        this.onChange.emit(validHeaders);
    };
    KeyValueItemsList.prototype.writeValue = function (value) {
        if (!!value)
            this.headers = value;
    };
    KeyValueItemsList.prototype.registerOnChange = function (fn) {
        this.onChange.subscribe(fn);
    };
    KeyValueItemsList.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    return KeyValueItemsList;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], KeyValueItemsList.prototype, "headers", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], KeyValueItemsList.prototype, "onChange", void 0);
KeyValueItemsList = KeyValueItemsList_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'key-value-control',
        template: "   \n    <div *ngFor='let header of headers; let i = index' class='header-item-container'>         \n        <key-value-item [item]='header' (changed)='emitChanges(event)'></key-value-item>\n        <button class='btn btn-default remove-button' (click)='deleteItem(i)'>\n            <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n        </button>\n    </div>\n    <div class='key-value-item-inputs-box' (click)='addItem()'>      \n          <input type='text' class='form-control' placeholder='key'>        \n          <input type='text' class='form-control' placeholder='value'>  \n    </div> \n  ",
        styles: [__webpack_require__("../../../../../src/app/+entries/shared/components/key-value/key-value-item.scss")],
        providers: [
            {
                provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* NG_VALUE_ACCESSOR */],
                useExisting: __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return KeyValueItemsList_1; }),
                multi: true
            }
        ]
    })
], KeyValueItemsList);

var KeyValueItemsList_1, _a;
//# sourceMappingURL=key-value-item-list.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/key-value/key-value-item.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".header-item-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: row;\n          flex-flow: row;\n  -webkit-box-flex: 1;\n      -ms-flex: 1 0 auto;\n          flex: 1 0 auto; }\n  .header-item-container > * {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 auto;\n            flex: 0 1 auto; }\n\n.key-value-item {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 0 auto;\n          flex: 1 0 auto; }\n\n.key-value-item-inputs-box {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: row;\n          flex-flow: row;\n  margin-bottom: 2px; }\n  .key-value-item-inputs-box > * {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 auto;\n            flex: 0 1 auto; }\n  .key-value-item-inputs-box input {\n    width: 50%;\n    margin: 0 2px;\n    border: none;\n    border-top: 1px solid whitesmoke;\n    border-radius: 0;\n    color: #757575;\n    height: 25px;\n    font-size: .9em; }\n    .key-value-item-inputs-box input:focus {\n      outline: none !important;\n      border-bottom: 1px solid #d26911; }\n    .key-value-item-inputs-box input:hover {\n      background-color: whitesmoke; }\n\n.remove-button {\n  outline: none !important;\n  width: 25px;\n  height: 25px;\n  padding: 0;\n  font-size: .8em;\n  background-color: whitesmoke;\n  border-radius: 2px; }\n  .remove-button:hover {\n    color: #d26911; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/key-value/key-value-item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KeyValueItem; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var KeyValueItem = (function () {
    function KeyValueItem(_formBuilder, _renderer) {
        this._formBuilder = _formBuilder;
        this._renderer = _renderer;
        this.changed = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.delete = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.form = _formBuilder.group({
            key: '',
            value: ''
        });
    }
    KeyValueItem.prototype.ngAfterViewInit = function () {
        this.form.valueChanges.subscribe(this.changed);
        if (this.item.focused) {
            this._renderer.invokeElementMethod(this.keyInput.nativeElement, 'focus');
        }
    };
    return KeyValueItem;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], KeyValueItem.prototype, "item", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], KeyValueItem.prototype, "changed", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], KeyValueItem.prototype, "delete", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('keyInput'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _c || Object)
], KeyValueItem.prototype, "keyInput", void 0);
KeyValueItem = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'key-value-item',
        template: "\n     <div class='key-value-item'>\n        <form [formGroup]='form' >     \n            <div class='key-value-item-inputs-box'>      \n              <input #keyInput class='form-control' type='text' placeholder='key' [(ngModel)]='item.key' formControlName='key'>        \n              <input class='form-control' type='text' placeholder='value' [(ngModel)]='item.value' formControlName='value'>  \n            </div>                        \n        </form>\n     </div>     \n  ",
        styles: [__webpack_require__("../../../../../src/app/+entries/shared/components/key-value/key-value-item.scss")],
        host: {
            '[style.width]': "'100%'",
            '[style.display]': "'inline-block'"
        }
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _e || Object])
], KeyValueItem);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=key-value-item.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/option-input/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__option_input__ = __webpack_require__("../../../../../src/app/+entries/shared/components/option-input/option-input.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__option_input__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/option-input/option-input.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"form\" #f=\"ngForm\" class=\"form-group\">\r\n    <div [ngSwitch]=\"field.type\">\r\n        <label [attr.for]=\"field.key\">{{ field.label | capitalize }}</label>\r\n        <input class=\"form-control\" \r\n            *ngSwitchCase=\"'string'\" \r\n            [formControlName]=\"field.key\" \r\n            [(ngModel)]='field.value' \r\n            [id]=\"field.key\">\r\n        <select class=\"form-control\" [id]=\"field.key\" \r\n            *ngSwitchCase=\"'select'\" \r\n            [(ngModel)]='field.value' \r\n            [formControlName]=\"field.key\">\r\n             <option *ngFor=\"let opt of field.options\" [value]=\"opt.value\">{{ opt.key }}</option>\r\n        </select>\r\n        <show-error *ngIf=\"f.control.controls[field.key].dirty\" \r\n            [control]=\"field.key\" \r\n            [options]=\"{'required': 'Field is required'}\">\r\n        </show-error>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/option-input/option-input.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OptionInput; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OptionInput = (function () {
    function OptionInput() {
    }
    Object.defineProperty(OptionInput.prototype, "isValid", {
        get: function () {
            return this.form.controls[this.field.key] ? this.form.controls[this.field.key].valid : true;
        },
        enumerable: true,
        configurable: true
    });
    return OptionInput;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], OptionInput.prototype, "field", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["i" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["i" /* FormGroup */]) === "function" && _a || Object)
], OptionInput.prototype, "form", void 0);
OptionInput = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'option-input',
        template: __webpack_require__("../../../../../src/app/+entries/shared/components/option-input/option-input.tmpl.html")
    })
], OptionInput);

var _a;
//# sourceMappingURL=option-input.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugin-form/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plugin_form__ = __webpack_require__("../../../../../src/app/+entries/shared/components/plugin-form/plugin-form.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__plugin_form__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugin-form/plugin-form.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div [formGroup]='form' class=\"__container\">\r\n    <loader [active]='loading && !no_dependencies' [delay]='200'></loader>\r\n    <dynamic-form \r\n        [fields]=\"pluginTemplate\" \r\n        #settingsForm='dynForm' \r\n        [(ngModel)]='settingsValue' \r\n        formControlName='settings'>\r\n    </dynamic-form>\r\n    <dynamic-form [hidden]='no_dependencies'\r\n        [fields]=\"dependenciesTemplate\" \r\n        #dependenciesForm='dynForm' \r\n        [(ngModel)]='dependenciesValue' \r\n        formControlName='dependencies'>\r\n    </dynamic-form>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugin-form/plugin-form.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PluginForm; });
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PluginForm = (function () {
    function PluginForm(_builder, serviceApi) {
        this._builder = _builder;
        this.serviceApi = serviceApi;
        this.dependenciesValue = {};
        this.serviceConfigs = {};
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.validation = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.loading = false;
        this.form = this._builder.group({
            settings: [],
            dependencies: []
        });
    }
    Object.defineProperty(PluginForm.prototype, "plugin", {
        get: function () {
            return this._plugin;
        },
        set: function (pl) {
            var _this = this;
            if (pl) {
                this._plugin = Object.assign({}, pl);
                // set template for plugin settings
                this.pluginTemplate = this._plugin.settingsTemplate;
                // set config and find available options for required service
                var fromDependencies = __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"]
                    .from((this._plugin.dependenciesTemplate || []).slice());
                if (this._plugin.dependenciesTemplate && this._plugin.dependenciesTemplate.length === 0) {
                    this.no_dependencies = true;
                }
                else {
                    this.no_dependencies = false;
                }
                this.loading = true;
                fromDependencies
                    .flatMap(function (dep) { return _this.serviceApi.getByType(_this._plugin.dependenciesTemplate); })
                    .subscribe(function (serviceConfigs) {
                    var tmpl = _this._plugin.dependenciesTemplate
                        .reduce(function (options, v) {
                        var srvs = serviceConfigs.filter(function (sc) { return sc.serviceType == v; });
                        options[v] = {
                            required: true,
                            label: v,
                            help: v + " configuration",
                            type: "select",
                            options: srvs.map(function (s) { return ({ key: s.name, value: s.id }); })
                        };
                        return options;
                    }, {});
                    _this.dependenciesTemplate = tmpl;
                    // set dynamic form template for plugin's dependencies
                    _this.dependenciesValue = _this._plugin.dependencies ? __assign({}, _this._plugin.dependencies) : {};
                    _this.loading = false;
                }, function (err) {
                    _this.loading = false;
                });
                // set dynamic form template for regular settings
                this.settingsValue = Object.assign({}, this._plugin.settings);
            }
        },
        enumerable: true,
        configurable: true
    });
    PluginForm.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.validation.emit(this.valid);
        this.form
            .valueChanges
            .subscribe(function (value) {
            _this.onChange.emit(value);
            _this.validation.emit(_this.valid);
        });
    };
    Object.defineProperty(PluginForm.prototype, "valid", {
        get: function () {
            var isValid = true;
            isValid = this.settForm ? this.settForm.valid : isValid;
            isValid = this.depsForm ? this.depsForm.valid : isValid;
            return isValid;
        },
        enumerable: true,
        configurable: true
    });
    return PluginForm;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('settingsForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__shared__["c" /* DynamicForm */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__shared__["c" /* DynamicForm */]) === "function" && _a || Object)
], PluginForm.prototype, "settForm", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('dependenciesForm'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__shared__["c" /* DynamicForm */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__shared__["c" /* DynamicForm */]) === "function" && _b || Object)
], PluginForm.prototype, "depsForm", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], PluginForm.prototype, "onChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _d || Object)
], PluginForm.prototype, "validation", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__core__["r" /* Plugin */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["r" /* Plugin */]) === "function" && _e || Object),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_4__core__["r" /* Plugin */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["r" /* Plugin */]) === "function" && _f || Object])
], PluginForm.prototype, "plugin", null);
PluginForm = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'plugin-form',
        template: __webpack_require__("../../../../../src/app/+entries/shared/components/plugin-form/plugin-form.tmpl.html"),
        styles: ["\n    \n    .__container {\n        position:relative\n    }\n    \n    "]
    }),
    __metadata("design:paramtypes", [typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4__core__["j" /* ServiceApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__core__["j" /* ServiceApi */]) === "function" && _h || Object])
], PluginForm);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=plugin-form.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugins-set/component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".plugin-item-container {\n  padding: 0;\n  margin: 0;\n  list-style: none; }\n  .plugin-item-container > li {\n    position: relative;\n    padding: 10px;\n    color: #455A64 !important;\n    background: #ffffff;\n    background: #f9f9f9;\n    font-size: 14px;\n    font-weight: 700;\n    cursor: pointer;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1); }\n    .plugin-item-container > li > .button-group {\n      position: absolute;\n      right: 3px;\n      top: 0;\n      height: 100%;\n      font-size: 1em; }\n      .plugin-item-container > li > .button-group > span {\n        margin-left: 1px;\n        color: #455A64; }\n      .plugin-item-container > li > .button-group > span.delete {\n        color: #900000; }\n    .plugin-item-container > li > .content {\n      margin-left: 20px; }\n    .plugin-item-container > li:not(:last-child)::after {\n      content: '';\n      background: url(\"/assets/images/lines.png\") no-repeat 0 0;\n      background-position: -107px 0;\n      width: 2px;\n      display: inline-block;\n      height: 30px;\n      position: absolute;\n      left: 50%;\n      bottom: -30px; }\n    .plugin-item-container > li:not(:last-child):not(:first-child) {\n      margin: 10px 0; }\n    .plugin-item-container > li:first-child {\n      margin: 0 0 10px; }\n    .plugin-item-container > li:last-child {\n      margin: 10px 0 0; }\n    .plugin-item-container > li.active {\n      background: #eeeeee;\n      box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26); }\n    .plugin-item-container > li.invalid::before {\n      display: inline-block;\n      content: '\\F071';\n      font-family: FontAwesome;\n      color: #b36e19;\n      position: absolute;\n      width: 20px;\n      right: -25px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugins-set/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pluginsSet_component__ = __webpack_require__("../../../../../src/app/+entries/shared/components/plugins-set/pluginsSet.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__pluginsSet_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugins-set/pluginsSet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PluginSet; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PluginSet = (function () {
    function PluginSet() {
        this._plugins = [];
        this.all = [];
        this.emitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selected = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.deleted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.added = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.active = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    Object.defineProperty(PluginSet.prototype, "plugins", {
        get: function () {
            return this._plugins;
        },
        set: function (v) {
            this._plugins = v || [];
            if (this._plugins.length > 0) {
                this.activePlugin = this._plugins[0];
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PluginSet.prototype, "_lastOrder", {
        get: function () {
            var lastOrder = 0;
            if (this.plugins.length > 0) {
                lastOrder = this.plugins
                    .reduce(function (prev, current) {
                    return prev.order < current.order ? current : prev;
                }).order;
            }
            return lastOrder;
        },
        enumerable: true,
        configurable: true
    });
    /**
        * Select plugin, action that sets active plugin and show it's settings form
        *
        * @param {Plugin} [plugin]
        * @returns
        *
        * @memberOf StepPlugins
        */
    PluginSet.prototype.selectPluginInPipe = function (plugin) {
        this.active.emit();
        this.plugins.forEach(function (p) {
            p.active = false;
        });
        if (!plugin) {
            if (this.plugins.length > 0) {
                this.plugins[0].active = true;
                this.activePlugin = this.plugins[0];
            }
            else {
                return;
            }
        }
        else {
            console.log('Select plugin:', plugin);
            plugin.active = true;
            this.activePlugin = plugin;
        }
        this.selected.emit(plugin);
    };
    PluginSet.prototype.pluginUp = function (plugin) {
        this.selectPluginInPipe(plugin);
        if (!this.isLast(plugin)) {
            var next = this.plugins[this.plugins.indexOf(plugin) + 1];
            plugin.order++;
            next.order--;
            this._sort();
        }
        this.selected.emit(plugin);
    };
    PluginSet.prototype.pluginDown = function (plugin) {
        this.selectPluginInPipe(plugin);
        if (!this.isFirst(plugin)) {
            var prev = this.plugins[this.plugins.indexOf(plugin) - 1];
            plugin.order--;
            prev.order++;
            this._sort();
        }
        this.selected.emit(plugin);
    };
    PluginSet.prototype.pluginDelete = function (plugin) {
        var index = this.plugins.indexOf(plugin);
        this.plugins.splice(index, 1);
        if (this.plugins[0])
            this.selectPluginInPipe();
        this.deleted.emit();
    };
    PluginSet.prototype.isLast = function (plugin) {
        return (plugin === this.plugins
            .reduce(function (prev, current) {
            return prev.order < current.order ? current : prev;
        }));
    };
    PluginSet.prototype.isFirst = function (plugin) {
        return (plugin === this.plugins
            .reduce(function (prev, current) {
            return prev.order > current.order ? current : prev;
        }));
    };
    PluginSet.prototype._sort = function () {
        this.plugins.sort(function (a, b) {
            return a.order - b.order;
        });
    };
    PluginSet.prototype.selectLast = function () {
        if (this.plugins.length > 0) {
            var pluginToSelect = this.plugins[this.plugins.length - 1];
            this.selectPluginInPipe(pluginToSelect);
        }
    };
    return PluginSet;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('close'),
    __metadata("design:type", Object)
], PluginSet.prototype, "close", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('contentPlugins'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _a || Object)
], PluginSet.prototype, "content", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], PluginSet.prototype, "plugins", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], PluginSet.prototype, "all", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('pluginsChange'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], PluginSet.prototype, "emitter", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], PluginSet.prototype, "selected", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _d || Object)
], PluginSet.prototype, "deleted", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _e || Object)
], PluginSet.prototype, "added", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _f || Object)
], PluginSet.prototype, "active", void 0);
PluginSet = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'plugins-set',
        template: __webpack_require__("../../../../../src/app/+entries/shared/components/plugins-set/pluginsSet.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/shared/components/plugins-set/component.scss")]
    })
], PluginSet);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=pluginsSet.component.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/plugins-set/pluginsSet.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<ul class=\"plugin-item-container\" [hidden]=\"plugins.length == 0\">\r\n    <li *ngFor=\"let plugin of plugins\" [ngClass]=\"{active: plugin?.active, invalid: !plugin?.valid}\" (click)=\"selectPluginInPipe(plugin)\">\r\n        <div class=\"content\">\r\n            {{plugin.order}}: {{plugin.name | capitalize}}\r\n        </div>\r\n        <div class=\"button-group\">\r\n            <span class='down' [hidden]='isFirst(plugin)' (click)='pluginDown(plugin)'>             \r\n                                    <i class=\"fa fa fa-arrow-circle-up\"></i>              \r\n                                </span>\r\n            <span class='up' [hidden]='isLast(plugin)' (click)='pluginUp(plugin)'>            \r\n                                    <i class=\"fa fa fa-arrow-circle-down\"></i>               \r\n                                </span>\r\n            <span class='delete' (click)='pluginDelete(plugin)'>\r\n                                    <i class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\r\n                                 </span>\r\n        </div>\r\n    </li>\r\n</ul>\r\n"

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/proxy-form/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__proxy_form__ = __webpack_require__("../../../../../src/app/+entries/shared/components/proxy-form/proxy-form.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__proxy_form__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/proxy-form/proxy-form.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"proxy-form\">\r\n    <form [formGroup]='methodsForm' class='form-horizontal' role='form'>\r\n        <div class=\"proxy-form__methods\" [class.hidden]='rule?.base'>\r\n            <toggleGroup [options]=\"options\" formControlName=\"methods\" [(ngModel)]=\"methods\">\r\n            </toggleGroup>\r\n        </div>\r\n    </form>\r\n    <form [formGroup]='proxyForm' class='form-horizontal' role='form'>\r\n        <div class=\"proxy-form__header\">\r\n            <h3 style='position: relative'>\r\n                Proxy\r\n                <i class=\"icon-help-circled\" style='position: absolute;\r\n                right: -18px;\r\n                top: 0px;\r\n                font-size: 15px;\r\n                color: darkgrey;'></i>\r\n            </h3>\r\n            <span *ngIf='!rule?.base'>\r\n                <label for='override-switch' (click)='override.toggleValue()'><small>{{ proxy.active ? \"Override\": \"Use\" }} parent config</small></label>\r\n                <loc-switch #override id='override-switch' class='health__switch' formControlName=\"active\" [(ngModel)]='proxy.active' [type]=\"'rectangle'\" style='transform: translateY(4px);\r\n    display: inline-block;'></loc-switch>\r\n            </span>           \r\n        </div>\r\n        <div class=\"m-form__group\">\r\n            <loc-input-text label='Proxy target' formControlName=\"target\" [(ngModel)]='proxy.target' id='target' required validateUrl></loc-input-text>\r\n            <show-error *ngIf=\"proxyForm.dirty\" [control]=\"proxyForm.get('target')\" [options]=\"{'required': 'Field is required', 'validateUrl': 'Wrong url format'}\">\r\n            </show-error>\r\n        </div>\r\n        <div class=\"m-form__group\">\r\n            <loc-input-text label='Proxy timeout (ms)' formControlName=\"proxyTimeout\" type='number' [(ngModel)]='proxy.proxyTimeout'\r\n                id='target'></loc-input-text>\r\n        </div>\r\n        <div class=\"m-form__group\">\r\n            <loc-input-text label='Basic auth' formControlName=\"auth\" [(ngModel)]='proxy.auth' id='auth' required></loc-input-text>\r\n            <show-error *ngIf=\"proxy.auth\" [control]=\"proxyForm.get('auth')\" [options]=\"{'required': 'Field is required', 'validateUrl': 'Wrong url format'}\">\r\n            </show-error>\r\n        </div>\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>Add x-forward headers</span>\r\n            <loc-switch class='health__switch' formControlName=\"xfwd\" [(ngModel)]='proxy.xfwd' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>Passes the absolute URL as the path</span>\r\n            <loc-switch class='health__switch' formControlName=\"toProxy\" [(ngModel)]='proxy.toProxy' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>Secure</span>\r\n            <loc-switch class='health__switch' formControlName=\"secure\" [(ngModel)]='proxy.secure' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>Prepend path</span>\r\n            <loc-switch class='health__switch' formControlName=\"prependPath\" [(ngModel)]='proxy.prependPath' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>IgnorePath path</span>\r\n            <loc-switch class='health__switch' formControlName=\"ignorePath\" [(ngModel)]='proxy.ignorePath' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>Change origin</span>\r\n            <loc-switch class='health__switch' formControlName=\"changeOrigin\" [(ngModel)]='proxy.changeOrigin' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n\r\n        <div class=\"m-form__group proxy-form__inline\">\r\n            <span>Proxy Websockets</span>\r\n            <loc-switch class='health__switch' formControlName=\"ws\" [(ngModel)]='proxy.ws' [type]=\"'rectangle'\">\r\n            </loc-switch>\r\n        </div>\r\n    </form>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/proxy-form/proxy-form.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProxyForm; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProxyForm = (function () {
    function ProxyForm(_builder) {
        this._builder = _builder;
        this.onChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.ruleChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.validation = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this._rule = {};
        this._proxy = {};
        this._methods = [];
        this.options = [
            { name: 'GET', description: 'GET' },
            { name: 'POST', description: 'POST' },
            { name: 'PUT', description: 'PUT' },
            { name: 'DELETE', description: 'DELETE' },
            { name: 'HEAD', description: 'HEAD' },
            { name: 'PATCH', description: 'PATCH' }
        ];
        this.proxyForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["i" /* FormGroup */]({
            active: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            target: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: '' }, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required),
            prependPath: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            secure: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            ignorePath: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            changeOrigin: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            toProxy: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            xfwd: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            auth: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: '' }),
            ws: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: false }),
            proxyTimeout: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: 3000 }),
        });
        this.methodsForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["i" /* FormGroup */]({
            methods: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* FormControl */]({ value: [] })
        });
    }
    Object.defineProperty(ProxyForm.prototype, "rule", {
        get: function () {
            return this._rule;
        },
        set: function (value) {
            if (value) {
                this.proxy = value.proxy;
                this.methods = value.methods;
                this.methods = value.methods;
                this._rule = value;
                //  this.policy = value.policy || "{}"
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProxyForm.prototype, "proxy", {
        get: function () {
            return this._proxy;
        },
        set: function (value) {
            var _this = this;
            if (value) {
                if (!value.active) {
                    Object.keys(this.proxyForm.controls).forEach(function (c) {
                        if (c != 'active')
                            _this.proxyForm.controls[c].disable();
                    });
                }
                else {
                    Object.keys(this.proxyForm.controls).forEach(function (c) {
                        if (c != 'active')
                            _this.proxyForm.controls[c].enable();
                    });
                }
                this._proxy = Object.assign({}, value);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProxyForm.prototype, "methods", {
        get: function () {
            return this._methods;
        },
        set: function (value) {
            if (value && Array.isArray(value)) {
                this._methods = value.slice();
            }
        },
        enumerable: true,
        configurable: true
    });
    ProxyForm.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.proxyForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe(function (value) {
            _this.rule.proxy = _this.proxy;
            _this.rule.proxy.proxyTimeout = +_this.rule.proxy.proxyTimeout || 3000;
            _this.ruleChange.emit(_this.rule);
            // this.validation.emit(this.valid);
        });
        this.methodsForm
            .valueChanges
            .distinctUntilChanged()
            .subscribe(function (value) {
            _this.rule.methods = value.methods;
            _this.ruleChange.emit(_this.rule);
            // this.validation.emit(this.valid);
        });
    };
    Object.defineProperty(ProxyForm.prototype, "valid", {
        get: function () {
            var isValid = true;
            isValid = this.proxyForm ? this.proxyForm.valid : isValid;
            return isValid;
        },
        enumerable: true,
        configurable: true
    });
    return ProxyForm;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], ProxyForm.prototype, "onChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], ProxyForm.prototype, "ruleChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _c || Object)
], ProxyForm.prototype, "validation", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__core__["p" /* Rule */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["p" /* Rule */]) === "function" && _d || Object),
    __metadata("design:paramtypes", [typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__core__["p" /* Rule */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["p" /* Rule */]) === "function" && _e || Object])
], ProxyForm.prototype, "rule", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__core__["q" /* Proxy */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["q" /* Proxy */]) === "function" && _f || Object),
    __metadata("design:paramtypes", [typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__core__["q" /* Proxy */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__core__["q" /* Proxy */]) === "function" && _g || Object])
], ProxyForm.prototype, "proxy", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], ProxyForm.prototype, "methods", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], ProxyForm.prototype, "active", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], ProxyForm.prototype, "base", void 0);
ProxyForm = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'rule-form',
        template: __webpack_require__("../../../../../src/app/+entries/shared/components/proxy-form/proxy-form.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/shared/components/proxy-form/proxy-settings-form.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* FormBuilder */]) === "function" && _h || Object])
], ProxyForm);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=proxy-form.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/proxy-form/proxy-settings-form.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".proxy-form {\n  width: 100%;\n  display: block; }\n  .proxy-form__inline {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    line-height: 1rem; }\n    .proxy-form__inline--active {\n      -webkit-box-pack: start;\n          -ms-flex-pack: start;\n              justify-content: flex-start; }\n      .proxy-form__inline--active > * {\n        margin-right: 1.3rem; }\n  .proxy-form__methods {\n    margin-bottom: 20px; }\n    .proxy-form__methods.hidden {\n      display: none; }\n  .proxy-form__header {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    border-bottom: 1px solid #eee;\n    margin-bottom: 25px; }\n\n:host {\n  width: 100%; }\n\n:host /deep/ .control-container > * {\n  -webkit-box-flex: 1 !important;\n      -ms-flex: 1 0 16.6666% !important;\n          flex: 1 0 16.6666% !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/step-tabs/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__step_tabs__ = __webpack_require__("../../../../../src/app/+entries/shared/components/step-tabs/step-tabs.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__step_tabs_pane__ = __webpack_require__("../../../../../src/app/+entries/shared/components/step-tabs/step-tabs-pane.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__step_tabs__["a"]; });
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return STEP_TABS_COMPONENTS; });




var STEP_TABS_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__step_tabs__["a" /* UiTabs */],
    __WEBPACK_IMPORTED_MODULE_1__step_tabs_pane__["a" /* UiPane */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/step-tabs/step-tabs-pane.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UiPane; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UiPane = (function () {
    function UiPane() {
        this.valid = true;
        this.visited = false;
        this._active = false;
    }
    Object.defineProperty(UiPane.prototype, "current", {
        get: function () { return !this.active; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UiPane.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (active) {
            if (active == this._active)
                return;
            this._active = active;
        },
        enumerable: true,
        configurable: true
    });
    return UiPane;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.hidden'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [])
], UiPane.prototype, "current", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], UiPane.prototype, "id", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], UiPane.prototype, "valid", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], UiPane.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], UiPane.prototype, "active", null);
UiPane = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ui-pane',
        template: "\n        <div class='pane-content'>           \n            <ng-content></ng-content>                    \n        </div>\n    ",
        styles: [
            "       \n        .pane-content{  \n            flex: 1;        \n            display: flex;\n            flex-direction: column;   \n            padding-bottom: 10px;\n        }\n        :host {\n            display: flex;\n            flex: 1;\n            flex-direction: column;\n        }\n        :host.hidden {\n            display: none;\n        }\n    "
        ],
    }),
    __metadata("design:paramtypes", [])
], UiPane);

//# sourceMappingURL=step-tabs-pane.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/step-tabs/step-tabs.tmpl.html":
/***/ (function(module, exports) {

module.exports = " <div class='l-container'>\r\n      <div class=\"l-row\">\r\n         <div class=\"col-sm-12 col-md-2 padding-shrink-right l-col\">           \r\n            <ul class=\"my-steps\">\r\n                <li *ngFor=\"let pane of panes\" class='{{ pane.id }}' \r\n                    (click)=\"goTo(pane.id)\"\r\n                    role=\"presentation\" \r\n                    [ngClass] = \"{ invalid: !pane.valid, active: pane.active, visited: pane.visited }\">\r\n                    <i class='fa'></i>\r\n                    <span>{{ pane.title }}</span> \r\n                </li>\r\n                <!-- <li class='save' \r\n                    (click)=\"save.emit()\"                  \r\n                    [ngClass] = \"{ disabled: isSomeInvalid() }\">\r\n                    <i></i>\r\n                    <span>Save</span> \r\n                </li> -->\r\n            </ul>            \r\n         </div>\r\n         <div class=\"col-sm-12 col-md-10 padding-shrink-left l-col\" style='display: flex;' flexy [bottom]='50'>                \r\n              <ng-content></ng-content>                        \r\n         </div>\r\n      </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/step-tabs/step-tabs.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__step_tabs_pane__ = __webpack_require__("../../../../../src/app/+entries/shared/components/step-tabs/step-tabs-pane.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UiTabs; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UiTabs = (function () {
    function UiTabs() {
        this.save = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    UiTabs.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.panes) {
            this.default
                ? this.currentPane = this.panes.toArray().find(function (p) { return p.id == _this.default; })
                : this.currentPane = this.panes.first;
            this.currentPane.active = true;
        }
    };
    UiTabs.prototype.goTo = function (id) {
        if (this.panes) {
            if (this.currentPane) {
                this.currentPane.visited = true;
            }
            this.panes.toArray().forEach(function (p) { return p.active = false; });
            this.currentPane = this.panes.toArray().find(function (p) { return p.id == id; });
            this.currentPane.active = true;
        }
    };
    UiTabs.prototype.isSomeInvalid = function () {
        return this.panes.some(function (p) { return !p.valid; });
    };
    return UiTabs;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ContentChildren"])(__WEBPACK_IMPORTED_MODULE_1__step_tabs_pane__["a" /* UiPane */]),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"]) === "function" && _a || Object)
], UiTabs.prototype, "panes", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], UiTabs.prototype, "default", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], UiTabs.prototype, "save", void 0);
UiTabs = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ui-tabs',
        template: __webpack_require__("../../../../../src/app/+entries/shared/components/step-tabs/step-tabs.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/shared/components/step-tabs/tabs.scss")]
    })
], UiTabs);

var _a, _b;
//# sourceMappingURL=step-tabs.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/step-tabs/tabs.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".my-steps {\n  padding: 0;\n  float: right;\n  width: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  height: 100%;\n  /*tab's images*/ }\n  @media (max-width: 47.9em) {\n    .my-steps {\n      float: none;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: row;\n              flex-direction: row;\n      height: auto; } }\n  .my-steps li {\n    list-style: none;\n    background: #fff;\n    box-shadow: 0 0 0 1px #F8F8F8 inset, 0 0 0 1px #e0e0e0;\n    color: rgba(69, 90, 100, 0.7);\n    height: 80px;\n    margin: 0 0 7px;\n    padding-top: 17px;\n    text-align: center;\n    cursor: pointer;\n    text-decoration: none;\n    width: 80px;\n    position: relative; }\n    @media (max-width: 47.9em) {\n      .my-steps li {\n        display: inline-block;\n        margin: 0;\n        width: 33.333333%; } }\n    .my-steps li:last-of-type::after {\n      display: none; }\n  .my-steps > li > span {\n    display: block; }\n  .my-steps > li > i {\n    font-size: 1.5rem; }\n    .my-steps > li > i::before {\n      font-family: FontAwesome;\n      font-style: normal;\n      font-weight: normal;\n      text-decoration: inherit; }\n  .my-steps > li.general > i::before {\n    content: '\\F013'; }\n  .my-steps > li.plugins > i::before {\n    content: '\\F1E0'; }\n  .my-steps > li.preview > i::before {\n    content: '\\F00C'; }\n  .my-steps > li.healthcheck > i::before {\n    content: 'Y';\n    font-family: \"loc\" !important; }\n  .my-steps > li.docs > i::before {\n    content: '0';\n    font-family: \"loc\" !important; }\n  .my-steps > li.save > i::before {\n    content: \"Z\";\n    font-family: \"loc\" !important; }\n  .my-steps > li.save {\n    margin-top: auto;\n    margin-bottom: 59px;\n    background: #27a93e;\n    color: white;\n    box-shadow: none;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding-top: 0;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center; }\n    .my-steps > li.save.disabled {\n      background: #a6b0b7;\n      cursor: not-allowed; }\n  .my-steps > li.active {\n    color: #555; }\n    .my-steps > li.active::before {\n      position: absolute;\n      top: 0;\n      bottom: 0;\n      left: 0;\n      width: 2px;\n      content: \"\";\n      background-color: #d26911; }\n      @media (max-width: 47.9em) {\n        .my-steps > li.active::before {\n          width: 100%;\n          height: 2px;\n          top: auto; } }\n  .my-steps > li.invalid.visited {\n    color: #900000 !important; }\n  .my-steps span {\n    text-decoration: none;\n    color: inherit;\n    cursor: pointer; }\n  @media screen and (max-width: 767px) {\n    .my-steps {\n      margin-bottom: 10px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/toggle-group/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__toggle_group__ = __webpack_require__("../../../../../src/app/+entries/shared/components/toggle-group/toggle-group.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__toggle_group__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/toggle-group/toggle-group.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".control-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  color: #8a8a8a; }\n\n.control-container > div {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 0 25%;\n          flex: 1 0 25%; }\n  @media screen and (max-width: 767px) {\n    .control-container > div {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 0 50%;\n              flex: 1 0 50%;\n      -ms-flex-wrap: wrap;\n          flex-wrap: wrap; } }\n\n.control-container .check-box {\n  margin: 2px;\n  padding: 8px;\n  background: #ffffff;\n  border: 1px dotted #cfd8dc;\n  font-size: .9rem;\n  font-weight: 400;\n  transition: all .1s ease-in-out;\n  overflow: hidden;\n  text-decoration: none;\n  cursor: pointer;\n  text-align: center;\n  position: relative; }\n\n.__icon {\n  position: absolute;\n  left: .625rem; }\n\n.control-container .check-box.active {\n  color: #555;\n  border: 1px solid #cfd8dc; }\n  .control-container .check-box.active .__icon {\n    color: #27a93e; }\n\n.control-container .check-box span.content {\n  padding: 0 5px; }\n\n.control-container .check-box span.text {\n  padding: 0 8px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/toggle-group/toggle-group.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"control-container\">\r\n    <div *ngFor=\"let option of options; let i = index\">\r\n        <div class='check-box' [class.active]=\"option.active\" (click)=\"onCheck(i)\">\r\n            <i class=\"fa fa-circle __icon\" *ngIf='option.active' aria-hidden=\"true\" style='top: 11px;'></i>\r\n            <i class=\"fa fa-circle-o __icon\" *ngIf='!option.active' aria-hidden=\"true\" style='top: 11px;'></i>\r\n            <span class=\"text\">{{option.description}}</span>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/+entries/shared/components/toggle-group/toggle-group.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToggleGroup; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ToggleGroup = (function () {
    function ToggleGroup(ngControl) {
        this.ngControl = ngControl;
        this._selectedOptions = [];
        this._options = [];
        /**
         * ControlValueAccessor
         */
        this.onChange = function (_) {
        };
        this.onTouched = function () {
        };
        if (ngControl)
            ngControl.valueAccessor = this;
    }
    Object.defineProperty(ToggleGroup.prototype, "options", {
        get: function () {
            return this._options;
        },
        set: function (op) {
            if (op) {
                this._options = op;
                this.__update();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ToggleGroup.prototype, "selectedOptions", {
        get: function () {
            return this._selectedOptions;
        },
        set: function (arrayOfSelected) {
        },
        enumerable: true,
        configurable: true
    });
    ToggleGroup.prototype.onCheck = function (index) {
        this.options[index].active = !this.options[index].active;
        var activeOptions = this.options.filter(function (option) { return option.active; });
        var newValue = activeOptions.map(function (option) { return option.name; });
        if (this.ngControl)
            this.ngControl.viewToModelUpdate(newValue);
        this.onChange(newValue);
    };
    ToggleGroup.prototype.__update = function () {
        var _this = this;
        if (this._selectedOptions && this._selectedOptions.length > 0 && this._options && this._options.length > 0) {
            this._options = this._options.map(function (option) {
                return _this._selectedOptions.indexOf(option.name) > -1
                    ? Object.assign(option, { active: true })
                    : Object.assign(option, { active: false });
            });
        }
    };
    ToggleGroup.prototype.writeValue = function (value) {
        if (value) {
            this._selectedOptions = value;
            this.__update();
        }
    };
    ToggleGroup.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    ToggleGroup.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    return ToggleGroup;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], ToggleGroup.prototype, "options", null);
ToggleGroup = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'toggleGroup[ngModel]',
        template: __webpack_require__("../../../../../src/app/+entries/shared/components/toggle-group/toggle-group.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+entries/shared/components/toggle-group/toggle-group.scss")]
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Optional"])()), __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Self"])()),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* NgControl */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* NgControl */]) === "function" && _a || Object])
], ToggleGroup);

var _a;
//# sourceMappingURL=toggle-group.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/directives/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENTRIES_DIRECTIVES; });
var ENTRIES_DIRECTIVES = [];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/entries.shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components__ = __webpack_require__("../../../../../src/app/+entries/shared/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__directives__ = __webpack_require__("../../../../../src/app/+entries/shared/directives/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes__ = __webpack_require__("../../../../../src/app/+entries/shared/pipes/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers__ = __webpack_require__("../../../../../src/app/+entries/shared/providers/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EntriesSharedModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var DECLARATIONS = __WEBPACK_IMPORTED_MODULE_2__components__["a" /* ENTRIES_COMPONENTS */].concat(__WEBPACK_IMPORTED_MODULE_3__directives__["a" /* ENTRIES_DIRECTIVES */], __WEBPACK_IMPORTED_MODULE_4__pipes__["a" /* ENTRIES_PIPES */]);
var EntriesSharedModule = (function () {
    function EntriesSharedModule() {
    }
    return EntriesSharedModule;
}());
EntriesSharedModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: DECLARATIONS,
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__shared__["a" /* SharedModule */]
        ],
        providers: __WEBPACK_IMPORTED_MODULE_5__providers__["a" /* ENTRIES_PROVIDERS */].slice(),
        exports: DECLARATIONS
    })
], EntriesSharedModule);

//# sourceMappingURL=entries.shared.module.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entries_shared_module__ = __webpack_require__("../../../../../src/app/+entries/shared/entries.shared.module.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components__ = __webpack_require__("../../../../../src/app/+entries/shared/components/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__components__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__components__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__("../../../../../src/app/+entries/shared/providers/index.ts");
/* unused harmony namespace reexport */



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/pipes/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENTRIES_PIPES; });
var ENTRIES_PIPES = [];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/providers/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__status_resolver__ = __webpack_require__("../../../../../src/app/+entries/shared/providers/status.resolver.ts");
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ENTRIES_PROVIDERS; });


var ENTRIES_PROVIDERS = [
    __WEBPACK_IMPORTED_MODULE_0__status_resolver__["a" /* StatusResolver */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+entries/shared/providers/status.resolver.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StatusResolver = (function () {
    function StatusResolver(_serviceApi) {
        this._serviceApi = _serviceApi;
    }
    StatusResolver.prototype.resolve = function (route, state) {
        return this._serviceApi.summary(route.params['id']);
    };
    return StatusResolver;
}());
StatusResolver = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core__["j" /* ServiceApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core__["j" /* ServiceApi */]) === "function" && _a || Object])
], StatusResolver);

var _a;
//# sourceMappingURL=status.resolver.js.map

/***/ }),

/***/ "../../../../file-saver/FileSaver.js":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/* FileSaver.js
 * A saveAs() FileSaver implementation.
 * 1.3.2
 * 2016-06-16 18:25:19
 *
 * By Eli Grey, http://eligrey.com
 * License: MIT
 *   See https://github.com/eligrey/FileSaver.js/blob/master/LICENSE.md
 */

/*global self */
/*jslint bitwise: true, indent: 4, laxbreak: true, laxcomma: true, smarttabs: true, plusplus: true */

/*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */

var saveAs = saveAs || (function(view) {
	"use strict";
	// IE <10 is explicitly unsupported
	if (typeof view === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
		return;
	}
	var
		  doc = view.document
		  // only get URL when necessary in case Blob.js hasn't overridden it yet
		, get_URL = function() {
			return view.URL || view.webkitURL || view;
		}
		, save_link = doc.createElementNS("http://www.w3.org/1999/xhtml", "a")
		, can_use_save_link = "download" in save_link
		, click = function(node) {
			var event = new MouseEvent("click");
			node.dispatchEvent(event);
		}
		, is_safari = /constructor/i.test(view.HTMLElement) || view.safari
		, is_chrome_ios =/CriOS\/[\d]+/.test(navigator.userAgent)
		, throw_outside = function(ex) {
			(view.setImmediate || view.setTimeout)(function() {
				throw ex;
			}, 0);
		}
		, force_saveable_type = "application/octet-stream"
		// the Blob API is fundamentally broken as there is no "downloadfinished" event to subscribe to
		, arbitrary_revoke_timeout = 1000 * 40 // in ms
		, revoke = function(file) {
			var revoker = function() {
				if (typeof file === "string") { // file is an object URL
					get_URL().revokeObjectURL(file);
				} else { // file is a File
					file.remove();
				}
			};
			setTimeout(revoker, arbitrary_revoke_timeout);
		}
		, dispatch = function(filesaver, event_types, event) {
			event_types = [].concat(event_types);
			var i = event_types.length;
			while (i--) {
				var listener = filesaver["on" + event_types[i]];
				if (typeof listener === "function") {
					try {
						listener.call(filesaver, event || filesaver);
					} catch (ex) {
						throw_outside(ex);
					}
				}
			}
		}
		, auto_bom = function(blob) {
			// prepend BOM for UTF-8 XML and text/* types (including HTML)
			// note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
			if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
				return new Blob([String.fromCharCode(0xFEFF), blob], {type: blob.type});
			}
			return blob;
		}
		, FileSaver = function(blob, name, no_auto_bom) {
			if (!no_auto_bom) {
				blob = auto_bom(blob);
			}
			// First try a.download, then web filesystem, then object URLs
			var
				  filesaver = this
				, type = blob.type
				, force = type === force_saveable_type
				, object_url
				, dispatch_all = function() {
					dispatch(filesaver, "writestart progress write writeend".split(" "));
				}
				// on any filesys errors revert to saving with object URLs
				, fs_error = function() {
					if ((is_chrome_ios || (force && is_safari)) && view.FileReader) {
						// Safari doesn't allow downloading of blob urls
						var reader = new FileReader();
						reader.onloadend = function() {
							var url = is_chrome_ios ? reader.result : reader.result.replace(/^data:[^;]*;/, 'data:attachment/file;');
							var popup = view.open(url, '_blank');
							if(!popup) view.location.href = url;
							url=undefined; // release reference before dispatching
							filesaver.readyState = filesaver.DONE;
							dispatch_all();
						};
						reader.readAsDataURL(blob);
						filesaver.readyState = filesaver.INIT;
						return;
					}
					// don't create more object URLs than needed
					if (!object_url) {
						object_url = get_URL().createObjectURL(blob);
					}
					if (force) {
						view.location.href = object_url;
					} else {
						var opened = view.open(object_url, "_blank");
						if (!opened) {
							// Apple does not allow window.open, see https://developer.apple.com/library/safari/documentation/Tools/Conceptual/SafariExtensionGuide/WorkingwithWindowsandTabs/WorkingwithWindowsandTabs.html
							view.location.href = object_url;
						}
					}
					filesaver.readyState = filesaver.DONE;
					dispatch_all();
					revoke(object_url);
				}
			;
			filesaver.readyState = filesaver.INIT;

			if (can_use_save_link) {
				object_url = get_URL().createObjectURL(blob);
				setTimeout(function() {
					save_link.href = object_url;
					save_link.download = name;
					click(save_link);
					dispatch_all();
					revoke(object_url);
					filesaver.readyState = filesaver.DONE;
				});
				return;
			}

			fs_error();
		}
		, FS_proto = FileSaver.prototype
		, saveAs = function(blob, name, no_auto_bom) {
			return new FileSaver(blob, name || blob.name || "download", no_auto_bom);
		}
	;
	// IE 10+ (native saveAs)
	if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
		return function(blob, name, no_auto_bom) {
			name = name || blob.name || "download";

			if (!no_auto_bom) {
				blob = auto_bom(blob);
			}
			return navigator.msSaveOrOpenBlob(blob, name);
		};
	}

	FS_proto.abort = function(){};
	FS_proto.readyState = FS_proto.INIT = 0;
	FS_proto.WRITING = 1;
	FS_proto.DONE = 2;

	FS_proto.error =
	FS_proto.onwritestart =
	FS_proto.onprogress =
	FS_proto.onwrite =
	FS_proto.onabort =
	FS_proto.onerror =
	FS_proto.onwriteend =
		null;

	return saveAs;
}(
	   typeof self !== "undefined" && self
	|| typeof window !== "undefined" && window
	|| this.content
));
// `self` is undefined in Firefox for Android content script context
// while `this` is nsIContentFrameMessageManager
// with an attribute `content` that corresponds to the window

if (typeof module !== "undefined" && module.exports) {
  module.exports.saveAs = saveAs;
} else if (("function" !== "undefined" && __webpack_require__("../../../../webpack/buildin/amd-define.js") !== null) && (__webpack_require__("../../../../webpack/buildin/amd-options.js") !== null)) {
  !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
    return saveAs;
  }.call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
}


/***/ }),

/***/ "../../../../ng2-charts/ng2-charts.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__("../../../../ng2-charts/index.js"));


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-drop.directive.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var file_uploader_class_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-uploader.class.js");
var FileDropDirective = (function () {
    function FileDropDirective(element) {
        this.fileOver = new core_1.EventEmitter();
        this.onFileDrop = new core_1.EventEmitter();
        this.element = element;
    }
    FileDropDirective.prototype.getOptions = function () {
        return this.uploader.options;
    };
    FileDropDirective.prototype.getFilters = function () {
        return {};
    };
    FileDropDirective.prototype.onDrop = function (event) {
        var transfer = this._getTransfer(event);
        if (!transfer) {
            return;
        }
        var options = this.getOptions();
        var filters = this.getFilters();
        this._preventAndStop(event);
        this.uploader.addToQueue(transfer.files, options, filters);
        this.fileOver.emit(false);
        this.onFileDrop.emit(transfer.files);
    };
    FileDropDirective.prototype.onDragOver = function (event) {
        var transfer = this._getTransfer(event);
        if (!this._haveFiles(transfer.types)) {
            return;
        }
        transfer.dropEffect = 'copy';
        this._preventAndStop(event);
        this.fileOver.emit(true);
    };
    FileDropDirective.prototype.onDragLeave = function (event) {
        if (this.element) {
            if (event.currentTarget === this.element[0]) {
                return;
            }
        }
        this._preventAndStop(event);
        this.fileOver.emit(false);
    };
    FileDropDirective.prototype._getTransfer = function (event) {
        return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer; // jQuery fix;
    };
    FileDropDirective.prototype._preventAndStop = function (event) {
        event.preventDefault();
        event.stopPropagation();
    };
    FileDropDirective.prototype._haveFiles = function (types) {
        if (!types) {
            return false;
        }
        if (types.indexOf) {
            return types.indexOf('Files') !== -1;
        }
        else if (types.contains) {
            return types.contains('Files');
        }
        else {
            return false;
        }
    };
    return FileDropDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", file_uploader_class_1.FileUploader)
], FileDropDirective.prototype, "uploader", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], FileDropDirective.prototype, "fileOver", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], FileDropDirective.prototype, "onFileDrop", void 0);
__decorate([
    core_1.HostListener('drop', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], FileDropDirective.prototype, "onDrop", null);
__decorate([
    core_1.HostListener('dragover', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], FileDropDirective.prototype, "onDragOver", null);
__decorate([
    core_1.HostListener('dragleave', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], FileDropDirective.prototype, "onDragLeave", null);
FileDropDirective = __decorate([
    core_1.Directive({ selector: '[ng2FileDrop]' }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], FileDropDirective);
exports.FileDropDirective = FileDropDirective;


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-item.class.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var file_like_object_class_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-like-object.class.js");
var FileItem = (function () {
    function FileItem(uploader, some, options) {
        this.url = '/';
        this.headers = [];
        this.withCredentials = true;
        this.formData = [];
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = false;
        this.isSuccess = false;
        this.isCancel = false;
        this.isError = false;
        this.progress = 0;
        this.index = void 0;
        this.uploader = uploader;
        this.some = some;
        this.options = options;
        this.file = new file_like_object_class_1.FileLikeObject(some);
        this._file = some;
        if (uploader.options) {
            this.method = uploader.options.method || 'POST';
            this.alias = uploader.options.itemAlias || 'file';
        }
        this.url = uploader.options.url;
    }
    FileItem.prototype.upload = function () {
        try {
            this.uploader.uploadItem(this);
        }
        catch (e) {
            this.uploader._onCompleteItem(this, '', 0, {});
            this.uploader._onErrorItem(this, '', 0, {});
        }
    };
    FileItem.prototype.cancel = function () {
        this.uploader.cancelItem(this);
    };
    FileItem.prototype.remove = function () {
        this.uploader.removeFromQueue(this);
    };
    FileItem.prototype.onBeforeUpload = function () {
        return void 0;
    };
    FileItem.prototype.onBuildForm = function (form) {
        return { form: form };
    };
    FileItem.prototype.onProgress = function (progress) {
        return { progress: progress };
    };
    FileItem.prototype.onSuccess = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype.onError = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype.onCancel = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype.onComplete = function (response, status, headers) {
        return { response: response, status: status, headers: headers };
    };
    FileItem.prototype._onBeforeUpload = function () {
        this.isReady = true;
        this.isUploading = true;
        this.isUploaded = false;
        this.isSuccess = false;
        this.isCancel = false;
        this.isError = false;
        this.progress = 0;
        this.onBeforeUpload();
    };
    FileItem.prototype._onBuildForm = function (form) {
        this.onBuildForm(form);
    };
    FileItem.prototype._onProgress = function (progress) {
        this.progress = progress;
        this.onProgress(progress);
    };
    FileItem.prototype._onSuccess = function (response, status, headers) {
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = true;
        this.isSuccess = true;
        this.isCancel = false;
        this.isError = false;
        this.progress = 100;
        this.index = void 0;
        this.onSuccess(response, status, headers);
    };
    FileItem.prototype._onError = function (response, status, headers) {
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = true;
        this.isSuccess = false;
        this.isCancel = false;
        this.isError = true;
        this.progress = 0;
        this.index = void 0;
        this.onError(response, status, headers);
    };
    FileItem.prototype._onCancel = function (response, status, headers) {
        this.isReady = false;
        this.isUploading = false;
        this.isUploaded = false;
        this.isSuccess = false;
        this.isCancel = true;
        this.isError = false;
        this.progress = 0;
        this.index = void 0;
        this.onCancel(response, status, headers);
    };
    FileItem.prototype._onComplete = function (response, status, headers) {
        this.onComplete(response, status, headers);
        if (this.uploader.options.removeAfterUpload) {
            this.remove();
        }
    };
    FileItem.prototype._prepareToUploading = function () {
        this.index = this.index || ++this.uploader._nextIndex;
        this.isReady = true;
    };
    return FileItem;
}());
exports.FileItem = FileItem;


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-like-object.class.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function isElement(node) {
    return !!(node && (node.nodeName || node.prop && node.attr && node.find));
}
var FileLikeObject = (function () {
    function FileLikeObject(fileOrInput) {
        var isInput = isElement(fileOrInput);
        var fakePathOrObject = isInput ? fileOrInput.value : fileOrInput;
        var postfix = typeof fakePathOrObject === 'string' ? 'FakePath' : 'Object';
        var method = '_createFrom' + postfix;
        this[method](fakePathOrObject);
    }
    FileLikeObject.prototype._createFromFakePath = function (path) {
        this.lastModifiedDate = void 0;
        this.size = void 0;
        this.type = 'like/' + path.slice(path.lastIndexOf('.') + 1).toLowerCase();
        this.name = path.slice(path.lastIndexOf('/') + path.lastIndexOf('\\') + 2);
    };
    FileLikeObject.prototype._createFromObject = function (object) {
        // this.lastModifiedDate = copy(object.lastModifiedDate);
        this.size = object.size;
        this.type = object.type;
        this.name = object.name;
    };
    return FileLikeObject;
}());
exports.FileLikeObject = FileLikeObject;


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-select.directive.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var file_uploader_class_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-uploader.class.js");
// todo: filters
var FileSelectDirective = (function () {
    function FileSelectDirective(element) {
        this.element = element;
    }
    FileSelectDirective.prototype.getOptions = function () {
        return this.uploader.options;
    };
    FileSelectDirective.prototype.getFilters = function () {
        return void 0;
    };
    FileSelectDirective.prototype.isEmptyAfterSelection = function () {
        return !!this.element.nativeElement.attributes.multiple;
    };
    FileSelectDirective.prototype.onChange = function () {
        // let files = this.uploader.isHTML5 ? this.element.nativeElement[0].files : this.element.nativeElement[0];
        var files = this.element.nativeElement.files;
        var options = this.getOptions();
        var filters = this.getFilters();
        // if(!this.uploader.isHTML5) this.destroy();
        this.uploader.addToQueue(files, options, filters);
        if (this.isEmptyAfterSelection()) {
            // todo
            this.element.nativeElement.value = '';
        }
    };
    return FileSelectDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", file_uploader_class_1.FileUploader)
], FileSelectDirective.prototype, "uploader", void 0);
__decorate([
    core_1.HostListener('change'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], FileSelectDirective.prototype, "onChange", null);
FileSelectDirective = __decorate([
    core_1.Directive({ selector: '[ng2FileSelect]' }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], FileSelectDirective);
exports.FileSelectDirective = FileSelectDirective;


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-type.class.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var FileType = (function () {
    function FileType() {
    }
    FileType.getMimeClass = function (file) {
        var mimeClass = 'application';
        if (this.mime_psd.indexOf(file.type) !== -1) {
            mimeClass = 'image';
        }
        else if (file.type.match('image.*')) {
            mimeClass = 'image';
        }
        else if (file.type.match('video.*')) {
            mimeClass = 'video';
        }
        else if (file.type.match('audio.*')) {
            mimeClass = 'audio';
        }
        else if (file.type === 'application/pdf') {
            mimeClass = 'pdf';
        }
        else if (this.mime_compress.indexOf(file.type) !== -1) {
            mimeClass = 'compress';
        }
        else if (this.mime_doc.indexOf(file.type) !== -1) {
            mimeClass = 'doc';
        }
        else if (this.mime_xsl.indexOf(file.type) !== -1) {
            mimeClass = 'xls';
        }
        else if (this.mime_ppt.indexOf(file.type) !== -1) {
            mimeClass = 'ppt';
        }
        if (mimeClass === 'application') {
            mimeClass = this.fileTypeDetection(file.name);
        }
        return mimeClass;
    };
    FileType.fileTypeDetection = function (inputFilename) {
        var types = {
            'jpg': 'image',
            'jpeg': 'image',
            'tif': 'image',
            'psd': 'image',
            'bmp': 'image',
            'png': 'image',
            'nef': 'image',
            'tiff': 'image',
            'cr2': 'image',
            'dwg': 'image',
            'cdr': 'image',
            'ai': 'image',
            'indd': 'image',
            'pin': 'image',
            'cdp': 'image',
            'skp': 'image',
            'stp': 'image',
            '3dm': 'image',
            'mp3': 'audio',
            'wav': 'audio',
            'wma': 'audio',
            'mod': 'audio',
            'm4a': 'audio',
            'compress': 'compress',
            'rar': 'compress',
            '7z': 'compress',
            'lz': 'compress',
            'z01': 'compress',
            'pdf': 'pdf',
            'xls': 'xls',
            'xlsx': 'xls',
            'ods': 'xls',
            'mp4': 'video',
            'avi': 'video',
            'wmv': 'video',
            'mpg': 'video',
            'mts': 'video',
            'flv': 'video',
            '3gp': 'video',
            'vob': 'video',
            'm4v': 'video',
            'mpeg': 'video',
            'm2ts': 'video',
            'mov': 'video',
            'doc': 'doc',
            'docx': 'doc',
            'eps': 'doc',
            'txt': 'doc',
            'odt': 'doc',
            'rtf': 'doc',
            'ppt': 'ppt',
            'pptx': 'ppt',
            'pps': 'ppt',
            'ppsx': 'ppt',
            'odp': 'ppt'
        };
        var chunks = inputFilename.split('.');
        if (chunks.length < 2) {
            return 'application';
        }
        var extension = chunks[chunks.length - 1].toLowerCase();
        if (types[extension] === undefined) {
            return 'application';
        }
        else {
            return types[extension];
        }
    };
    return FileType;
}());
/*  MS office  */
FileType.mime_doc = [
    'application/msword',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    'application/vnd.ms-word.document.macroEnabled.12',
    'application/vnd.ms-word.template.macroEnabled.12'
];
FileType.mime_xsl = [
    'application/vnd.ms-excel',
    'application/vnd.ms-excel',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
    'application/vnd.ms-excel.sheet.macroEnabled.12',
    'application/vnd.ms-excel.template.macroEnabled.12',
    'application/vnd.ms-excel.addin.macroEnabled.12',
    'application/vnd.ms-excel.sheet.binary.macroEnabled.12'
];
FileType.mime_ppt = [
    'application/vnd.ms-powerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.openxmlformats-officedocument.presentationml.template',
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    'application/vnd.ms-powerpoint.addin.macroEnabled.12',
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'
];
/* PSD */
FileType.mime_psd = [
    'image/photoshop',
    'image/x-photoshop',
    'image/psd',
    'application/photoshop',
    'application/psd',
    'zz-application/zz-winassoc-psd'
];
/* Compressed files */
FileType.mime_compress = [
    'application/x-gtar',
    'application/x-gcompress',
    'application/compress',
    'application/x-tar',
    'application/x-rar-compressed',
    'application/octet-stream'
];
exports.FileType = FileType;


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-upload.module.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var common_1 = __webpack_require__("../../../common/@angular/common.es5.js");
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var file_drop_directive_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-drop.directive.js");
var file_select_directive_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-select.directive.js");
var FileUploadModule = (function () {
    function FileUploadModule() {
    }
    return FileUploadModule;
}());
FileUploadModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule],
        declarations: [file_drop_directive_1.FileDropDirective, file_select_directive_1.FileSelectDirective],
        exports: [file_drop_directive_1.FileDropDirective, file_select_directive_1.FileSelectDirective]
    })
], FileUploadModule);
exports.FileUploadModule = FileUploadModule;


/***/ }),

/***/ "../../../../ng2-file-upload/file-upload/file-uploader.class.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var file_like_object_class_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-like-object.class.js");
var file_item_class_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-item.class.js");
var file_type_class_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-type.class.js");
function isFile(value) {
    return (File && value instanceof File);
}
var FileUploader = (function () {
    function FileUploader(options) {
        this.isUploading = false;
        this.queue = [];
        this.progress = 0;
        this._nextIndex = 0;
        this.options = {
            autoUpload: false,
            isHTML5: true,
            filters: [],
            removeAfterUpload: false,
            disableMultipart: false
        };
        this.setOptions(options);
    }
    FileUploader.prototype.setOptions = function (options) {
        this.options = Object.assign(this.options, options);
        this.authToken = options.authToken;
        this.authTokenHeader = options.authTokenHeader || 'Authorization';
        this.autoUpload = options.autoUpload;
        this.options.filters.unshift({ name: 'queueLimit', fn: this._queueLimitFilter });
        if (this.options.maxFileSize) {
            this.options.filters.unshift({ name: 'fileSize', fn: this._fileSizeFilter });
        }
        if (this.options.allowedFileType) {
            this.options.filters.unshift({ name: 'fileType', fn: this._fileTypeFilter });
        }
        if (this.options.allowedMimeType) {
            this.options.filters.unshift({ name: 'mimeType', fn: this._mimeTypeFilter });
        }
        for (var i = 0; i < this.queue.length; i++) {
            this.queue[i].url = this.options.url;
        }
        // this.options.filters.unshift({name: 'folder', fn: this._folderFilter});
    };
    FileUploader.prototype.addToQueue = function (files, options, filters) {
        var _this = this;
        var list = [];
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            list.push(file);
        }
        var arrayOfFilters = this._getFilters(filters);
        var count = this.queue.length;
        var addedFileItems = [];
        list.map(function (some) {
            if (!options) {
                options = _this.options;
            }
            var temp = new file_like_object_class_1.FileLikeObject(some);
            if (_this._isValidFile(temp, arrayOfFilters, options)) {
                var fileItem = new file_item_class_1.FileItem(_this, some, options);
                addedFileItems.push(fileItem);
                _this.queue.push(fileItem);
                _this._onAfterAddingFile(fileItem);
            }
            else {
                var filter = arrayOfFilters[_this._failFilterIndex];
                _this._onWhenAddingFileFailed(temp, filter, options);
            }
        });
        if (this.queue.length !== count) {
            this._onAfterAddingAll(addedFileItems);
            this.progress = this._getTotalProgress();
        }
        this._render();
        if (this.options.autoUpload) {
            this.uploadAll();
        }
    };
    FileUploader.prototype.removeFromQueue = function (value) {
        var index = this.getIndexOfItem(value);
        var item = this.queue[index];
        if (item.isUploading) {
            item.cancel();
        }
        this.queue.splice(index, 1);
        this.progress = this._getTotalProgress();
    };
    FileUploader.prototype.clearQueue = function () {
        while (this.queue.length) {
            this.queue[0].remove();
        }
        this.progress = 0;
    };
    FileUploader.prototype.uploadItem = function (value) {
        var index = this.getIndexOfItem(value);
        var item = this.queue[index];
        var transport = this.options.isHTML5 ? '_xhrTransport' : '_iframeTransport';
        item._prepareToUploading();
        if (this.isUploading) {
            return;
        }
        this.isUploading = true;
        this[transport](item);
    };
    FileUploader.prototype.cancelItem = function (value) {
        var index = this.getIndexOfItem(value);
        var item = this.queue[index];
        var prop = this.options.isHTML5 ? item._xhr : item._form;
        if (item && item.isUploading) {
            prop.abort();
        }
    };
    FileUploader.prototype.uploadAll = function () {
        var items = this.getNotUploadedItems().filter(function (item) { return !item.isUploading; });
        if (!items.length) {
            return;
        }
        items.map(function (item) { return item._prepareToUploading(); });
        items[0].upload();
    };
    FileUploader.prototype.cancelAll = function () {
        var items = this.getNotUploadedItems();
        items.map(function (item) { return item.cancel(); });
    };
    FileUploader.prototype.isFile = function (value) {
        return isFile(value);
    };
    FileUploader.prototype.isFileLikeObject = function (value) {
        return value instanceof file_like_object_class_1.FileLikeObject;
    };
    FileUploader.prototype.getIndexOfItem = function (value) {
        return typeof value === 'number' ? value : this.queue.indexOf(value);
    };
    FileUploader.prototype.getNotUploadedItems = function () {
        return this.queue.filter(function (item) { return !item.isUploaded; });
    };
    FileUploader.prototype.getReadyItems = function () {
        return this.queue
            .filter(function (item) { return (item.isReady && !item.isUploading); })
            .sort(function (item1, item2) { return item1.index - item2.index; });
    };
    FileUploader.prototype.destroy = function () {
        return void 0;
        /*forEach(this._directives, (key) => {
         forEach(this._directives[key], (object) => {
         object.destroy();
         });
         });*/
    };
    FileUploader.prototype.onAfterAddingAll = function (fileItems) {
        return { fileItems: fileItems };
    };
    FileUploader.prototype.onBuildItemForm = function (fileItem, form) {
        return { fileItem: fileItem, form: form };
    };
    FileUploader.prototype.onAfterAddingFile = function (fileItem) {
        return { fileItem: fileItem };
    };
    FileUploader.prototype.onWhenAddingFileFailed = function (item, filter, options) {
        return { item: item, filter: filter, options: options };
    };
    FileUploader.prototype.onBeforeUploadItem = function (fileItem) {
        return { fileItem: fileItem };
    };
    FileUploader.prototype.onProgressItem = function (fileItem, progress) {
        return { fileItem: fileItem, progress: progress };
    };
    FileUploader.prototype.onProgressAll = function (progress) {
        return { progress: progress };
    };
    FileUploader.prototype.onSuccessItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onErrorItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onCancelItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onCompleteItem = function (item, response, status, headers) {
        return { item: item, response: response, status: status, headers: headers };
    };
    FileUploader.prototype.onCompleteAll = function () {
        return void 0;
    };
    FileUploader.prototype._mimeTypeFilter = function (item) {
        return !(this.options.allowedMimeType && this.options.allowedMimeType.indexOf(item.type) === -1);
    };
    FileUploader.prototype._fileSizeFilter = function (item) {
        return !(this.options.maxFileSize && item.size > this.options.maxFileSize);
    };
    FileUploader.prototype._fileTypeFilter = function (item) {
        return !(this.options.allowedFileType &&
            this.options.allowedFileType.indexOf(file_type_class_1.FileType.getMimeClass(item)) === -1);
    };
    FileUploader.prototype._onErrorItem = function (item, response, status, headers) {
        item._onError(response, status, headers);
        this.onErrorItem(item, response, status, headers);
    };
    FileUploader.prototype._onCompleteItem = function (item, response, status, headers) {
        item._onComplete(response, status, headers);
        this.onCompleteItem(item, response, status, headers);
        var nextItem = this.getReadyItems()[0];
        this.isUploading = false;
        if (nextItem) {
            nextItem.upload();
            return;
        }
        this.onCompleteAll();
        this.progress = this._getTotalProgress();
        this._render();
    };
    FileUploader.prototype._headersGetter = function (parsedHeaders) {
        return function (name) {
            if (name) {
                return parsedHeaders[name.toLowerCase()] || void 0;
            }
            return parsedHeaders;
        };
    };
    FileUploader.prototype._xhrTransport = function (item) {
        var _this = this;
        var xhr = item._xhr = new XMLHttpRequest();
        var sendable;
        this._onBeforeUploadItem(item);
        // todo
        /*item.formData.map(obj => {
         obj.map((value, key) => {
         form.append(key, value);
         });
         });*/
        if (typeof item._file.size !== 'number') {
            throw new TypeError('The file specified is no longer valid');
        }
        if (!this.options.disableMultipart) {
            sendable = new FormData();
            this._onBuildItemForm(item, sendable);
            sendable.append(item.alias, item._file, item.file.name);
            if (this.options.additionalParameter !== undefined) {
                Object.keys(this.options.additionalParameter).forEach(function (key) {
                    sendable.append(key, _this.options.additionalParameter[key]);
                });
            }
        }
        else {
            sendable = item._file;
        }
        xhr.upload.onprogress = function (event) {
            var progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
            _this._onProgressItem(item, progress);
        };
        xhr.onload = function () {
            var headers = _this._parseHeaders(xhr.getAllResponseHeaders());
            var response = _this._transformResponse(xhr.response, headers);
            var gist = _this._isSuccessCode(xhr.status) ? 'Success' : 'Error';
            var method = '_on' + gist + 'Item';
            _this[method](item, response, xhr.status, headers);
            _this._onCompleteItem(item, response, xhr.status, headers);
        };
        xhr.onerror = function () {
            var headers = _this._parseHeaders(xhr.getAllResponseHeaders());
            var response = _this._transformResponse(xhr.response, headers);
            _this._onErrorItem(item, response, xhr.status, headers);
            _this._onCompleteItem(item, response, xhr.status, headers);
        };
        xhr.onabort = function () {
            var headers = _this._parseHeaders(xhr.getAllResponseHeaders());
            var response = _this._transformResponse(xhr.response, headers);
            _this._onCancelItem(item, response, xhr.status, headers);
            _this._onCompleteItem(item, response, xhr.status, headers);
        };
        xhr.open(item.method, item.url, true);
        xhr.withCredentials = item.withCredentials;
        if (this.options.headers) {
            for (var _i = 0, _a = this.options.headers; _i < _a.length; _i++) {
                var header = _a[_i];
                xhr.setRequestHeader(header.name, header.value);
            }
        }
        if (item.headers.length) {
            for (var _b = 0, _c = item.headers; _b < _c.length; _b++) {
                var header = _c[_b];
                xhr.setRequestHeader(header.name, header.value);
            }
        }
        if (this.authToken) {
            xhr.setRequestHeader(this.authTokenHeader, this.authToken);
        }
        xhr.send(sendable);
        this._render();
    };
    FileUploader.prototype._getTotalProgress = function (value) {
        if (value === void 0) { value = 0; }
        if (this.options.removeAfterUpload) {
            return value;
        }
        var notUploaded = this.getNotUploadedItems().length;
        var uploaded = notUploaded ? this.queue.length - notUploaded : this.queue.length;
        var ratio = 100 / this.queue.length;
        var current = value * ratio / 100;
        return Math.round(uploaded * ratio + current);
    };
    FileUploader.prototype._getFilters = function (filters) {
        if (!filters) {
            return this.options.filters;
        }
        if (Array.isArray(filters)) {
            return filters;
        }
        if (typeof filters === 'string') {
            var names_1 = filters.match(/[^\s,]+/g);
            return this.options.filters
                .filter(function (filter) { return names_1.indexOf(filter.name) !== -1; });
        }
        return this.options.filters;
    };
    FileUploader.prototype._render = function () {
        return void 0;
        // todo: ?
    };
    // protected _folderFilter(item:FileItem):boolean {
    //   return !!(item.size || item.type);
    // }
    FileUploader.prototype._queueLimitFilter = function () {
        return this.options.queueLimit === undefined || this.queue.length < this.options.queueLimit;
    };
    FileUploader.prototype._isValidFile = function (file, filters, options) {
        var _this = this;
        this._failFilterIndex = -1;
        return !filters.length ? true : filters.every(function (filter) {
            _this._failFilterIndex++;
            return filter.fn.call(_this, file, options);
        });
    };
    FileUploader.prototype._isSuccessCode = function (status) {
        return (status >= 200 && status < 300) || status === 304;
    };
    /* tslint:disable */
    FileUploader.prototype._transformResponse = function (response, headers) {
        // todo: ?
        /*var headersGetter = this._headersGetter(headers);
         forEach($http.defaults.transformResponse, (transformFn) => {
         response = transformFn(response, headersGetter);
         });*/
        return response;
    };
    /* tslint:enable */
    FileUploader.prototype._parseHeaders = function (headers) {
        var parsed = {};
        var key;
        var val;
        var i;
        if (!headers) {
            return parsed;
        }
        headers.split('\n').map(function (line) {
            i = line.indexOf(':');
            key = line.slice(0, i).trim().toLowerCase();
            val = line.slice(i + 1).trim();
            if (key) {
                parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
            }
        });
        return parsed;
    };
    /*protected _iframeTransport(item:FileItem) {
     // todo: implement it later
     }*/
    FileUploader.prototype._onWhenAddingFileFailed = function (item, filter, options) {
        this.onWhenAddingFileFailed(item, filter, options);
    };
    FileUploader.prototype._onAfterAddingFile = function (item) {
        this.onAfterAddingFile(item);
    };
    FileUploader.prototype._onAfterAddingAll = function (items) {
        this.onAfterAddingAll(items);
    };
    FileUploader.prototype._onBeforeUploadItem = function (item) {
        item._onBeforeUpload();
        this.onBeforeUploadItem(item);
    };
    FileUploader.prototype._onBuildItemForm = function (item, form) {
        item._onBuildForm(form);
        this.onBuildItemForm(item, form);
    };
    FileUploader.prototype._onProgressItem = function (item, progress) {
        var total = this._getTotalProgress(progress);
        this.progress = total;
        item._onProgress(progress);
        this.onProgressItem(item, progress);
        this.onProgressAll(total);
        this._render();
    };
    /* tslint:disable */
    FileUploader.prototype._onSuccessItem = function (item, response, status, headers) {
        item._onSuccess(response, status, headers);
        this.onSuccessItem(item, response, status, headers);
    };
    /* tslint:enable */
    FileUploader.prototype._onCancelItem = function (item, response, status, headers) {
        item._onCancel(response, status, headers);
        this.onCancelItem(item, response, status, headers);
    };
    return FileUploader;
}());
exports.FileUploader = FileUploader;


/***/ }),

/***/ "../../../../ng2-file-upload/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(__webpack_require__("../../../../ng2-file-upload/file-upload/file-select.directive.js"));
__export(__webpack_require__("../../../../ng2-file-upload/file-upload/file-drop.directive.js"));
__export(__webpack_require__("../../../../ng2-file-upload/file-upload/file-uploader.class.js"));
__export(__webpack_require__("../../../../ng2-file-upload/file-upload/file-item.class.js"));
__export(__webpack_require__("../../../../ng2-file-upload/file-upload/file-like-object.class.js"));
var file_upload_module_1 = __webpack_require__("../../../../ng2-file-upload/file-upload/file-upload.module.js");
exports.FileUploadModule = file_upload_module_1.FileUploadModule;


/***/ }),

/***/ "../../../../webpack/buildin/amd-options.js":
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ })

});
//# sourceMappingURL=0.chunk.js.map