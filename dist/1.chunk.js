webpackJsonp([1],{

/***/ "../../../../../src/app/+accessTable/accessTable.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__healthcheck_base__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-base/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__healthcheck_list__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__healthcheck_details__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-details/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__accessTable_routing_module__ = __webpack_require__("../../../../../src/app/+accessTable/accessTable.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pager__ = __webpack_require__("../../../../../src/app/+accessTable/pager/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccessTableModule", function() { return AccessTableModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var HEALTHCHECK_DECLARATIONS = [
    __WEBPACK_IMPORTED_MODULE_2__healthcheck_base__["a" /* HealthcheckBaseComponent */]
].concat(__WEBPACK_IMPORTED_MODULE_3__healthcheck_list__["a" /* HEALTHCHECK_LIST_COMPONENTS */], __WEBPACK_IMPORTED_MODULE_4__healthcheck_details__["a" /* HEALTHCHECK_DETAILS_COMPONENTS */]);



var AccessTableModule = (function () {
    function AccessTableModule() {
    }
    return AccessTableModule;
}());
AccessTableModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: HEALTHCHECK_DECLARATIONS.slice(),
        imports: [
            __WEBPACK_IMPORTED_MODULE_5__shared__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_6__accessTable_routing_module__["a" /* AccessTableRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap__["a" /* PaginationModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_7__pager__["a" /* PagerModule */],
            __WEBPACK_IMPORTED_MODULE_5__shared__["b" /* PageLayoutModule */]
        ]
    })
], AccessTableModule);

//# sourceMappingURL=accessTable.module.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/accessTable.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__healthcheck_base__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-base/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__healthcheck_list__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__healthcheck_details__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-details/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccessTableRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__healthcheck_base__["a" /* HealthcheckBaseComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_5__core__["f" /* IsAuthenticatedGuard */]],
        children: [
            {
                path: '',
                component: __WEBPACK_IMPORTED_MODULE_1__healthcheck_list__["b" /* HealthcheckListComponent */]
            },
            {
                path: 'details',
                component: __WEBPACK_IMPORTED_MODULE_2__healthcheck_details__["b" /* HealthcheckDetailsComponent */]
            }
        ]
    },
];
var AccessTableRoutingModule = (function () {
    function AccessTableRoutingModule() {
    }
    return AccessTableRoutingModule;
}());
AccessTableRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forChild(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */]],
    })
], AccessTableRoutingModule);

//# sourceMappingURL=accessTable.routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-base/healthcheck-base.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthcheckBaseComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var HealthcheckBaseComponent = (function () {
    function HealthcheckBaseComponent() {
    }
    return HealthcheckBaseComponent;
}());
HealthcheckBaseComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        template: "\n    \n    <router-outlet></router-outlet>\n\n    "
    })
], HealthcheckBaseComponent);

//# sourceMappingURL=healthcheck-base.component.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-base/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__healthcheck_base_component__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-base/healthcheck-base.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__healthcheck_base_component__["a"]; });

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-details/healthcheck-details.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<loc-page flexy [bottom]='50' *locAuthOnly>\r\n    <loc-page-header>\r\n        <span class=\"__breadcrumbs\">\r\n            <a routerLink='/security'>Access table</a> / details</span>\r\n    </loc-page-header>\r\n    <loc-page-menu [active]='false'>\r\n        <div class=\"left\"></div>      \r\n    </loc-page-menu>\r\n\r\n    <loc-page-body \r\n        [loading]=\"loading\"\r\n        [delay]=\"500\"\r\n        [overlay]=\"true\"\r\n    >\r\n        <div #ruleFormContainer class=\"__details\">            \r\n            <form [formGroup]='accessRuleForm' class='__details__form' role='form'>\r\n                <div class=\"row\">\r\n                    <div class=\"form-group col-xs-8\">\r\n                        <loc-input-text label='Rule name' formControlName=\"name\" [(ngModel)]='accessRule.name' id='name' required>\r\n                        </loc-input-text>\r\n                        <show-error *ngIf=\"accessRuleForm.dirty\" [control]=\"accessRuleForm.get('name')\" [options]=\"{'required': 'Field is required'}\">\r\n                        </show-error>\r\n                    </div>\r\n                    <div class=\"form-group col-xs-8\">\r\n                        <loc-input-text label='Expression' formControlName=\"expression\" [(ngModel)]='accessRule.expression' id='expression' required>\r\n                        </loc-input-text>\r\n                        <show-error *ngIf=\"accessRuleForm.dirty\" [control]=\"accessRuleForm.get('expression')\" [options]=\"{'required': 'Field is required'}\">\r\n                        </show-error>\r\n                    </div>\r\n                    <div class=\"form-group col-xs-4\">\r\n                        <loc-input-text label=\"Required permissions\" formControlName=\"permissions\" [(ngModel)]='accessRule.permissions' id='permissions'\r\n                            type=\"string\" required>\r\n                        </loc-input-text>\r\n                        <show-error *ngIf=\"accessRuleForm.dirty\" [control]=\"accessRuleForm.get('permissions')\" [options]=\"{'required': 'Field is required'}\">\r\n                        </show-error>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n            <button class=\"button button--primary __save-btn\" (click)=\"onSave($event)\"> Save</button>\r\n        </div>     \r\n    </loc-page-body>\r\n</loc-page>"

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-details/healthcheck-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_models__ = __webpack_require__("../../../../../src/app/core/models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_actions__ = __webpack_require__("../../../../../src/app/core/actions/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthcheckDetailsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HealthcheckDetailsComponent = (function () {
    function HealthcheckDetailsComponent(_store, _masterActions, route, router, _validationActions, _accessTableApiService, _fb) {
        this._store = _store;
        this._masterActions = _masterActions;
        this.route = route;
        this.router = router;
        this._validationActions = _validationActions;
        this._accessTableApiService = _accessTableApiService;
        this._fb = _fb;
        this._accessRule = {};
        this.loading = false;
        this.accessRuleForm = this._fb.group({
            permissions: [],
            expression: [],
            name: []
        });
    }
    Object.defineProperty(HealthcheckDetailsComponent.prototype, "accessRule", {
        get: function () {
            return this._accessRule;
        },
        set: function (v) {
            if (v)
                this._accessRule = v;
        },
        enumerable: true,
        configurable: true
    });
    HealthcheckDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this
            .route
            .paramMap
            .do(function () { return _this.loading = true; })
            .switchMap(function (params) {
            _this.ID = params.get('id');
            if (!_this.ID) {
                return __WEBPACK_IMPORTED_MODULE_4_rxjs__["Observable"].of(new __WEBPACK_IMPORTED_MODULE_3__core_models__["b" /* AccessRule */]());
            }
            return _this._accessTableApiService.findById(_this.ID);
        })
            .subscribe(function (result) {
            _this.accessRule = result;
            _this.loading = false;
        }, function (err) {
            _this.loading = false;
            console.error(err);
        });
    };
    HealthcheckDetailsComponent.prototype.onSave = function (event) {
        var _this = this;
        var hc = this.accessRuleForm.value;
        var sb;
        if (!this.ID) {
            sb = this._accessTableApiService
                .create(hc);
        }
        else {
            sb = this._accessTableApiService
                .update(this.ID, hc);
        }
        sb
            .subscribe(function (result) {
            _this.router.navigate(["/security"]);
        }, function (err) {
            console.log(err);
        });
    };
    return HealthcheckDetailsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], HealthcheckDetailsComponent.prototype, "accessRule", null);
HealthcheckDetailsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'healthcheck-details',
        template: __webpack_require__("../../../../../src/app/+accessTable/healthcheck-details/healthcheck-details.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+accessTable/healthcheck-details/healthcheck-details.scss")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["b" /* Store */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_7__core_actions__["f" /* MasterActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_actions__["f" /* MasterActions */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__core_actions__["h" /* ValidationActions */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__core_actions__["h" /* ValidationActions */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__core__["g" /* AccessTableApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__core__["g" /* AccessTableApi */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* FormBuilder */]) === "function" && _g || Object])
], HealthcheckDetailsComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=healthcheck-details.component.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-details/healthcheck-details.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".__details {\n  padding: 0.625rem;\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n  .__details__form {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    padding: .625rem; }\n\n.__breadcrumbs {\n  font-style: italic; }\n  .__breadcrumbs > a {\n    color: inherit;\n    font-style: normal; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-details/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__healthcheck_details_component__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-details/healthcheck-details.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__healthcheck_details_component__["a"]; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HEALTHCHECK_DETAILS_COMPONENTS; });


var HEALTHCHECK_DETAILS_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__healthcheck_details_component__["a" /* HealthcheckDetailsComponent */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list-item.component.tpml.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"__rule__body\">\n    <div class=\"__rule__title\">\n        <span>\n            <a [routerLink]='[\"details\", { id: item._id }]'>{{item?.name}}</a>\n        </span>\n    </div>\n    <div class=\"__rule__buttons-block\">\n        <button class=\"button button--icon __rule__trash-button\" (click)=\"onDelete.emit(item._id)\">\n            <i class=\"icon-delete-garbage-streamline\"></i>\n        </button>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthcheckListItemComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HealthcheckListItemComponent = (function () {
    function HealthcheckListItemComponent(_healthcheckApi, _store) {
        this._healthcheckApi = _healthcheckApi;
        this._store = _store;
        this.onDelete = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    HealthcheckListItemComponent.prototype.ngOnInit = function () {
    };
    HealthcheckListItemComponent.prototype.ngOnDestroy = function () {
    };
    return HealthcheckListItemComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__core__["h" /* HealthCheck */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core__["h" /* HealthCheck */]) === "function" && _a || Object)
], HealthcheckListItemComponent.prototype, "item", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], HealthcheckListItemComponent.prototype, "onDelete", void 0);
HealthcheckListItemComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'healthcheck-list-item',
        template: __webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list-item.component.tpml.html"),
        styles: [__webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list-item.scss")],
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('initial', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('start', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    visibility: 'hidden',
                    opacity: 0
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('complete', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    visibility: 'visible',
                    opacity: 1
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('start => complete', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('300ms linear')
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__core__["i" /* HealthcheckApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__core__["i" /* HealthcheckApi */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["b" /* Store */]) === "function" && _d || Object])
], HealthcheckListItemComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=healthcheck-list-item.component.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list-item.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host {\n  width: 100%;\n  display: block; }\n\n.__rule__body {\n  position: relative;\n  padding: 10px 30px 10px 10px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background: white;\n  transition: all 0.2s ease; }\n  .__rule__body:hover {\n    background: #eee; }\n\n.__rule__title {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1; }\n\n.__rule__buttons-block {\n  -webkit-box-flex: 0;\n      -ms-flex: 0 1 auto;\n          flex: 0 1 auto;\n  margin: 0 auto; }\n\n.__rule__trash-button {\n  font-size: 18px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list.component.tmpl.html":
/***/ (function(module, exports) {

module.exports = "<loc-page flexy [bottom]='50' *locAuthOnly>\r\n    <loc-page-header title=\"Access table\">\r\n        <span> Access table </span>\r\n    </loc-page-header>\r\n    <loc-page-menu>\r\n        <div class=\"left\"></div>\r\n        <div class=\"submenu\">\r\n            <button title=\"Add new\" class='button button--borderless __add-button' (click)='onCreateNewHealthCheck($event)'>\r\n                <i class=\"icon-android-add-circle\" aria-hidden=\"true\"></i> Add New\r\n            </button>\r\n        </div>\r\n    </loc-page-menu>\r\n\r\n    <loc-page-body \r\n        [loading]=\"loading\"\r\n        [delay]=\"300\" \r\n        [overlay]=\"true\"\r\n    >\r\n        <div class=\"m__table l-access__table\">\r\n            <div *ngFor='let hc of healthChecks; let c = count;' class='m__table-row l-access__table-row'>\r\n                <healthcheck-list-item #details [item]='hc' (onDelete)=\"removeItem($event)\">\r\n                </healthcheck-list-item>\r\n            </div>\r\n        </div>\r\n    </loc-page-body>\r\n\r\n    <!-- <eg-pagination \r\n        [hotStreamFactory]=\"getStreamFactory()\" \r\n        [(page)]=\"page\" \r\n        [limit]=\"limit\" \r\n        [auto]=\"true\" \r\n        (loading)=\"dataIsLoading = $event\"\r\n        [desableOnLoad]=\"true\"\r\n    >\r\n    </eg-pagination>\r\n    <div *ngIf=\"dataIsLoading\">\r\n        <span>LOADING</span>\r\n    </div>\r\n    <div> Currently loaded page number:\r\n        <span> {{ page }} page</span>\r\n    </div>\r\n    <div *ngFor='let hc of loadedData$ | async'>\r\n        {{hc.name}}\r\n    </div> -->\r\n</loc-page>"

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_components__ = __webpack_require__("../../../../../src/app/shared/components/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core__ = __webpack_require__("../../../../../src/app/core/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngrx_store__ = __webpack_require__("../../../../@ngrx/store/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_BehaviorSubject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HealthcheckListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HealthcheckListComponent = (function () {
    function HealthcheckListComponent(router, route, _serviceApi, _authService, _ngZone, modalService, _healthcheckApi, _store) {
        this.router = router;
        this.route = route;
        this._serviceApi = _serviceApi;
        this._authService = _authService;
        this._ngZone = _ngZone;
        this.modalService = modalService;
        this._healthcheckApi = _healthcheckApi;
        this._store = _store;
        /////////////////////////////
        this.configs = [];
        this.loading = false;
        this.sidebarActive = false;
        this.serviceStatusArray = [];
        this.initialComplete = false;
        this.state = 'start';
        this.searchParams = {};
        this.maxItems = 10;
        this.showMenu = false;
        this.searchControl = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* FormControl */]();
        this.importData = { routes: [] };
        this.loadedData$ = new __WEBPACK_IMPORTED_MODULE_8_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        /////////////////////////////////////////////////////
        this.page = 0;
        this.limit = 10;
        var header = { 'name': "Authorization", 'value': _authService.user ? _authService.user.accessToken : '' };
    }
    HealthcheckListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading = true;
        this._healthcheckApi
            .find()
            .subscribe(function (result) {
            _this.healthChecks = result;
            _this.loading = false;
        }, function (err) {
            _this.loading = false;
            console.log(err);
        });
    };
    HealthcheckListComponent.prototype.getStreamFactory = function () {
        return this.queryData.bind(this);
    };
    HealthcheckListComponent.prototype.queryData = function (query) {
        var _this = this;
        // todo: assign search query to paging request
        return this._healthcheckApi
            .find(query)
            .zip(this.loadedData$, function (loaded, current) { return current.concat(loaded); })
            .do(function (result) { return _this.loadedData$.next(result); });
    };
    //////////////////////////////////////////////////////
    // ngAfterViewInit() {
    //     if (this.locPager)
    //         this.locPager
    //             .pageChanged
    //             .distinctUntilChanged()
    //             .do(() => this.loading = true)
    //             .map((value) => { return { page: value.page } })
    //             .subscribe((q: IQuery) => {
    //                 this.loading = false;
    //             });
    // }
    HealthcheckListComponent.prototype.ngOnDestroy = function () {
        if (this.updateIntervalHandler) {
            clearInterval(this.updateIntervalHandler);
        }
    };
    HealthcheckListComponent.prototype.removeItem = function (id) {
        var _this = this;
        this._healthcheckApi
            .deleteById(id)
            .subscribe(function (res) {
            var ind = _this.healthChecks.findIndex(function (el) { return el._id == id; });
            _this.healthChecks.splice(ind, 1);
        }, function (err) {
            console.log(err);
        });
    };
    HealthcheckListComponent.prototype.onCreateNewHealthCheck = function (event) {
        this.router.navigate(["details", {}], { queryParams: {}, relativeTo: this.route });
    };
    return HealthcheckListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('searchInput'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], HealthcheckListComponent.prototype, "searchInput", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap__["e" /* PagerComponent */]),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap__["e" /* PagerComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_ngx_bootstrap__["e" /* PagerComponent */]) === "function" && _b || Object)
], HealthcheckListComponent.prototype, "pager", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__shared_components__["c" /* LocPagerComponent */]),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__shared_components__["c" /* LocPagerComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_components__["c" /* LocPagerComponent */]) === "function" && _c || Object)
], HealthcheckListComponent.prototype, "locPager", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('importPreview'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["TemplateRef"]) === "function" && _d || Object)
], HealthcheckListComponent.prototype, "content", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('uploadEl'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _e || Object)
], HealthcheckListComponent.prototype, "uploadElRef", void 0);
HealthcheckListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'healthcheck-list',
        template: __webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list.component.tmpl.html"),
        styles: [__webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list.scss")],
        animations: [
            __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["trigger"])('initial', [
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('start', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    visibility: 'hidden',
                    opacity: 0
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["state"])('complete', __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["style"])({
                    visibility: 'visible',
                    opacity: 1
                })),
                __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["transition"])('start => complete', [
                    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["animate"])('300ms linear')
                ])
            ])
        ]
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* ActivatedRoute */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_3__core__["j" /* ServiceApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core__["j" /* ServiceApi */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_3__core__["e" /* LoopBackAuth */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core__["e" /* LoopBackAuth */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_3__core__["g" /* AccessTableApi */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__core__["g" /* AccessTableApi */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_7__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__ngrx_store__["b" /* Store */]) === "function" && _o || Object])
], HealthcheckListComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
//# sourceMappingURL=healthcheck-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".l-access__table {\n  padding: 0;\n  margin: 0; }\n\n.l-access__table-row {\n  padding: 0; }\n\n.__add-button {\n  width: 100px;\n  height: 30px;\n  font-size: .9rem;\n  padding: 5px; }\n  .__add-button i {\n    -webkit-transform: translateY(2px);\n            transform: translateY(2px);\n    display: inline-block; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+accessTable/healthcheck-list/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__healthcheck_list_component__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__healthcheck_list_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__healthcheck_list_item_component__ = __webpack_require__("../../../../../src/app/+accessTable/healthcheck-list/healthcheck-list-item.component.ts");
/* unused harmony namespace reexport */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HEALTHCHECK_LIST_COMPONENTS; });




var HEALTHCHECK_LIST_COMPONENTS = [
    __WEBPACK_IMPORTED_MODULE_0__healthcheck_list_component__["a" /* HealthcheckListComponent */],
    __WEBPACK_IMPORTED_MODULE_1__healthcheck_list_item_component__["a" /* HealthcheckListItemComponent */]
];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/pager/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pager_component__ = __webpack_require__("../../../../../src/app/+accessTable/pager/pager.component.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pager_module__ = __webpack_require__("../../../../../src/app/+accessTable/pager/pager.module.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__pager_module__["a"]; });


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/pager/pager.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"border: 2px solid red\">\n    <button (click)=\"next()\" [disabled]=\"desableOnLoad && processing\">\n        <span>Load next({{ page + 1 }}) page</span>      \n    </button>\n    <br>\n    <div class=\"debug-block\">\n        <div>\n            <span>Currently loaded page number: </span>\n            <span>{{  page  }} </span>\n        </div>\n        <div *ngIf=\"processing\">\n            <span>Currently loading with query: </span>\n            <span>{{ '{ page:' + (page + 1) + ', ' + 'limit: ' + limit + '}' }}</span>         \n        </div>       \n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/+accessTable/pager/pager.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/+accessTable/pager/pager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EGPaginationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EGPaginationComponent = (function () {
    function EGPaginationComponent() {
        /**
         * Property expects factory **function** as input, which defines remote data flow, bound to the caller (parent) context
         */
        this.hotStreamFactory = function () { return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].of([]); };
        /**
         * Number of items to be fetched per iteration
         */
        this.limit = 10;
        /**
         * Start from page, first page by default
         */
        this.page = 1;
        /**
         * Should pager run data stream on load
         */
        this.auto = false;
        /**
         * Should pager disable control button(s) when untill data flkow is not completed
         */
        this.desableOnLoad = true;
        /**
         * Notify subscribers on page **successfull** change
         */
        this.pageChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        /**
         * Notify external subscribers on data flow running
         */
        this.loading = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.pager$ = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["Subject"]();
        this.processing = false;
    }
    /**
     * Set component loading state depending on data flow
     * @param {boolean} loading
     */
    EGPaginationComponent.prototype.setLoadingState = function (loading) {
        if (loading === void 0) { loading = false; }
        this.processing = loading;
        this.loading.next(loading);
    };
    /**
     * Notify on data flow completed event and change pager state if successull
     * @param { boolean } successfull data flow completed successfully
     */
    EGPaginationComponent.prototype.notifyCompleted = function (successfull) {
        if (successfull === void 0) { successfull = false; }
        // increment page value only if success, remain previous state otherwise
        if (successfull) {
            this.page = this.page + 1;
            this.pageChange.next(this.page);
        }
    };
    EGPaginationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscr = this.pager$
            .map(function () { return ({
            page: _this.page + 1,
            limit: _this.limit
        }); })
            .do(function () { return _this.setLoadingState(true); })
            .switchMap(function (query) { return _this.hotStreamFactory(query); })
            .do(function () { return _this.setLoadingState(false); })
            .subscribe(function () { return _this.notifyCompleted(true); }, function () { return _this.notifyCompleted(false); });
        // launch state change on load
        if (this.auto) {
            this.next();
        }
    };
    EGPaginationComponent.prototype.ngOnDestroy = function () {
        this.subscr && this.subscr.unsubscribe();
    };
    EGPaginationComponent.prototype.next = function () {
        this.pager$.next();
    };
    return EGPaginationComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Function)
], EGPaginationComponent.prototype, "hotStreamFactory", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], EGPaginationComponent.prototype, "limit", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], EGPaginationComponent.prototype, "page", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], EGPaginationComponent.prototype, "auto", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean)
], EGPaginationComponent.prototype, "desableOnLoad", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _a || Object)
], EGPaginationComponent.prototype, "pageChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]) === "function" && _b || Object)
], EGPaginationComponent.prototype, "loading", void 0);
EGPaginationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'eg-pagination',
        template: __webpack_require__("../../../../../src/app/+accessTable/pager/pager.component.html"),
        styles: [__webpack_require__("../../../../../src/app/+accessTable/pager/pager.component.scss")],
    })
], EGPaginationComponent);

var _a, _b;
//# sourceMappingURL=pager.component.js.map

/***/ }),

/***/ "../../../../../src/app/+accessTable/pager/pager.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pager_component__ = __webpack_require__("../../../../../src/app/+accessTable/pager/pager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagerModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//import { PagerService } from './pager.service';

var PagerModule = (function () {
    function PagerModule() {
    }
    return PagerModule;
}());
PagerModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_1__pager_component__["a" /* EGPaginationComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
        ],
        providers: [],
        exports: [
            __WEBPACK_IMPORTED_MODULE_1__pager_component__["a" /* EGPaginationComponent */]
        ]
    })
], PagerModule);

//# sourceMappingURL=pager.module.js.map

/***/ })

});
//# sourceMappingURL=1.chunk.js.map