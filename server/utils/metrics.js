"use strict";
const R = require('ramda')
    ;

module.exports = function (clusterStore) {

    let __produceInitMetrics = () => {
        return {
            load: [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ],
            average: 0,
            statuses: {}
        };
    };

    function __constructMetrics(stats) {
        let load = stats.load || [];
        let actualLoad = R.filter((s) => Date.now() - s.stop <= 60000)(load);
        let agg = [];

        let now = Date.now();
        for (let i = 60; i >= 0; i--) {
            let end = i * 1000;
            let pEnd = end - 1000 > 0 ? end - 1000 : 0;
            let ti = R.find((l) => now - l.stop <= end && now - l.start >= pEnd)(actualLoad);
            agg.push(ti ? ti.count : 0);
        }

        let tts = 0;
        actualLoad.forEach((l) => {
            tts = tts + l.tts;
        });

        let statuses = R.reduce((acc, elem) => {
            let statusesObj = elem.statuses;
            R.forEach((statusCode) => {
                acc[statusCode] = acc[statusCode] !== undefined ? acc[statusCode] + statusesObj[statusCode] : 0;
            })(R.keys(statusesObj));
            return acc;
        }, {})(actualLoad);

        return {
            load: agg,
            statuses: statuses,
            average: Math.floor(actualLoad.length > 0 ? tts / actualLoad.length : 0),
            id: stats.id
        };
    }

    const __gainMetrics = (id) => {
        return clusterStore
            .keys('node:*')
            .then((nodes) => {
                if (nodes && nodes.length > 0) {
                    let statsKeys = nodes.map((n) => `stats:${n.split(':')[1]}`);

                    return clusterStore.mget(statsKeys).then((clusterStats) => {
                        if (clusterStats && Array.isArray(clusterStats)) {
                            let statsSource = clusterStats.map((stats) => stats
                                ? __constructMetrics(JSON.parse(stats))
                                : __produceInitMetrics());

                            let stats = R.reduce((agg, el) => {
                                if (el.id === id) {
                                    for (var i = 0; i < agg.load.length; i++) {
                                        agg.load[i] += el.load[i] || 0;
                                    }
                                    Object.keys(el.statuses)
                                        .forEach((key) => {
                                            agg.statuses[key] = agg.statuses[key]
                                                ? agg.statuses[key] + el.statuses[key]
                                                : el.statuses[key];
                                        });
                                    agg.average = agg.average + el.average || 0;
                                }
                                return agg;
                            }, __produceInitMetrics())(statsSource);
                            return stats;
                        } else {
                            return __produceInitMetrics();
                        }
                    });
                } else {
                    return __produceInitMetrics();
                }
            });
    };

    const __getHealth = (id) => {
        return clusterStore
            .keys('node:*').then((nodes) => {
                if (nodes && nodes.length > 0) {
                    return clusterStore
                        .mget(nodes.map((n) => `health:${n.split(':')[1]}`))
                        .then((clusterHealth) => {
                            let healthFromNodes = clusterHealth;
                            let health = R.pipe(
                                R.map((health) => JSON.parse(health)),
                                R.reduce((agg, nodeHealthArray) => {
                                    if (nodeHealthArray)
                                        nodeHealthArray.forEach((nh) => {
                                            let metric = agg.find((m) => m.id === nh.id);
                                            if (!metric) {
                                                agg.push(nh);
                                            }
                                        });
                                    return agg;
                                }, [])
                            )(healthFromNodes);

                            if (id == 'all') {
                                return health;
                            } else {
                                return health.filter((h) => R.propEq('id', id)(h));
                            }
                        });
                } else {
                    if (id == 'all') {
                        return [];
                    } else {
                        return [{
                            message: 'Not connected',
                            error: true
                        }];
                    }
                }
            });
    };

    const __getNodes = () => {
        return clusterStore
            .keys('node:*')
            .then((nodesKeys) => {
                if (nodesKeys && nodesKeys.length > 0) {
                    return clusterStore
                        .mget(nodesKeys)
                        .then((nodes) => R.flatten(nodes || []).map((n) => JSON.parse(n)));
                } else {
                    return [];
                }
            });
    };

    return {
        gainMetrics: __gainMetrics,
        constructNodeMetrics: __constructMetrics,
        produceInitMetrics: __produceInitMetrics,
        gainHealthStats: __getHealth,
        getNodes: __getNodes
    };
};