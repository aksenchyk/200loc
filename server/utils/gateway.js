"use strict";

const async = require('async')
    , geoip = require('geoip-lite')
    , uuidV1 = require('uuid/v1')
    , R = require('ramda')
    ;

module.exports = function () {

    const propagationHeader = process.env.PROPOGATE_TRACE_HEADER || 'x-amzn-trace-id';

    const __flowRunner = (req, res, plugins) => new Promise((resolve, reject) => {
        const handlers = (plugins || []).map(plugin => plugin.handler.bind(plugin, req, res));
        async.series(handlers, (err) => err ? reject(err) : resolve());
    });

    const __constructContext = (req) => {
        let ctx = new Map();
        ctx.set('dateTime', new Date().toISOString());
        ctx.set('startTime', Date.now());
        ctx.set('trace_id', req.get(propagationHeader) || uuidV1());
        ctx.set('originalUrl', req.originalUrl);
        let geo = geoip.lookup(req.headers['x-forwarded-for'] || req.connection.remoteAddress);
        if (geo && geo.ll && geo.ll.length > 0) {
            geo.location = {
                lat: geo.ll[0],
                lon: geo.ll[1]
            };
        }
        ctx.set('geo', geo);
        return ctx;
    };

    const __isConditionSutisfied = (condition, req) => {
        let satisfied = false;
        if (!condition)
            return satisfied;

        switch (condition.source) {
            case "Header": {
                satisfied = condition.operation == 'eq' ? R.propEq(condition.key, condition.value, req.headers) : !R.propEq(condition.key, condition.value, req.headers);
                break;
            }
            case "Query": {
                satisfied = condition.operation == 'eq' ? R.propEq(condition.key, condition.value, req.query) : !R.propEq(condition.key, condition.value, req.query);
                break;
            }
            case "Context": {
                satisfied = condition.operation == 'eq' ? R.equals(condition.value, req.__ctx.get(condition.key)) : !R.equals(condition.value, req.__ctx.get(condition.key));
                break;
            }
            default:
                console.log("Policy configuration is wrong. [source]: main.flow.policy_runner");
                break;
        }
        return satisfied;
    };

    const __buildFromPolicy = (req, conditions, policy) => {
        if (Array.isArray(policy) && policy.length > 0) {
            let rule = R.find((policyRule) => {
                let sutisfiedPredicates = (policyRule.satisfy /* condition names to be sutisfied*/ || [])
                    .map((conName) => R.pipe(
                        R.find((c) => c.name === conName),
                        R.curry(__isConditionSutisfied))(conditions)
                    );
                return R.allPass(sutisfiedPredicates)(req);
            })(policy);
            return rule ? rule.proxy : {};
        } else {
            return undefined;
        }
    };

    return {
        flowRunner: __flowRunner,
        constructContext: __constructContext,
        buildFromPolicy: __buildFromPolicy
    };
};