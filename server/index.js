"use strict";
const express = require("express");
const app = module.exports = express();

const http = require("http")
  , stateManager = require("./stateManager")
  , routesTableBuilder = require("./gateway/tableBuilder")()
  , accessTableBuilder = require("./gateway/accessTableBuilder")()
  , debug = require("debug")("gateway")
  , path = require("path")
  , fs = require("fs")
  , environment = require("./environment")()
  , persistance = require("./persistance")
  , gatewayFactory = require("./gateway")
  , helmet = require("helmet")
  , chalk = require("chalk")
  , morgan = require("morgan")
  , bootstrapServices = require('./gateway/servicesBootstrap')
  , cors = require('cors')
  ;

require("dotenv").config();
environment.buildFor(app);

const logger = require("./logger"),
  loader = require("./loader")();


function __logEndExit(message, code = 0) {
  logger.log("error", message);
  process.exit(code);
}

app.update = () => {
  app.emit('updating');
  stateManager.setReadiness(false);
  return routesTableBuilder.build(app)
    .then(() => accessTableBuilder.build())
    .then(() => {
      stateManager.setReadiness(true);
      debug(`Node ${app.get("node_name")} configuration updated`);
      app.emit("updated");
    });
};

require("./mw/cloud_health")(app);

if (app.get('secured')) {
  app.enable('trust proxy');
  app.use(function (req, res, next) {
    if (req.secure) {
      next();
    } else {
      res.redirect('https://' + req.headers.host + req.url);
    }
  });
}
app.use(helmet({}));

app.start = (configPath) => {
  configPath = configPath
    ? configPath
    : path.resolve(app.get('configRoot'), "LocConfig.json");

  /*istanbul ignore next*/
  if (process.env.NODE_ENV !== "test") {
    require("./management")(app);
  }

  if (!fs.existsSync(configPath)) {
    return Promise.reject(new Error("No configuration file found"));
  }
  let config = require(configPath);
  if (!config.services || !Array.isArray(config.services) || !Array.isArray(config.plugins)) {
    return Promise.reject("Configuration has wrong format");
  }

  debug(`Gateway is starting with ${configPath}.`);
//let services = config.services;
  let plugins = config.plugins;

  // Load plugins and services, configured in LocConfig.json

  const setPlugins = Promise.all(plugins.map(pluginConfig => loader.loadComponent(pluginConfig)));
 // const runServices = Promise.all(services.map(serviceConfig => loader.loadComponent(serviceConfig)));

  return Promise.all([setPlugins/*, */])
    .then(([plugins/*, services*/]) => {
      stateManager.plugins.push(...plugins);
   //   stateManager.services.push(...services);
    })
    .then(() => bootstrapServices(app))         // bootstrap configured in LocConfig.json services
    .then(() => routesTableBuilder.build(app))        // build routing table   
    .then(() => accessTableBuilder.build())
    .then(() => {

      app.use(cors(app.get('corsOptions')));    // apply cors policy
      app.use(morgan("combined", { skip: (req) => req.method === "HEAD" || process.env.NODE_ENV === 'test' }));
      require("./mw/analytic_mw")(app);         // analytic middleware
      app.use(gatewayFactory(stateManager));    // main proxy flow
      require("./mw/error_handler")(app);       // error handling

      console.log('\n', '*************** ' + chalk.bgCyan('Starting server') + ' *********************', '\n');

      const httpServer = http.createServer(app)
        .listen(app.get("port"), () => {
          app.emit("started");
         // stateManager.runHealthChecking();
          stateManager.setReadiness(true);

          app.close = (done) => {
            app.removeAllListeners("started");
            app.removeAllListeners("loaded");
            httpServer.close(done);
          };

          process.once("SIGTERM", (msg) => {
            console.log('SIGTERM RECEIVED, exiting...', msg ? msg : '');
            app.close((err) => (err ? __logEndExit(err, -1) : process.exit(0)));
          });
          process.once("SIGINT", (msg) => {
            console.log('SIGINT RECEIVED, exiting...', msg ? msg : '');
            app.close((err) => (err ? __logEndExit(err, -1) : process.exit(0)));
          });
          debug(`Gateway node ${app.get("node_name")} started on port: ` + chalk.bgGreen(`${httpServer.address().port}`));
        });
      app.loaded = true;
      app.emit("loaded");
    });
};

if (require.main === module) {
  persistance
    .init(app.get("mongo_url"))
    .then(() => app.start())
    .catch(err => __logEndExit(err, -1));
}
