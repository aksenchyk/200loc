/**
 * @module App state 
 */
"use strict";
const axios = require('axios')
    , debug = require('debug')('gateway')
    , chalk = require('chalk')
    , EventEmitter = require('events').EventEmitter
    , R = require('ramda')
    ;

const request = R.curry(axios.create({ validateStatus: (s) => s === 200 }));
/** 
 * @typedef State
 * @type {object}
 * @property {Map} servicesStore - services metadata store.
 * @property {Array} services - services constructors to be consumed by dashboard.
 * @property {Array} plugins - plugins constructors to be consumed by dashboard.
 * @property {Object} routeTable - routing table.
 * @property {boolean} ready - ready state.
 * @property {Map} health - health state.
 * @property {Map} statsSwitcher - Wheither server should gain stats when UI command.
 * @property {Map} stats - Entry point statistic.
 * @property {EndPoint[]} endpoints - Entry point statistic.
 * 
 */

class State extends EventEmitter {
    constructor() {
        super();
        this.servicesStore = new Map();
        this.services = [];
        this.plugins = [];
        this.routeTable = [];
        this.ready = false;
        this.health = new Map();
        this.stats = new Map();
        this.statsSwitcher = new Map();
        this.endpoints = new Array();
        this.__intervals = [];
        this.accessTable = [];
        this.systemServices = new Map();
    }
    clean() {
        this.endpoints.splice(0);
        this.services.splice(0);
        this.plugins.splice(0);
        this.statsSwitcher.clear();
        this.servicesStore.clear();
        this.health.clear();
        this.stats.clear();
        this.systemServices.clear();
    }

    // runHealthChecking() {
    //     setInterval(() => {
    //         this.health.forEach((value, key) => {
    //             if (value !== 'healthy' && value !== 'disabled') {
    //                 let broken = this.endpoints.find((ep) => ep.id == key);
    //                 if (broken) {
    //                     debug(`Found broken endpoint: ${broken.name}, try to restore healthy state`);
    //                     this.__checkEndpoint(broken);
    //                 }
    //             }
    //         });
    //     }, +process.env.HEALTHCHECK_TIMEOUT || 2000);
    // }

    // __updateEndpoints() {
    //     console.log('\n', '*************** ' + chalk.bgCyan('Endpoints Health check') + ' ***************', '\n');
    //     this.health.forEach((value, key) => {
    //         if (!this.endpoints.find((ep) => ep.id == key)) {
    //             this.health.delete(key);
    //         }
    //     });
    // };

    // __startHealthCheckProcess(endpoint) {
    //     let health = endpoint.health || { active: false };
    //     let interval = health.interval || 5000;
    //     let intervalHandler;
    //     if (health.active) {
    //         debug(`Started healthchecking for ${endpoint.name}. Interval: ${interval}`);
    //         this.__checkEndpoint(endpoint)
    //             .then(() => {
    //                 intervalHandler = setInterval(() => {
    //                     this.__checkEndpoint(endpoint);
    //                 }, interval);
    //                 this.__intervals.push(intervalHandler);
    //             });
    //     } else {
    //         this.health.set(endpoint.id, 'disabled');
    //     }
    // };

    // __checkEndpoint(endpoint) {
    //     return this
    //         .doEndpointHealthCheck(endpoint)
    //         .then(() => {
    //             if (this.health.get(endpoint.id) !== 'healthy') {
    //                 console.log(chalk.bgGreen(`Endpoint ${endpoint.name} healthcheck SUCCESS`));
    //             }
    //             this.health.set(endpoint.id, 'healthy');
    //         })
    //         .catch((err) => {
    //             console.log(`Healthcheck for ${endpoint.name} ` + chalk.bgRed('FAILED') + " " + err.message);
    //             this.health.set(endpoint.id, {
    //                 status: 503,
    //                 message: `Remote target is not reachable. [Source]: gateway.main.healthcheck`
    //             });
    //         });
    // };

    // doEndpointHealthCheck(endpoint) {
    //     let pQ = [];
    //     if (endpoint.health) {
    //         (endpoint.health.healthCheck || [])
    //             .forEach((hc) => {
    //                 let p = request({
    //                     baseURL: hc.target,
    //                     url: hc.withPath,
    //                     method: hc.method || 'GET',
    //                     timeout: +hc.timeout || 1500,
    //                 });
    //                 pQ.push(p);
    //             });
    //     }
    //     return Promise.all(pQ);
    // };

    // restart() {
    //     this.__updateEndpoints();
    //     this.__intervals.forEach((i) => {
    //         clearInterval(i);
    //     });
    //     this.__intervals.splice(0, this.__intervals.length);
    //     this.endpoints.forEach((ep) => this.__startHealthCheckProcess(ep));
    // }

    setReadiness(value) {
        this.ready = value;
    }
}

const state = new State();
module.exports = Object.seal(state);