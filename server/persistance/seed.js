"use strict";
const mongoose = require('mongoose');

module.exports = function () {
    let User = mongoose.model('GatewayUser');
    return User.find()
        .then((user) => {
            if (user.length === 0) {
                let defaultUser = new User({
                    name: 'admin',
                    password: 'admin',
                    admin: true
                });
                defaultUser.save(function (err) {
                    if (err) {
                        throw err;
                    }
                    console.log('Default user created success');
                });
            }
        });
};