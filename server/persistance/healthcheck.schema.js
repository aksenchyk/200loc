"use strict";
const mongoose = require('mongoose')
    , Schema = mongoose.Schema
    ;

let entrySchemaFactory = function () {
    
    const entrySchema = new Schema({
        name: { type: String, required: false },
        interval: { type: Number, required: false },
        method: { type: String, required: false },
        withPath: { type: String, required: false },
        target: { type: String, required: false },
        active: { type: Boolean, required: false, default: true },
    });

    return mongoose.model('Healthcheck', entrySchema);
};

module.exports = entrySchemaFactory;
