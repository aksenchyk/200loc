"use strict";
const mongoose = require('mongoose')
    , Schema = mongoose.Schema
    ;

let entrySchemaFactory = function () {

    const entrySchema = new Schema({
        name: String,
        public: Boolean,
        active: { type: Boolean, default: true },
        description: String,
        entry: { type: String, default: '/' },
        methods: { type: [String], default: ['GET'] },
        plugins: [Schema.Types.Mixed],
        rules: [Schema.Types.Mixed],
        proxy: Schema.Types.Mixed,
        health: Schema.Types.Mixed,
        conditions: { type: [Schema.Types.Mixed], default: [] },
    });

    return mongoose.model('Route', entrySchema);
};

module.exports = entrySchemaFactory;
