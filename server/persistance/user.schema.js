"use strict";
const mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , crypto = require('crypto')
    ;

let userSchemaFactory = function () {
    const UserSchema = new Schema({
        name: String,
        password: String,
        admin: { type: Boolean, default: true },
        salt: { type: String },
    });
    UserSchema.pre("save", function (next) {
        this.salt = new Buffer(crypto.randomBytes(16).toString("base64"), "base64");
        this.password = this.hashPassword(this.password);
        next();
    });
    UserSchema.methods.hashPassword = function (password) {
        if (this.salt && password) {
            return crypto.pbkdf2Sync(password, this.salt, 100000, 64, 'sha512').toString("base64");
        } else {
            return password;
        }
    };
    UserSchema.methods.authenticate = function (password) {
        return this.password === this.hashPassword(password);
    };
    return mongoose.model('GatewayUser', UserSchema);
};

module.exports = userSchemaFactory;