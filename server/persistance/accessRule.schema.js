"use strict";
const mongoose = require('mongoose')
    , Schema = mongoose.Schema
    ;

let accessRuleSchemaFactory = function () {
    
    const entrySchema = new Schema({
        name: { type: String, required: false },
        expression: { type: String, required: true },
        permissions: { type: String, required: true },      
    });

    return mongoose.model('AccessRule', entrySchema);
};

module.exports = accessRuleSchemaFactory;
