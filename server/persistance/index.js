"use strict";
const mongoose = require('mongoose')
    , chalk = require('chalk')
    , debug = require('debug')('gateway')
    ;
mongoose.Promise = global.Promise;

const __init = (connectionString) => new Promise((resolve, reject) => {

    console.log('\n', '*************** ' + chalk.bgCyan('Starting Mongo Persistance') + ' ***********', '\n');
    debug('Connection string: ', `${connectionString}`);
    mongoose
        .connect(connectionString, {
            reconnectTries: Number.MAX_VALUE
        })
        .then(() => {
            require('./route.schema')();
            require('./user.schema')();
            require('./userService.schema')();
            require('./accessRule.schema')();
            return mongoose
                .model('Route')
                .count()
                .then((number) => {
                    debug('Entries found: ' + chalk.bgGreen(number));
                    return number;
                });
        })
        .then(() => require('./seed')())
        .then(resolve)
        .catch(err => {
            reject("Error connecting to MongoDB...", err);
        });
    mongoose.connection.on('connected', () => {
        debug(`${chalk.bgGreen('Connected')} to mongo on: ${connectionString}`);
    });
    mongoose.connection.on('disconnected', () => {
        debug(`${chalk.bgRed('Disconnected')} from mongo on: ${connectionString}`);
    });
});

const __getActiveEndpoints = () => {
    const Route = mongoose.model('Route');
    return Route
        .find({ active: true });
};

const __getAccessMap = () => {
    const AccessRule = mongoose.model('AccessRule');
    return AccessRule
        .find(/*{ active: true }*/);
};

const __getServices = () => {
    const CustomService = mongoose.model('CustomService');
    return CustomService
        .find();
};


module.exports = {
    init: __init,
    getActiveEndpoints: __getActiveEndpoints,
    getAccessMap: __getAccessMap,
    getServices: __getServices,
};