"use strict";
const mongoose = require('mongoose')
    , Schema = mongoose.Schema
    ;

let userServiceSchemaFactory = function () {
    
    const entrySchema = new Schema({
        name: { type: String, required: false },
        healthcheckUrl: { type: String, required: true },
        summaryUrl: { type: String, required: false },      
    });

    return mongoose.model('CustomService', entrySchema);
};

module.exports = userServiceSchemaFactory;
