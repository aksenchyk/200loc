"use strict";
const express = require('express')
    , mongoose = require('mongoose')
    , jwt = require('jsonwebtoken')
    , httpError = require('http-errors')
    ;

module.exports = function (app) {

    let router = express.Router();

    router.post('/login', function (req, res, next) {
        let User = mongoose.model('GatewayUser');
        let { username, password } = req.body;
        User
            .findOne({ name: username })
            .then((user) => {
                if (!user) {
                    return Promise.reject(httpError(404, 'User not found'));
                }
                if (!user.authenticate(password)) {
                    return Promise.reject(httpError(400, 'Bad credentials'));
                } else {
                    var token = jwt.sign(user.toJSON(), app.get('secret'), {
                        expiresIn: 86400 // expires in 24 hours
                    });
                    return { id: token, user: { username: user.name } };
                }
            })
            .then((auth) => res.json(auth))
            .catch((err) => {
                console.log(err);
                next(err);
            });
    });

    router.post('/logout', (req, res) => {
        return res.json({ ok: true });
    });

    router.post('/change', (req, res, next) => {
        let User = mongoose.model('GatewayUser');
        let { username, password, newPassword } = req.body;
        if (!!password && !!username && !!newPassword) {
            User.findOne({ name: username })
                .then((user) => {
                    if (!user) {
                        return Promise.reject(httpError(400, 'User not found'));
                    }
                    if (!user.authenticate(password)) {
                        return Promise.reject(httpError(400, 'Username or password are not walid'));
                    } else {
                        user.password = newPassword;
                        return user.save();
                    }
                })
                .then(() => res.json({ ok: true }))
                .catch((err) => next(err));
        } else {
            return next(httpError(400, 'Username, password fields required'));
        }
    });

    return router;
};