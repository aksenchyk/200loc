"use strict";
const http = require('http')
    , debug = require('debug')('gateway')
    , express = require('express')
    , bodyParser = require('body-parser')
    , chalk = require('chalk')
    , errorHandler = require('strong-error-handler')
    , path = require('path')
    ;

module.exports = function (app) {

    let managementApp = express();
    managementApp.use(bodyParser.json());
   // managementApp.disable('etag');
    managementApp.use('/api/users', require('./auth.api')(app));
    managementApp.use('/api', require('./management.api')(app));
    managementApp.get('*', express.static(path.resolve(__dirname, '../../dist')));

    managementApp.use(errorHandler({
        debug: false,
        log: false,
    }));

    const __run = () => {
        let httpServer = http.Server(managementApp);
        httpServer.listen(app.get('mport'), () => {
            debug(`Console started on port: ` + chalk.bgGreen(app.get('mport')));
            console.log('\n', '================== ' + chalk.bgGreen('GATEWAY READY') + ' ====================', '\n');
        });
    };

    app.on('started', () => __run());
};