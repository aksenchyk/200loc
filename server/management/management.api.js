"use strict";
const R = require('ramda')
    , http = require('http')
    , debug = require('debug')('gateway')
    , express = require('express')
    , stateManager = require('../stateManager')
    , killable = require('killable')
    , middlewareFactory = require('../gateway')
    , rulesFactory = require('../gateway/rules')
    , chalk = require('chalk')
    , fetch = require('node-fetch')
    , querystring = require('querystring')
    , cluster = require('../cluster')
    , httpError = require('http-errors')
    , mongoose = require('mongoose')
    , multer = require('multer')
    , stream = require('stream')
    , multerMw = multer({ storage: multer.memoryStorage() })
    , metricsUtils = require('../utils/metrics')
    , redis = require('redis')
    , EndPoint = require('../gateway/EndPoint')
    ;

module.exports = function (app) {
    let metrics = metricsUtils(cluster.store);
    let __kickClusterUpdate = () => {
        app.update()
            .then(() => cluster.updateCluster())
            .then(() => {
                debug(chalk.bgGreen('Node update success'));
            })
            .catch((err) => {
                console.log(err);
            });
    };

    let router = express.Router();

    router.get('/plugins', (req, res) => {
        return res.send((stateManager.plugins || []).map(plugin => {
            return {
                name: plugin.name,
                displayName: plugin.displayName,
                description: plugin.description,
                settingsTemplate: plugin.config,
                dependenciesTemplate: plugin.dependencies,
                value: {}
            };
        }));
    });

    /**
     * Get all installed services
     */
    router.get('/services', (req, res,next) => {
        // let services = (stateManager.services || [])
        //     .map((service) => ({
        //         name: service.name,
        //         description: service.description,
        //         settings: service.config,
        //         type: service.type
        //     }));
        // return res.json(services);
    
        debug(`Accessing services list`);
        let CustomService = mongoose.model('CustomService');
        CustomService
            .find()
            .then((result) => res.json(result))
            .catch((err) => next(err));
    });

    /**
     * Group servcies by their type for DI 
     */
    router.get('/services/getByType', (req, res) => {

        let types = R.path(["query", "filter", "where", "types"], req) || [];

        let services = R.pipe(
            R.filter((d) => R.contains(R.prop('type', d), types)),
            R.map((service) => ({
                name: service.name,
                description: service.description,
                type: service.type,
                displayName: service.displayName,
                id: service.name,
                serviceType: service.type
            }))
        )(stateManager.services || []);
        return res.json(services);
    });

    /**
   * Group servcies by their type for DI 
   */
    router.get('/services/getOne/:id', (req, res, next) => {
        let serviceId = req.params.id;
        debug(`Accessing service: [${serviceId}]`);
        let CustomService = mongoose.model('CustomService');
        CustomService
            .findById(serviceId)
            .then((result) => res.json(result))
            .catch((err) => next(err));

    });

    /**
  * Group servcies by their type for DI 
  */
    router.post('/services', (req, res, next) => {
        debug(`Creating service`, req.body);
        let CustomService = mongoose.model('CustomService');
        CustomService.create(req.body)
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

    /**
 * Group servcies by their type for DI 
 */
    router.put('/services/:id', (req, res, next) => {
        let serviceData = req.body;
        let ID = req.params.id;
        debug(`Updating service to monitoring: [${ID}:${serviceData.healthcheckUrl}: ${serviceData.summaryUrl}]`);
        let CustomService = mongoose.model('CustomService');
        CustomService.findByIdAndUpdate(ID, serviceData)
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

        /**
 * Group servcies by their type for DI 
 */
router.delete('/services/:id', (req, res, next) => {
    let serviceData = req.body;
    let ID = req.params.id;
    debug(`Updating service to monitoring: [${ID}:${serviceData.healthcheckUrl}: ${serviceData.summaryUrl}]`);
    let CustomService = mongoose.model('CustomService');
    CustomService.findByIdAndRemove(ID)
        .then((result) => res.json(result))
        .then(() => __kickClusterUpdate())
        .catch((err) => next(err));
});

    /**
     * Get service summaru info
     */
    router.get('/services/summary/:name', (req, res, next) => {
        const name = req.params.name;
        let service = stateManager.servicesStore.get(name);
        if (!!service && service.instance) {
            service.instance
                .summary()
                .then(result => {
                    return res.json(Object.assign({
                        name: service.name,
                        version: service.version
                    }, result));
                });
        } else {
            return next(httpError(404, 'Service not found'));
        }
    });

    /**
     * Test entry without restart 
     */
    router.post('/routes/test', (req, res) => {
        const app = express()
            , config = req.body.config
            , basePath = config.entry
            , rules = rulesFactory()
            , withPath = req.body.path
            ;

        const testServer = http.createServer(app).listen()
            , port = testServer.address().port;

        try {
            let target = new EndPoint(Object.assign(config, { entry: basePath, id: "test", name: 'test' }));
            rules.createSet([target]);
        } catch (error) {
            res.status(200).send({ body: JSON.stringify(error) });
        }
        const testState = {
            servicesStore: new Map(),
            services: [],
            plugins: [],
            routeTable: rules,
            ready: true,
            health: new Map()
        };
        testState.health.set('test', 'healthy');

        app.use(middlewareFactory(testState));
        killable(testServer);

        const headers = (req.body.headers || [])
            .reduce(function (acc, cur) {
                acc[cur.key] = cur.value;
                return acc;
            }, {});
        const method = req.body.method;
        const query = req.body.params || {};
        const body = req.body.body;
        // todo: const params = req.body.params
        const options = {
            headers: headers,
            method: method
        };
        if (body && method === 'POST') {
            options.body = body;
        }
        let queryString = Object.keys(query).length > 0 ? `?${querystring.stringify(query)}` : '';
        fetch(`http://localhost:${port}${basePath}${withPath}${queryString}`, options)
            .then((res) => res.text())
            .then((text) => {
                testServer.kill(() => {
                    debug(`Test server on port ${port} killed`);
                    res.status(200).send({ body: text });
                });
            })
            .catch((err) => {
                testServer.kill(() => {
                    debug(`Test server on port ${port} killed with error:`, err ? `[${err.statusCode}]: ${err.message}` : 'undefined');
                    res.status(200).send({ body: JSON.stringify(err) });
                });
            });
    });

    /**
     * Get remote service health (called in enty edit master 'health' tab)
     */
    router.post('/routes/healthcheck', (req, res) => {
        let health = req.body;
        debug(`Try to healthcheck [${(health.healthCheck || []).map((h) => `${h.target}${h.withPath}`)}]`);
        if (!health) {
            return res.status(400).end();
        }
        stateManager
            .doEndpointHealthCheck({ health: health })
            .then(() => {
                debug('................... ok');
                return res.json({ healthy: true });
            })
            .catch(() => {
                debug('................... not ok');
                return res.json({ helthy: false });
            });
    });

    /**
    * Get remote service health (called in enty edit master 'health' tab)
    */
    router.post('/accessRules', (req, res, next) => {
        let accessRule = req.body;
        console.log(accessRule);
        debug(`Adding service to monitoring: [${accessRule.target}]`);
        let AccessRule = mongoose.model('AccessRule');
        AccessRule.create(accessRule)
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

    /**
   * Get remote service health (called in enty edit master 'health' tab)
   */
    router.put('/accessRules/:id', (req, res, next) => {
        let accessRule = req.body;
        let ID = req.params.id;
        debug(`Updating service to monitoring: [${ID}:${accessRule.expression}: ${accessRule.permissions}]`);
        let AccessRule = mongoose.model('AccessRule');
        AccessRule.findByIdAndUpdate(ID, accessRule)
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

    /**
     * Get remote service health (called in enty edit master 'health' tab)
     */
    router.delete('/accessRules/:id', (req, res, next) => {
        let ruleId = req.params.id;
        debug(`Deleting service from monitoring: [${ruleId}]`);
        let AccessRule = mongoose.model('AccessRule');
        AccessRule.findByIdAndRemove(ruleId)
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

    /**
     * Get remote service health (called in enty edit master 'health' tab)
     */
    router.get('/accessRules/:id', (req, res, next) => {
        let ruleId = req.params.id;
        debug(`Accessing service: [${ruleId}]`);
        let AccessRule = mongoose.model('AccessRule');
        AccessRule.findById(ruleId)
            .then((result) => res.json(result))
            .catch((err) => next(err));
    });

    /**
    * Get remote service health (called in enty edit master 'health' tab)
    */
    router.get('/accessRules', (req, res, next) => {

        debug(`Accessing accessRules list`);
        let AccessRule = mongoose.model('AccessRule');
        let filter = req.query.filter || {};
        AccessRule
            .find()
            .limit(+(filter.limit || 10))
            .skip(+(filter.skip || 0))
            .exec()
            .then((results) => {
                let r = results.map((route) => Object.assign({}, route.toJSON(), { id: route._id }));
                return res.json(r);
            })
            .catch(err => next(err));
    });


    router.get('/routes', (req, res, next) => {
        let Route = mongoose.model('Route');
        let filter = req.query.filter;
        if (filter.where && filter.where.name) {
            filter.where.name = new RegExp(filter.where.name);
        }
        Route
            .find(filter.where)
            .limit(+filter.limit)
            .skip(+filter.skip)
            .exec((err, results) => {
                let r = results.map((route) => Object.assign({}, route.toJSON(), { id: route._id }));
                return err ? next(err) : res.json(r);
            });
    });

    router.patch('/routes', (req, res, next) => {
        let Route = mongoose.model('Route');
        let data = req.body;
        Route.findById(data._id)
            .then((route) => route ? route.update(data) : Route.create(data))
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

    router.delete('/routes/:id', (req, res, next) => {
        let Route = mongoose.model('Route');
        Route.findByIdAndRemove(req.params.id)
            .then((result) => res.json(result))
            .then(() => __kickClusterUpdate())
            .catch((err) => next(err));
    });

    router.get('/routes/count', (req, res, next) => {
        let Route = mongoose.model('Route');
        Route.count()
            .then((count) => res.json({ count: count }))
            .catch((err) => next(err));
    });

    router.get('/routes/:id', (req, res, next) => {
        let Route = mongoose.model('Route');
        let id = req.params.id;
        Route.findById(id)
            .then((result) => {
                return res.json(result);
            })
            .catch((err) => next(err));
    });

    router.post('/routes/clone/:id', (req, res, next) => {
        let id = req.params.id;
        let Route = mongoose.model('Route');
        Route.findById(id)
            .then((routeToClone) => {
                let data = Object.assign({}, routeToClone.toJSON());
                delete data._id;
                data.active = false;
                return Route.create(data);
            })
            .then((newEntry) => res.json(newEntry))
            .catch((err) => next(err));
    });

    router.get('/export', (req, res, next) => {
        let Route = mongoose.model('Route');
        let data = {};
        Route.find()
            .then((allRoutes) => {
                data.routes = allRoutes.map((r) => {
                    r = r.toJSON();
                    delete r._id;
                    return r;
                });
            })
            .then(() => {
                res.set({
                    "Content-Type": "application/zip",
                    "Content-Disposition": "attachment;filename=loc.zip"
                });
                res.send(JSON.stringify(data));
            })
            .catch((err) => next(err));
    });

    router.post('/import', (req, res, next) => {
        let routes = req.body.routes;
        const Route = mongoose.model('Route');

        if (routes && Array.isArray(routes) && routes.length > 0) {
            routes.forEach(r => {
                r.active = false;
            });
        }
        Route.insertMany(routes)
            .then((result) => {
                console.log('Routes imported successfully...', result.length);
                res.json(result);
            })
            .catch((err) => next(err));
    });

    router.post('/upload', multerMw.single('file'), (req, res, next) => {
        let buffer = req.file.buffer;
        if (!/(json)$/i.test(req.file.originalname)) {
            return next(httpError(422, `Invalid Image "${req.file.originalname}"`));
        }
        let st = new stream.PassThrough();
        st.end(buffer);
        st.on('data', (data) => {
            try {
                let config = JSON.parse(data.toString());
                if (!config.routes) {
                    return next(new httpError(422, 'Wrong format of loc microgateway configuration file'));
                } else {
                    res.json(config);
                }
            } catch (error) {
                next(error);
            }
        });
        st.on('error', next);
    });

    router.get('/stats', (req, res, next) => {
        let action = req.query.action;
        let clusterInfo = {};
        switch (action) {
            case 'info': {
                metrics
                    .gainHealthStats('all')
                    .then((result) => {
                        clusterInfo.health = result;
                    })
                    .then(() => metrics.getNodes())
                    .then((nodes) => {
                        clusterInfo.nodes = nodes;
                        res.json(clusterInfo);
                    })
                    .catch((err) => {
                        console.log(err);
                        next(err);
                    });
                break;
            }
            case 'stats': {
                let id = req.query.id;
                if (!id) {
                    return next(httpError(400, "Id is not set for entry to gain stats from"));
                }
                let publisher = redis.createClient({ url: process.env.REDIS_CONNECTION_STRING || 'redis://redis:6379' });

                publisher.publish("cluster channel", `stats:${id}`, (err) => {
                    let initial = {
                        stats: {
                            statuses: {},
                            average: 0,
                            load: []
                        }
                    };
                    if (err) {
                        console.log(err);
                        return res.json(initial);
                    }
                    publisher.quit();
                    metrics
                        .gainMetrics(id)
                        .then((result) => res.json({ stats: result }))
                        .catch((err) => {
                            console.log(err);
                            return res.json(initial);
                        });
                });
                break;
            }
            default:
                next(httpError(400, 'Unknown operation'));
                break;
        }
    });

    return router;
};
