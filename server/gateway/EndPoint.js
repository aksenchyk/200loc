"use strict";

const EventEmitter = require('events')
    , pipeBuilder = require('./pipeBuilder')()
    , UrlPattern = require('url-pattern')
    , comparatorFn = require('./routeCompare')
    , R = require('ramda')
    ;

class EndPoint extends EventEmitter {
    constructor(route) {
        super();
        this.name = route.name;
        this.health = route.health;
        this.id = route.id;
        this.proxy = route.proxy;
        this.conditions = route.conditions || [];
        this.path = route.entry.toLowerCase();
        if (!!route.policy) {
            try {
                this.policy = JSON.parse(route.policy);
            } catch (error) {
                console.log(`Wrong policy JSON for endpoint: ${this.name}`);
            }
        }
        const basePreFlow = pipeBuilder.build(route.plugins);
        this.flow = basePreFlow.flow;
        const subRouteForBase = R.curry(this.__produceSubEndpoint)(basePreFlow.pipe);
        this.subRoutes = R.pipe(R.map(subRouteForBase), R.sort(comparatorFn))(route.rules || []);
        this.__subroutesMatchers = R.map((subRoute) => {
            console.log(`${this.path}${subRoute.pattern}`.replace('//', '/'));
            console.log(`${this.path}${subRoute.pattern.replace(/reg/, "").replace(/\s/, "")}` + '(?:\\W|$)');
            return {
                subRoute: subRoute,
                matcher: new UrlPattern(new RegExp(`${subRoute.pattern.replace(/\s/, "")}`))
            };
        })(this.subRoutes);
    }

    match(originalUrl) {
        const route = R.find((m) => m.matcher.match(originalUrl), this.__subroutesMatchers);
        return route ? route.subRoute : undefined;
    }

    __produceSubEndpoint(basePipe, rule) {
        let policy = undefined;
        if (!!rule.policy) {
            try {
                policy = JSON.parse(rule.policy);
            } catch (error) {
                console.log(`Wrong policy JSON for subroute: ${rule.path}`);
            }
        }
        return {
            pattern: rule.path,
            flow: (rule.plugins && rule.plugins.length > 0) ? pipeBuilder.build(rule.plugins, basePipe).flow : [],
            proxy: rule.proxy,
            methods: rule.methods || [],
            policy: policy,
            conditions: rule.conditions
        };
    };
}

module.exports = EndPoint;