'use strict';
const trace = require('debug')('trace')
    , error = require('http-errors')
    , logger = require('../logger')
    , httpProxy = require('http-proxy')
    , R = require('ramda')
    , gatewayUtils = require('../utils/gateway')
    , Authorizer = require('../authorization')
    ;

// , chalk = require('chalk')
// , heapdump = require('heapdump')
// ;

// const memwatch = require('memwatch-next');
// memwatch.on('leak', function (info) {
//     logger.log('fatal', chalk.bgRed('Leak detected'), info);
//     if (process.env.NODE_ENV === 'development') {
//         //process.exit(-1);
//         heapdump.writeSnapshot((err, filename) => {
//             if (err) console.error(err);
//             else console.error('Wrote snapshot: ' + filename);
//         });
//     }
// });

module.exports = function __locFlow(__stateManager) {
    let stateManager = __stateManager || require('../stateManager');
    let utils = gatewayUtils();
    let authorizer = new Authorizer();

    const proxy = httpProxy.createProxyServer();

    proxy.on('proxyReq', function (proxyReq, req) {
        proxyReq.setHeader('oneopp-x-trace', req.__ctx.get('trace_id'));
    });

    return function handler(req, res, next) {
        let authConfig;
        console.log(req.originalUrl);


        const memStats = process.memoryUsage();
        req.__ctx = utils.constructContext(req);
        req.__ctx.set('memory', {
            rss: memStats.rss,
            heapTotal: memStats.heapTotal,
            heapUsed: memStats.heapUsed,
        });
        req.__ctx.set('host', req.headers.host);

        for (let i = 0; i < stateManager.accessTable.length; i++) {
            if (stateManager.accessTable[i].pattern.match(req.originalUrl)) {
                trace(stateManager.accessTable[i].name, `is authorised for  ${stateManager.accessTable[i].permissions}\n`);
                authConfig = stateManager.accessTable[i];
                break;
            }
        }
        // todo: run auth pipe with params, not concat it to the main flow

        if (stateManager.ready) {
            let target = stateManager.routeTable.match(req);
            if (target) {
                const runFlowFunc = R.curry(utils.flowRunner)(req, res);
                let health = stateManager.health.get(target.id) || { status: 503, message: `Endpoint ${target.path} is in unhealthy state` };
                trace(`[1] Heathcheck for ~${target.path} is: ${health}`);
                const childRoute = target.match(req.originalUrl);
                if (!childRoute) {
                    trace(`Entry found, but no any child routes matched ${req.originalUrl} request`);
                    res.status(404);
                    res.end();
                    return;
                }
                if (!childRoute.methods || !childRoute.methods.includes(req.method)) {
                    trace(`Shared entry cant handle ${req.method} ${req.originalUrl} request. Allowed:`, childRoute.methods);
                    res.status(404);
                    res.end();
                    return;
                }
                const basePreFlight = target.flow
                    , childPreflight = childRoute.flow;

                trace(`[2] Processing ${req.originalUrl} to target: ` + `${target.path}${childRoute.pattern}`);

                req.__ctx.set('target_id', target.id);
                req.__ctx.set('health', health);

                console.log(authConfig);
                //****************** REQUEST HANDLING *****************/        
                (authConfig
                    ? authorizer.validate(req, res, authConfig.permissions)
                    : Promise.resolve())
                    .then(() => runFlowFunc(basePreFlight))
                    .then(() => runFlowFunc(childPreflight))
                    .then(() => {
                        let proxyOptions, conditions, policy;

                        if (!!childRoute.proxy && childRoute.proxy.active) {
                            proxyOptions = childRoute.proxy;
                            conditions = childRoute.conditions;
                            policy = childRoute.policy;
                        } else {
                            proxyOptions = target.proxy;
                            conditions = target.conditions;
                            policy = target.policy;
                        }
                        // plugins can override proxy configuration defined in base or child tagrets                           
                        let overrideProxyConfig = utils.buildFromPolicy(req, conditions, policy);
                        if (overrideProxyConfig) {
                            proxyOptions = Object.assign(proxyOptions, overrideProxyConfig);
                        }
                        if (proxyOptions && proxyOptions.active) {
                            // todo: check this before save, not here
                            if (!/^(https?):\/\/([A-Z\d\.-]{2,})(:\d{2,4})?/i.test(proxyOptions.target)) {
                                return next(error(503, `Bad proxy server configuration for ${proxyOptions.target}. [Source]: gateway.main`));
                            }
                            req.__ctx.set('proxyTarget', proxyOptions.target);
                            proxy.web(req, res, proxyOptions, (err) => {
                                logger.log('error', `Can't reach remote target ${proxyOptions.target} to handle ${req.originalUrl}`, { err: err });
                                res.set('Retry-After', 5);
                                res.status(503).send(error(503, `Remote target ${proxyOptions.target} is not reachable. [Source]: gateway.main.flow`));
                                // switch circuit breaker off
                                if (health !== 'disabled') {
                                    stateManager.health.set(target.id, {
                                        status: 503,
                                        message: 'Remote target is not reachable. [Source]: gateway.main.healthcheck'
                                    });
                                }
                            });
                        } else {
                            logger.warn('Entry may be configured wrong. No result to user (404)');
                            return next(error(404, `Unexpected proxy config for entry ${req.originalUrl}`));
                        }
                    })
                    .catch((err) => {
                        logger.warn(`Error processing ${req.originalUrl}, ${err}`);
                        return next(err);
                    });
            } else {
                console.warn(`Target not found for ${req.originalUrl}`);
                res.status(404);
                res.end();
                return;
            }
        } else {
            logger.log('error', `Gateway node is not ready to process request`, { source: 'gateway.main', action: 'request_process' });
            res.set('Retry-After', 5);
            res.status(503).send(error(503, `Gateway is not ready to process request. Source: gateway.main`));
        }
    };
};
