/**
 * @module Gateway table builder
 * @author Aksenchyk Viacheslav <https://github.com/byavv>
 * @description
 * Module provides functionality to build gateway table
 * @type {Promise}
 **/

"use strict";
const comparatorFn = require('./routeCompare')
    , stateManager = require('../stateManager')
    , R = require('ramda')
    , proxyRules = require('./rules')()
    , columnify = require('columnify')
    , chalk = require('chalk')
    , EndPoint = require('./EndPoint')
    , persistance = require('../persistance')
    ;

/**
 * Bootstrap services and configure rules for api table
 * 
 * @method buildGatewayTable
 * @param   {Object}    app     Loopback application
 * @returns {Promise}           Result
 */
module.exports = function buildGatewayTable() {
    /**
     * Pring routing table when load or update
     * @param {EndPoint} endpoints 
     */
    const __showTable = (endpoints) => {
        const map = R.reduce((agg, val) => {
            const path = {
                name: endpoints[val].name,
                path: endpoints[val].path,
                healthcheck: endpoints[val].health && endpoints[val].health.active ? chalk.bgGreen("ON") : "OFF",
                subRoutes: `[${endpoints[val].subRoutes.map((r) => r.pattern)}]`
            };
            agg.push(path);
            return agg;
        }, [])(R.keys(endpoints));
        console.log(columnify(map, { columnSplitter: ' | ' }));
    };

    /**
     * Build gateway routing table
     * @param {object} express application 
     */
    const __buildTable = () => {
        return persistance
            .getActiveEndpoints()
            .then((endpoints) => R.pipe(R.sort(comparatorFn), R.map((route) => new EndPoint(route)))(endpoints))
            .then((endpoints) => {
                console.log('\n', '*************** ' + chalk.bgCyan('Routing table') + ' ************************', '\n');
                __showTable(endpoints);
                proxyRules.createSet(endpoints);
                stateManager.routeTable = proxyRules;
                stateManager.endpoints = endpoints;
               // stateManager.restart();
            });
    };

    return {
        build: __buildTable
    };
};
