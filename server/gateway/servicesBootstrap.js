/**
 * @module Services Bootstrap 
 * @author Aksenchyk Viacheslav <https://github.com/byavv>
 * @description
 * Bootstrap services
 * @type {Promise}
 **/
"use strict";
const debug = require("debug")("gateway")
    , logger = require('../logger')
    , stateManager = require('../stateManager')
    , R = require('ramda')
    , chalk = require('chalk')
    , persistance = require('../persistance')
    , CustomServiceBase = require('../../services/customServiceBase')
    , path = require('path')
    ;

const __pipePromise = R.reduce((p, fn) => p.then(fn));

const __getCheckMessage = (metadata, resultFromService) => ({
    name: metadata.name,
    error: resultFromService.error,
    message: (typeof resultFromService.message === 'string') ? resultFromService.message : JSON.stringify(resultFromService.message)
});

const __getStatusInfoFrom = (services) => R.reduce((aggr, metadata) => {
    if (metadata.instance && typeof metadata.instance.check === 'function') {
        aggr.push(__pipePromise(metadata.instance.check(), [R.curry(__getCheckMessage)(metadata)]));
    } else {
        aggr.push(Promise.resolve({
            error: true,
            name: metadata.name,
            message: `Status check method for ${metadata.name} service is not implemented`
        }));
    }
    return aggr;
}, [], services);

/**
 * Read all services from config and build instances of active ones
 * 
 * @method bootstrapServices
 * @param   {Object}    app     Loopback application
 * @returns {Promise}           Result
 */
module.exports = (app) => {

    console.log('\n', '*************** ' + chalk.bgCyan('Starting services') + ' ********************', '\n');
    const pQ = [];
    __runSystemServices(app);
    return persistance
        .getServices()
        .then((userServices) => {
            try {
                stateManager.servicesStore = R.reduce((aggr, service) => {
                    return __buildCustomUserServiceMetadata(aggr, service.toJSON());
                }, new Map())([ 
                    ...userServices // user defined services
                ]);
                for (let metadata of stateManager.servicesStore.values()) {
                    pQ.push(metadata.instance.run());
                }
                return Promise.all(pQ)
                    .then(() => console.log('\n', '*************** ' + chalk.bgCyan('Services status check') + ' ****************', '\n'))
                    .then(() => Promise.all(__getStatusInfoFrom(stateManager.servicesStore.values())))
                    .then((result) => {
                        result.forEach(r => {
                            const diagnosticMessage = `Diagnostic [${r.name}]:${r.error ? chalk.bgRed('\t[ERROR]') : chalk.bgGreen('\t[OK]')} ${r.message}`;
                            if (r.error) {
                                logger.log('error', diagnosticMessage);
                                debug(diagnosticMessage);
                            } else {
                                debug(diagnosticMessage);
                            }
                        });
                    });

            } catch (error) {
                console.error(error);
            }
        });
};

const __buildCustomUserServiceMetadata = (serviceMetadata, serviceData) => {
    let config = {};
    Object
        .keys(serviceData || {})
        .forEach(key => {
            if (/\env\{(\w+)\}$/.test(serviceData[key])) {
                let pp = serviceData[key].replace(/[env{}]/g, '');
                config[key] = process.env[pp];
            } else {
                config[key] = serviceData[key];
            }
        });
    let serviceInstance = new CustomServiceBase(config);
    serviceMetadata.set(serviceData._id, {
        name: serviceData.name,
        version: serviceData.version,
        description: serviceData.description,
        instance: serviceInstance
    });
    return serviceMetadata;
};

const __runSystemServices = (app) => {
    const servicesConfigs = require('../../LocConfig.json').services;
    let componentPath = path.resolve(process.cwd());
    servicesConfigs.forEach((configuration) => {
        let Ctr = require(path.join(componentPath, configuration.path));
        debug(`Instansiate service: ${configuration.name}`);
        let mConfig = configuration.config || {};
        let config = {};
        Object
            .keys(mConfig)
            .forEach(key => {
                if (/\env\{(\w+)\}$/.test(mConfig[key])) {
                    let pp = mConfig[key].replace(/[env{}]/g, '');
                    config[key] = process.env[pp];
                }
            });
        let serviceInstance = new Ctr(app, config);
        stateManager.systemServices.set(configuration.name, serviceInstance);
        serviceInstance.run();
    });
};