/**
 * @module Plugin Pipe builder
 * @description Provides methods for instantiating plugins with servivces, checking their health state and testing via dashboard
 */
"use strict";
const pipeFactory = require('./pipe')
    , logger = require('../../server/logger')
    , contextFactory = require('./context')
    , PluginBase = require('./pluginBase')
    , util = require('util')
    , R = require('ramda')
    , uuid = require('uuid/v1')
    ;

module.exports = function (_stateManager) {

    let stateManager = _stateManager || require('../stateManager');

    let __buildPluginInstanceWithDI = (pipe, uuid, dependencies, pluginDefinition) => {
        let Plugin = pluginDefinition.ctr;
        util.inherits(Plugin, PluginBase);
        let pluginContext = contextFactory(uuid, pipe, dependencies);
        let instance = new Plugin(pluginContext);
        instance.__postFlow = pluginDefinition.postFlow;
        instance.__preFlow = !pluginDefinition.postFlow;
        return instance;
    };

    let __createAndFillPipeWithPluginProperies = (plugins, pipe) => {
        pipe = pipe || pipeFactory();
        plugins.forEach((p) => pipe.insert(p.uuid, p.settings));
        return pipe;
    };

    let __getDependenciesForDI = (plugin) => {
        const __depRequired = Object.assign({}, plugin.dependencies);
        let deps = {};
        for (let key in __depRequired) {
            if (stateManager.servicesStore.has(__depRequired[key])) {
                deps[key] = stateManager.servicesStore.get(__depRequired[key]).instance;
            } else {
                logger.log('error', `Service dependency ${__depRequired[key]} is not defined`);
                deps[key] = null;
            }
        }
        return deps;
    };

    let __getPluginDefinition = (plugin) => {
        let pluginDefinition = stateManager.plugins.find((p) => p.name === plugin.name);
        if (!pluginDefinition) {
            throw new Error(`Plugin ${plugin.name} is not found`);
            // todo. Scenario, when user deletes plugin from config should be notified in UI
        }
        return pluginDefinition;
    };

    /**
     * Builds plugins pipe for entry, creates dependencies for all plugins,
     * applies pre-configured settings to them.
     * @param {Array}   plugins     plugins to be added into the entry flow
     */
    let build = (plugins, p) => {
        plugins.forEach((p) => p.uuid = uuid());
        try {
            const pipe = __createAndFillPipeWithPluginProperies(plugins, p);
            const buildPlugin = R.curry(__buildPluginInstanceWithDI)(pipe);
            let flow = R.reduce((aggr, plugin) => {
                let pluginInstance = R.useWith(buildPlugin(plugin.uuid), [
                    __getDependenciesForDI,
                    __getPluginDefinition
                ])(plugin)(plugin);
                aggr.push(pluginInstance);
                return aggr;
            }, [], plugins);

            return {
                flow: flow || [],
                pipe: pipe
            };
        } catch (error) {
            logger.error(error);
        }
    };

    return {
        build
    };
};
