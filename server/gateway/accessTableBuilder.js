/**
 * @module Gateway table builder
 * @author Aksenchyk Viacheslav <https://github.com/byavv>
 * @description
 * Module provides functionality to build gateway table
 * @type {Promise}
 **/

"use strict";
const stateManager = require('../stateManager')
    , R = require('ramda')
    , columnify = require('columnify')
    , chalk = require('chalk')
    , persistance = require('../persistance')
    , UrlPattern = require('url-pattern')
    ;

/**
 * Bootstrap services and configure rules for api table
 * 
 * @method buildGatewayTable
 * @param   {Object}    app     Loopback application
 * @returns {Promise}           Result
 */
module.exports = function buildGatewayTable() {
    /**
     * Pring routing table when load or update
     * @param {EndPoint} endpoints 
     */
    const __showTable = (endpoints) => {
        const map = R.reduce((agg, val) => {
            const path = {
                name: endpoints[val].name,
                expression: endpoints[val].expression,
                permissions: endpoints[val].permissions,
            };
            agg.push(path);
            return agg;
        }, [])(R.keys(endpoints));
        console.log(columnify(map, { columnSplitter: ' | ' }));
    };

    /**
     * Build gateway routing table
     * @param {object} express application 
     */
    const __buildTable = () => {
        return persistance
            .getAccessMap()
            .then((accr) => {
                return accr.map((ar) => ({
                    permissions: ar.permissions,
                    expression: ar.expression,
                    pattern: new UrlPattern(new RegExp(`${ar.expression.replace(/\s/, "")}`)),
                    name: ar.name,
                }));
            })
            .then((accessRules) => {
                console.log('\n', '*************** ' + chalk.bgCyan('Access table') + ' ************************', '\n');
                __showTable(accessRules);
                stateManager.accessTable = accessRules;
            });
    };

    return {
        build: __buildTable
    };
};
