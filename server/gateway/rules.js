"use strict";
const UrlPattern = require('url-pattern')
    , R = require('ramda')
    ;

module.exports = function () {
    let __matchers = [];
    const createSet = (rules) => {
        __matchers = R.map((rule) => {
            return {
                rule: rule,
                matcher: new UrlPattern(new RegExp('(' + rule.path + ')' + '(?:\\W|$)'))
            };
        })(rules || []);
    };

    const match = (req) => {
        const route = __matchers.find((m) => m.matcher.match(req.url));
        if (route) {
            req.url = req.url.replace(route.rule.path, '');
            return route.rule;
        } else {
            return undefined;
        }
    };
    return {
        match,
        createSet
    };
};
