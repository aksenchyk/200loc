/*jslint node: true */
'use strict';
const path = require("path"),
    fs = require("fs")
    ;

module.exports = function () {

    const __loadComponent = (config) => new Promise((resolve, reject) => {
        let componentPath = path.resolve(process.cwd(), config.path);
        if (fs.existsSync(path.resolve(`${componentPath}/package.json`))) {
            let pluginPackage = require(`${componentPath}/package.json`);
            let ctr = require(path.join(componentPath, pluginPackage.main));
            let uiConfig = pluginPackage.uiConfig || {};
            let name = config.name || pluginPackage.name;
            if (!ctr || !name) {
                reject(`Wrong config for : ${componentPath}`);
            }
            let dependencies = pluginPackage.$require;
            resolve({
                ctr: ctr, // component constructor function
                config: uiConfig, // configuration for UI form to be filled
                dependencies: dependencies || [], // component dependenciies from $required field. Plugins future. Services are injectable
                name: name, // name is the key for component. No double versuin can be loaded with the same name
                version: pluginPackage.version || '0.0.0',
                description: pluginPackage.description || '',
                type: pluginPackage.type || '',
                configuration: config.config,
                postFlow: pluginPackage.postFlow || false,
                displayName: pluginPackage.displayName
            });
        } else {
            reject(`No plugin found in provided path: ${componentPath}`);
        }
    });

    return {
        loadComponent: __loadComponent
    };
};
