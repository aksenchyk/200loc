'use strict';
/*jslint node: true */
const trace = require('debug')('trace')
    , logger = require('../../server/logger')
    , utilize = require('./utils')
    , cookieParserMw = require('cookie-parser')()
    , uuid = require('uuid/v1')
    , R = require('ramda')
    , error = require('http-errors')
    , state = require('../stateManager')
    , util = require('util')
    , cookieParser = util.promisify(cookieParserMw)
    ;
/*
 * Integration test example
 * 
 * 1. Sign up user
 * curl -H "Content-Type: application/json" -X POST -d '{"username":"xyz","password":"xyz", "email":"my@email.com", "role":"candidate"}' http://localhost:4000/v2/auth/signup?grant_type=password
 * 
 * 2. Login registered user to get token
 * curl -H "Content-Type: application/json" -X POST -d '{"password":"xyz", "email":"my@email.com"}' http://localhost:4000/v2/auth/token?grant_type=password
 * 
 *  - token provided by login call
 * yFAoqn3QHuxXihXc69J2PQ3xp4H335lExrnFE05zDj00zdquwiYTjniq3HDehy9T
 * 
 * 3. 
 * curl -H "Content-Type: application/json" -b ~/cookies.txt -c ~/cookies.txt -H "Authorization: yFAoqn3QHuxXihXc69J2PQ3xp4H335lExrnFE05zDj00zdquwiYTjniq3HDehy9T" -X POST http://localhost:3001/secured
 * 
 */
module.exports = (function () {

    const __producePayload = (auth = {}) => ({
        userId: auth.user_id || '$anonymous',
        role: auth.user_role || '$anonymous',
        scope: auth.scope || ['$anonymous'],
        authenticated: auth.authenticated || false,
        sub: uuid()
    });

    let cls = function () {
        const authService = state.systemServices.get('authentication');
        const redisService = state.systemServices.get('redis');

        const __secret = process.env.ONEOPP_SECRET || '37LvDSm4XvjYOh9Y';
        const utils = utilize(authService, redisService);
        this.validate = function (req, res, grant) {
            return new Promise((resolve, reject) => {

                const shouldCache = true;

                const token = req.get('authorization') || req.query.access_token;

                const requiredPermissions = utils.extractPermissionFromGrantString(grant);

                if (grant === '$anonymous' && !token) {
                    return utils.setSecurityPropagationToken(req, __producePayload())
                        .then(() => resolve())
                        .catch((err) => {
                            trace(`Error setting propagation token`, err);
                            return reject(error(401, 'Error setting propagation token'));
                        });
                } else {
                    if (!token) {
                        trace('Authentication token is not provided forbidden');
                        return reject(error(401, 'Authentication token is not provided. forbidden. Source: gateway.plugins.protector'));
                    }
                    if (!authService || !authService.healthy) {
                        trace('Authentication server is not responding, forbidden');
                        return reject(error(401, 'Authentication server is not healthy, forbidden. Source: gateway.plugins.protector'));
                    }
                    if (!redisService || !redisService.healthy) {
                        logger.log('warn', 'Redis service is not available, cache will be switched off');
                    }
                    /**
                     * Set session key as userId. Next time user make a request, don't require authentication service call
                     * which forces http request and database request
                     */
                    return cookieParser(req, res)
                        .then(() => utils.verify(req, '__oneopp.t.auth', __secret, grant))
                        .then((cookieData) => {
                            trace(`Cache is ${cookieData
                                ? 'valid, grant permissions'
                                : 'NOT valid, query auth server'}`, '----------', cookieData ? cookieData : '');

                            return (cookieData && cookieData.user_id && utils.validatePermissions(requiredPermissions, cookieData.scope))
                                ? utils.setSecurityPropagationToken(req, __producePayload(cookieData))
                                : authService
                                    .askpermissions(token)
                                    .then((authServerData) => {
                                        trace('Authentication server respond', authServerData);
                                        if (utils.validatePermissions(requiredPermissions, authServerData.scope)) {
                                            let auth = {
                                                "user_id": authServerData.userId,
                                                "user_role": authServerData.role, // todo: return from authentication service users permissions and cache them
                                                scope: [...authServerData.scope, '$authenticated'], // check it when incoming and do not forget to remove cooke when changing user role 
                                                sub: uuid(),
                                                authenticated: true
                                            };
                                            req.__ctx.set("authenticated", true);
                                            req.__ctx.set("role", authServerData.role);
                                            return utils
                                                .setCookie(res, '__oneopp.t.auth', auth, __secret, '10s')
                                                .then(() => shouldCache ? utils.cacheUserSession(auth) : Promise.resolve(auth));
                                        } else if (grant === '$anonymous') {
                                            req.__ctx.set("authenticated", false);
                                            req.__ctx.set("role", '$anonymous');
                                            return Promise.resolve(__producePayload());
                                        } else {
                                            req.__ctx.set("authenticated", false);
                                            req.__ctx.set("role", '$anonymous');
                                            return Promise.reject(error(401, `Access denied for ${authServerData.role}, no permissions. Source: gateway.plugins.protector`));
                                        }
                                    })
                                    .then((auth) => utils.setSecurityPropagationToken(req, __producePayload(auth)));
                        })
                        .then(() => resolve())
                        .catch((err) => {
                            if (grant === '$anonymous' && err.statusCode === 401) {
                                utils
                                    .setSecurityPropagationToken(req, __producePayload())
                                    .then(() => resolve());
                            } else {
                                trace(err);
                                return reject(error(401, `Access denied. Token is not valid or permissions cant be granted for ${grant}`));
                            }
                        });
                }
            });
        };
    };
    return cls;
})();
