"use strict";
const winston = require('winston')
    , transports = []
    ;

const customColors = {
    debug: 'cyan',
    verbose: 'gray',
    info: 'magenta',
    warn: 'yellow',
    error: 'red',
    fatal: 'red',
    errorNotification: 'red'
};

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
    const Logz = require('winston-logzio');

    transports.push(
        new Logz({
            token: 'ClQMcwesPbwjfFlexbVQAXumCUeWTwhE',
            level: 'info',
            host: 'listener.logz.io',
            type: 'ms-trace',
            trace: 'trace'
        })
    );
}

const logger = new (winston.Logger)({
    colors: customColors,
    levels: {
        fatal: 0,
        error: 1,
        warn: 2,
        info: 3,
        verbose: 4,
        debug: 5,
        errorNotification: 6
    },
    transports: [
        ...transports
    ]
});

winston.addColors(customColors);

module.exports = logger;
