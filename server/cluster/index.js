"use strict";

const EventEmitter = require('events')
    , redis = require('redis')
    , app = require('../')
    , stateManager = require('../stateManager')
    , cron = require('node-cron')
    , trace = require('debug')('trace')
    , R = require('ramda')
    , Store = require('./store')
    , logger = require('../logger')
    , debug = require('debug')('gateway')
    ;

class ClusterNode extends EventEmitter {
    constructor() {
        super();
        this._store = new Store();
        this._store.on('error', (err) => {
            logger.log('fatal', 'Private store error', { error: err });
        });
        app.on('started', () => {
            this.start();
        });
        app.on('updated', () => {
            debug('Node updated');
            this.__setHealth();
        });
    }

    start() {
        this.discoverScheduler = cron.schedule('*/15 * * * * *', () => this.__setNode());
        this.cleanScheduler = cron.schedule('* * * * *', () => this.__clean());
        this.healthScheduler = cron.schedule('*/15 * * * * *', () => this.__setHealth());

        if (process.env.REDIS_CONNECTION_STRING) {
            let redisSubscriber = redis.createClient({ url: process.env.REDIS_CONNECTION_STRING });
            redisSubscriber.on('connect', () => {
                debug(`Connected to redis on ${process.env.REDIS_CONNECTION_STRING}`);
            });
            redisSubscriber.on('error', () => {
                debug(`Redis is not reachable on ${process.env.REDIS_CONNECTION_STRING}`);
            });
            redisSubscriber.on("message", (channel, message) => {
                if (message == "update") {
                    app.update();
                }
                if (/^(update)/.test(message)) {
                    let nodeSource = message.split(':')[1].trim();
                    trace(`Updating cluster. Source: ${nodeSource}`);
                    if (nodeSource !== app.get('node_name')) {
                        app.update();
                    }
                }
                if (/^(stats)/.test(message)) {
                    let id = message.split(':')[1].trim();
                    stateManager.statsSwitcher.set(id, Date.now() + 3000);
                }
            });
            this.__setNode();
            this.discoverScheduler.start();
            this.cleanScheduler.start();
            this.healthScheduler.start();
            redisSubscriber.subscribe("cluster channel");
        }
        return this;
    }

    __setNode() {
        this._store
            .setex(`node:${app.get('node_name')}`, 30, {
                rss: `${Math.floor(process.memoryUsage().rss / 1000000)}M`,
                name: app.get('node_name')
            })
            .catch(() => { });
    };

    __clean() {
        stateManager.statsSwitcher.forEach((val, key, map) => {
            if (Date.now() - val > 0) {
                debug(`Node clean unused metrics data for entry: ${key} success`);
                map.delete(key);
            }
        });
    };

    updateCluster() {
        return new Promise((resolve, reject) => {
            let publisher = redis.createClient({ url: process.env.REDIS_CONNECTION_STRING });
            publisher.publish("cluster channel", `update:${app.get('node_name')}`, (err, res) => {
                publisher.quit();
                err ? reject(err) : resolve(res);
            });
        });
    }

    setMetrics(stats) {
        trace(`Setting metrics for ${app.get('node_name')}`);
        return this._store.setex(`stats:${app.get('node_name')}`, 60, stats);
    }

    __setHealth() {
        let health = stateManager.health;
        let aggregation = [];
        health.forEach((value, key) => {
            aggregation.push({
                id: key,
                message: R.equals(R.type(value), 'Object') ? `[${value.status}] ${value.message}` : value,
            });
        });
        return this._store.setex(`health:${app.get('node_name')}`, 300, aggregation);
    }

    get store() {
        return this._store;
    }
}

module.exports = new ClusterNode();