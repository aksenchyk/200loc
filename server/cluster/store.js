"use strict";
const debug = require('debug')('gateway')
    ;

const EventEmitter = require('events')
    , redis = require('redis')
    , chalk = require('chalk')
    ;

class Store extends EventEmitter {
    constructor() {
        super();
        this.__privateStore = new Map();
        if (process.env.REDIS_CONNECTION_STRING) {
            let client = redis.createClient({ url: process.env.REDIS_CONNECTION_STRING });
            client.on("ready", () => {
                this.redisClient = client;
            });
            client.on('error', (err) => {
                debug(chalk.underline(`Redis connection string is set, but instance is not reachable on ${process.env.REDIS_CONNECTION_STRING}`));
                this.emit('error', err);
            });
        }
    }

    get(key) {
        return new Promise((resolve, reject) => {
            if (this.redisClient && this.redisClient.connected) {
                this.redisClient.get(key, (err, result) => err ? reject(err) : resolve(JSON.parse(result)));
            } else {
                resolve(this.__privateStore.get(key));
            }
        });
    }

    set(key, value) {
        return new Promise((resolve, reject) => {
            if (this.redisClient && this.redisClient.connected) {
                this.redisClient.set(key, JSON.stringify(value), (err, result) => err ? reject(err) : resolve(result));
            } else {
                resolve(this.__privateStore.set(key, value));
            }
        });
    }

    setex(key, ttl, value) {
        return new Promise((resolve, reject) => {
            if (this.redisClient && this.redisClient.connected) {
                this.redisClient.setex(key, ttl, JSON.stringify(value), (err, result) => err ? reject(err) : resolve(result));
            } else {
                resolve(this.__privateStore.set(key, value));
            }
        });
    }

    mget(keys) {
        return new Promise((resolve, reject) => {
            if (this.redisClient && this.redisClient.connected) {
                this.redisClient.mget(keys, (err, result) => err ? reject(err) : resolve(result));
            } else {
                let result = [];
                if (Array.isArray(keys)) {
                    keys.forEach((k) => {
                        result.push(this.__privateStore.get(k));
                    });
                }
                resolve(result);
            }
        });
    }

    keys(pattern) {
        return new Promise((resolve, reject) => {
            if (this.redisClient && this.redisClient.connected) {
                this.redisClient.keys(pattern, (err, result) => err ? reject(err) : resolve(result));
            } else {
                let result = [];
                let keys = Array.from(this.__privateStore.keys());
                let reg = new RegExp(pattern);
                keys.forEach((k) => {
                    if (reg.test(k)) {
                        result.push(k);
                    }
                });
                resolve(result);
            }
        });
    }
}

module.exports = Store;