'use strict';
const program = require('commander')
    , path = require('path')
    , columnify = require('columnify')
    , chalk = require('chalk')
    , R = require('ramda')
    ;

/**
 * @module Application environment builder
 * @description Provides methods for building application enviromnent, applying configuration
 */
module.exports = function () {

    /**
     * Sets application variables based on environment and command line paramaters     
     * @param {Object}   app     express application
     */
    const build = function (app) {

        program
            .version('0.8.3-alpha.15')
            .option('-p, --port [value]', 'Select Port', (v) => +v, process.env.PORT || 3000)
            .option('-l, --log [value]', 'Specify log level', /^(debug|info|warn|error|verbose|fatal)$/i, 'info')
            .option('-m, --mport [value]', 'Specify internal management port', (v) => +v, 8080)
            .option('--secured [value]', 'Secured traffic only', false)
            .option('-e, --environment [value]', 'Define environment', /^(development|production|test|staging)$/i, 'development')
            .parse(process.argv);

        process.env.NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : program.environment;
        process.env.LOG_LEVEL = program.log;
        let nodeName = process.env.NODE_NAME = `node-${Math.random().toString(36).substring(2, 15)}`;
        process.env.PORT = program.port;
        process.env.CONFIG_PATH = program.config;

        app.set('mongo_url', process.env.MONGO_PERS_CONNECTION_STRING);
        app.set('redis_url', process.env.REDIS_CONNECTION_STRING);
        app.set('configRoot', process.cwd());
        app.set('gate_mode', program.mode);
        app.set('mport', program.mport);
        app.set('node_name', nodeName);
        app.set('port', process.env.PORT);
        app.set('secret', process.env.ONEOPP_SECRET || '37LvDSm4XvjYOh9Y');
        app.disable('x-powered-by');
        app.enable('trust proxy');
        app.set('secured', program.secured);
        app.set('propagation_header', process.env.PROPOGATE_TRACE_ID_HEADER);

        let whitelist = process.env.WHITELIST ? process.env.WHITELIST.split(',').map(p => p.trim()) : [];
        let corsOptions = {
            origin: function (origin, callback) {
                // TODO: review cors table as additional option to allow cors for endpoints
                if (R.contains('*', whitelist) || R.contains(origin, whitelist)) {
                    callback(null, true);
                } else {
                    callback(new Error('Not allowed by CORS'));
                }
            },
            optionsSuccessStatus: 204,
            credentials: true
        };        
        app.set('corsOptions', corsOptions);

        const data = {
            'version': '0.8.3-alpha.15',
            'env': chalk.bgGreen(process.env.NODE_ENV),
            'config': path.resolve(app.get('configRoot'), 'LocConfig.json'),
            'node_name': app.get('node_name'),
            'redis_url': app.get('redis_url'),
            'mongo_url': app.get('mongo_url'),
            'port': `${app.get('port')}`,
            'client_port': `${app.get('mport')}`,
            'propagate_trace': app.get('propagation_header'),
            'healthcheck_timeout': process.env.HEALTHCHECK_TIMEOUT || 2000
        };
        console.log('\n', '*************** ' + chalk.bgCyan('Build Environment') + ' *********************', '\n');
        console.log(columnify(data, { columnSplitter: ' | ' }));
    };
  
    return {
        buildFor: build
    };
};