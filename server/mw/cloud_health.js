"use strict";

module.exports = function (app) {
    app.all('/healthcheck', (req, res) => {
        res.status(200);
        return req.method === 'HEAD'
            ? res.end()
            : res.json({ message: 'ok', });
    });
};