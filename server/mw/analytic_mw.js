"use strict";
const url = require('url')
    , trace = require('debug')('trace')
    , cluster = require('../cluster')
    , R = require('ramda')
    ;

// logstash, loggly...
const logger = require('logzio-nodejs').createLogger({
    token: 'ClQMcwesPbwjfFlexbVQAXumCUeWTwhE',
    host: 'listener.logz.io',
    type: 'proxy'
});

module.exports = function (app, _stateManager) {
    let stateManager = _stateManager || require('../stateManager');
    let accountedMethods = process.env.ANALYSE_METHODS ? process.env.ANALYSE_METHODS.split(',').map(p => p.trim()) : ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];
    function __shipEventToLoggingProvider(event) {
        if (/^(production|staging)$/i.test(process.env.NODE_ENV)) {
            logger.log(event);
            return;
        }
        if (/^(development)$/i.test(process.env.NODE_ENV)) {
            trace(event);
        }
    }

    function __buildApiEvent(req, res, tts) {
        const urlParts = url.parse(req.originalUrl || req.url);

        return {
            requestMethod: req.method,
            proxyTarget: req.__ctx.get('proxyTarget'),
            originalUrl: req.__ctx.get('originalUrl'),
            uriPath: urlParts.pathname,
            datetime: req.__ctx.get('dateTime'),
            health: req.__ctx.get('health'),
            geoip: req.__ctx.get('geo'),
            "trace_id": req.__ctx.get('trace_id') || '',
            statusCode: String(res.statusCode),
            environment: process.env.NODE_ENV,
            timeToServeRequest: tts,
            remoteHost: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            host: req.__ctx.get('host'),
            userAgent: req.headers['user-agent'] || '',
            requestProtocol: req.connection.encrypted ? 'https' : 'http',
            bytesSent: req.socket.bytesWritten,
            node: app.get('node_name'),
            bytesReceived: req.socket.bytesRead ? req.socket.bytesRead : req.socket._bytesRead,
            memory: req.__ctx.get('memory') || {},
        };
    }

    const __updateLoadStats = (aggregation, event) => {
        if (!aggregation) {
            let statuses = {};
            statuses[event.statusCode] = 1;
            aggregation = [];
            let now = Date.now();
            let first = { start: now, stop: now + 1000, count: 1, tts: event.timeToServeRequest, statuses: statuses };
            aggregation.push(first);
            for (let i = 1; i <= 60; i++) {
                let start = now + i * 1000;
                aggregation.push({ start: start, stop: start + 1000, count: 0, tts: event.timeToServeRequest, statuses: statuses });
            }
        } else {
            let point = aggregation.find((p) => {
                return Date.now() >= p.start && Date.now() <= p.stop;
            });
            if (point) {
                point.count++;
                point.tts = (point.tts + event.timeToServeRequest) / 2;
                point.statuses[event.statusCode] = point.statuses[event.statusCode] ? point.statuses[event.statusCode] + 1 : 1;
            } else {
                let status = {};
                status[event.statusCode] = 1;
                aggregation.push({
                    start: Date.now(),
                    count: 1,
                    stop: Date.now() + 1000,
                    tts: event.timeToServeRequest,
                    statuses: status
                });
                aggregation.shift();
            }
        }
        return aggregation;
    };

    app.use((req, res, next) => {
        if (R.contains(req.method, accountedMethods)) {
            res.on('finish', () => {
                let now = Date.now();
                let duration = now - req.__ctx.get('startTime');
                let targetId = req.__ctx.get('target_id');
                let event = __buildApiEvent(req, res, duration);
                let switcherEndTime = stateManager.statsSwitcher.get(targetId);
                if (switcherEndTime && switcherEndTime - now > 0) {
                    let current = stateManager.stats.get(targetId) || {};
                    let stats = {
                        load: __updateLoadStats(current.load, event),
                        id: targetId
                    };
                    stateManager.stats.set(targetId, stats);
                    cluster.setMetrics(stats)
                        .catch((err) => {
                            logger.log('error', 'Set metrics fail', { err: err });
                        });
                }
                __shipEventToLoggingProvider(event);
            });
        }
        next();
    });
};
