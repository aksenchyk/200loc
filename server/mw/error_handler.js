"use strict";
const trace = require('debug')('trace')    
    , errorHandler = require('strong-error-handler')
    ;

module.exports = function (app) {
    /**
     * Custom error handlers for logging via app logger
     */
    app.use((err, req, res, next) => {      
        trace(err.message);
        next(err);
    });
    app.use(errorHandler({
        debug: false,
        log: false,
    }));
};
