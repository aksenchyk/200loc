"use strict";
const UrlPattern = require('url-pattern')
    , error = require('http-errors')
    , crypto = require('crypto')
    ;

module.exports = (function () {
    const SECRET = process.env.GATEWAY_AUTH_SECRET || "testingsecretword";    

    const cls = function (ctx) {
        const pattern = ctx('$get:pattern')
            , matcher = new UrlPattern(pattern)
            , clientId = ctx('$get:clientID')
            ;

        const __checkClient = (text) => {
            var decipher = crypto.createDecipher('aes-256-ctr', SECRET);
            var dec = decipher.update(text, 'hex', 'utf8');
            dec += decipher.final('utf8');
            return clientId === dec;
        };
        /**
         * Plugin for debug perposes, fetchesd back what it gets
         */
        this.handler = function (req, res, next) {
            if ((pattern == '*' || matcher.match(req.originalUrl))) {
                if (__checkClient(req.get('x-gate-auth') || req.query.x_gate_auth)) {
                    return next();
                } else {
                    return next(error(401, 'Gateway protection token mismatch, forbidden. Source: gateway.plugins.gateway-auth'));
                }
            } else {
                return next(error(401, 'Gateway protection banned request uri, forbidden. Source: gateway.plugins.gateway-auth'));
            }
        };
    };
    return cls;
})();
