"use strict";
const R = require('ramda')
    , jwt = require('jsonwebtoken')
    , trace = require('debug')('trace')
    ;

module.exports = function (authService, redisService) {

    let __pipePromise = R.reduce((p, fn) => p.then(fn));

    let __assignCookie = (res, name, jwtToken) => {
        if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production') {
            res.cookie(name, jwtToken, { domain: '.oneopp.com', secure: true, httpOnly: true, expire: 0 });
        } else {
            res.cookie(name, jwtToken, { domain: 'localhost', httpOnly: true, expire: 0 });
        }
    };

    let __isNotAuthenticated = (v) => !R.equals('$authenticated', v);
    let __isNotAnonimus = (v) => !R.equals('$anonymous', v);
    let __cleanPermissions = R.filter(R.allPass([__isNotAuthenticated, __isNotAnonimus]));

    let __generateToken = (payload, expires) => new Promise((resolve, reject) => jwt.sign(payload, process.env.ONEOPP_SECRET || '37LvDSm4XvjYOh9Y', { expiresIn: expires }, (err, token) => err ? reject(err) : resolve(token)));
    let setCookie = (res, name, val, secret, expires = '30m') => __pipePromise(__generateToken(val, expires), [R.curry(__assignCookie)(res, name)]);
    let setSecurityPropagationToken = (req, payload) => __generateToken(payload, '1m').then((token) => req.headers['x-opp-identity'] = `Bearer ${token}`);
    let extractPermissionFromGrantString = R.pipe((grant) => grant ? grant.split(',').map(p => p.trim()) : [], __cleanPermissions);

    let __validatePermissions = (grant, permissions) => {
        let containsRole = R.curry(R.intersection(permissions));
        let extractedRoles = extractPermissionFromGrantString(grant);
        return R.equals(extractedRoles, containsRole(extractedRoles));
    };
    let __getFromCache = (key) => new Promise((resolve) => {
        if (!redisService || !redisService.healthy || !redisService.client) {
            return resolve();
        } else {
            redisService.client.get(`auth-${key}`, (err, reply) => {
                trace(`Got result from redis for user ${key}`);
                return resolve(JSON.parse(reply.toString()));
            });
        }
    });

    let cacheUserSession = (authData) => new Promise((resolve) => {
        if (!redisService || !redisService.healthy || !redisService.client) {
            return resolve(authData);
        } else {
            redisService.client.set(`auth-${authData.user_id}`, JSON.stringify(authData), (err, responce) => {
                trace(`Redis set ${authData.user_id} responce:`, responce);
                return resolve(authData);
            });
        }
    });

    let verify = (req, name, secret, grant) => new Promise((resolve) => {
        if (!(req.cookies && req.cookies[name])) {
            return resolve();
        }
        jwt.verify(req.cookies[name], process.env.ONEOPP_SECRET || '37LvDSm4XvjYOh9Y', (err, decoded) => {
            if (!err && decoded && decoded.user_id) {
                __getFromCache(decoded.user_id)
                    .then((cached) => (cached && cached.sub === decoded.sub && __validatePermissions(grant, cached.scope)) ? resolve(cached) : resolve())
                    .catch(() => resolve());
            } else {
                return resolve();
            }
        });
    });

    /**
    * Check if all AND permissions fit
    * @param {Array<string>} perm Permissions array. E.g.  ['candidate+paid', 'recruiter']
    * @param {Array<string>} scope available permissions
    */
    const __mustBeIn = (perm, scope) => {      
        let mustBeIn = perm.map((permissionString) => {
            let and = permissionString
                .split('+')
                .map((p) => p.trim())
                .map((a) => R.contains(a, scope));
            return and.some((v) => !v);
        });
        return mustBeIn.some((v) => !v);
    };
    /**
     * Check if all OR permissions fit
     * @param {Array<string>} perm Permissions array. E.g.  ['candidate+paid', 'recruiter']
     * @param {Array<string>} scope available permissions
     */
    const __mayBeIn = (perm, scope) => R.gt(R.intersection(scope, perm).length, 0);

    /**
     * Validate user permissions
     * @param {Array<string>} perm Permissions array. E.g.  ['candidate+paid', 'recruiter']
     * @param {Array<string>} scope available permissions
     */
    const validatePermissions = (perm, scope) => R.or(perm.length === 0, __mustBeIn(perm, scope), __mayBeIn(perm, scope));

    return {
        setCookie,
        setSecurityPropagationToken,
        cacheUserSession,
        verify,
        extractPermissionFromGrantString,
        validatePermissions
    };
};