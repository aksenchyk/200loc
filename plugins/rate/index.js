"use strict";
const RateLimit = require('express-rate-limit')
    , RedisStore = require('rate-limit-redis')
    , uuid = require('uuid').v1
    ;
;

module.exports = (function () {
    let cls = function (ctx) {
        const max = ctx('$get:max');
        const delayMs = ctx('$get:delayMs');
        const windowMs = ctx('$get:windowMs');
        const prefix = ctx('$get:prefix') || uuid();
        const redisService = ctx('$inject:redis');
        let limiterMw;
        if (redisService) {
            limiterMw = new RateLimit({
                max: +max, // limit each IP to 100 requests per windowMs
                delayMs: delayMs, // disable delaying - full speed until the max limit is reached
                store: new RedisStore({
                    expiry: +windowMs,
                    prefix: prefix,
                    client: redisService.client
                })
            });
        } else {
            limiterMw = new RateLimit({
                max: +max, // limit each IP to 100 requests per windowMs
                delayMs: delayMs, // disable delaying - full speed until the max limit is reached
                windowMs: windowMs * 1000
            });
        }

        this.handler = limiterMw;
    };
    return cls;
})();
