"use strict";


module.exports = (function () {
    let cls = function (ctx) {
        let responceString = ctx('$get:rstring');
        /**
         * Plugin for debug perposes, fetchesd back what it gets
         */
        this.handler = function (req, res) {
            if (responceString) {
                return res.send(responceString);
            }
            return res.json({ headers: req.headers, cookies: req.cookies, body: req.body, url: req.originalUrl });
        };
    };
    return cls;
})();
