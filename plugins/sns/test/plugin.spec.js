/*jslint node: true, mocha:true*/
"use strict";
const express = require('express')
    , http = require("http")
    , chai = require('chai')
    , sinon = require('sinon')
    , expect = chai.expect
    , assert = chai.assert
    , request = require('supertest')
    , Pipe = require('../../../server/gateway/pipe')
    , Context = require('../../../server/gateway/context')   
    , bodyParser = require('body-parser')
    ;

describe('SIMPLE NOTIFICATION SERVICE CLIENT PLUGIN TESTS', function () {

    let mockAmazonNotificationMessageHeaders = {
        "x-amz-sns-message-type": "Notification",
        "x-amz-sns-message-id": "22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324",
        "x-amz-sns-topic-arn": "arn:aws:sns:us-west-2:123456789012:MyTopic",
        "x-amz-sns-subscription-arn": "arn:aws:sns:us-west-2:123456789012:MyTopic:c9135db0-26c4-47ec-8998-413945fb5a96",
        "Content-Type": "text/plain; charset=UTF-8",
        "Host": "example.com",
        "Connection": "Keep-Alive",
        "User-Agent": "Amazon Simple Notification Service Agent"
    };

    let mockAmazonConfirmationHeaders = {
        "x-amz-sns-message-type": "SubscriptionConfirmation",
        "x-amz-sns-message-id": "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
        "x-amz-sns-topic-arn": "arn:aws:sns:us-west-2:123456789012:MyTopic",
        "Content-Type": "text/plain; charset=UTF-8",
        "Host": "example.com",
        "Connection": "Keep-Alive",
        "User-Agent": "Amazon Simple Notification Service Agent",
    };

    let mockAmazonNotificationMessageBody = {
        "Type": "Notification", "MessageId": "22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324",
        "TopicArn": "arn:aws:sns:us-west-2:123456789012:MyTopic", "Subject": "My First Message",
        "Message": JSON.stringify({ my: 'message' }), "Timestamp": "2012-05-02T00:54:06.655Z", "SignatureVersion": "1",
        "Signature": "EXAMPLEw6JRNwm1LFQL4ICB0bnXrdB8ClRMTQFGBqwLpGbM78tJ4etTwC5zU7O3tS6tGpey3ejedNdOJ+1fkIp9F2/LmNVKb5aFlYq+9rk9ZiPph5YlLmWsDcyC5T+Sy9/umic5S0UQc2PEtgdpVBahwNOdMW4JPwk0kAJJztnc=",
        "SigningCertURL": "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
        "UnsubscribeURL": "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:c9135db0-26c4-47ec-8998-413945fb5a96"
    };

    let mockAmazonConfirmationMessageBody = {
        "Type": "SubscriptionConfirmation",
        "MessageId": "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
        "Token": "2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
        "TopicArn": "arn:aws:sns:us-west-2:123456789012:MyTopic",
        "Message": "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
        "SubscribeURL": "http://localhost:55555/confirm",
        "Timestamp": "2012-04-26T20:45:04.751Z",
        "SignatureVersion": "1",
        "Signature": "EXAMPLEpH+DcEwjAPg8O9mY8dReBSwksfg2S7WKQcikcNKWLQjwu6A4VbeS0QHVCkhRS7fUQvi2egU3N858fiTDN6bkkOxYDVrY0Ad8L10Hs3zH81mtnPk5uvvolIC1CXGu43obcgFxeL3khZl8IKvO61GWB6jI9b5+gLPoBc1Q=",
        "SigningCertURL": "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
    };

    var httpServer, pipe, authServiceMock, plugin;
    const app = express();
    let SNSPlugin = require("../");
    var accessToken = {
        userId: "1"
    };
    let spy = new sinon.spy();
    let spyConfirm = new sinon.spy();

    let testApp = express();
    testApp.use(bodyParser.json());
    testApp.post('/index', (req, res, next) => {
        spy(req.body);
        res.json(req.body);
    });
    testApp.post('/auth', (req, res, next) => {
        expect(req.headers.authorization).to.be.ok;
        res.json(req.headers);
    });
    testApp.get('/confirm', (req, res, next) => {
        spyConfirm();
        res.status(200).end();
    });
    const testServer = http.createServer(testApp);

    pipe = new Pipe();
    pipe.insert(0, { target: 'http://localhost:55555/index', topics: '*' });
    beforeEach((done) => {
        let ctx = new Context(0, pipe, {});
        plugin = new SNSPlugin(ctx);
        app.use((req, res, next) => {
            req.app = app;
            next();
        });
        app.use(plugin.handler.bind(plugin));
        httpServer = http
            .createServer(app)
            .listen(3235, done);
    });
    beforeEach((done) => testServer.listen(55555, (err) => done(err)));

    afterEach((done) => {
        httpServer.close(() => {
            testServer.close(done);
        });
    });

    it('should pass valid sns notification message', (done) => {
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(mockAmazonNotificationMessageBody))
            .expect(200)
            .end((err, res) => {
                expect(spy.calledWith({ my: 'message' })).to.be.true;
                done(err);
            });
    });

    it('should get 204 if payload has wrong json', (done) => {
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(Object.assign({}, mockAmazonNotificationMessageBody, { Message: 'not valid' })))
            .expect(204)
            .end(done);
    });

    it('should get 204 if plugin not supported type of message', (done) => {
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(Object.assign({}, mockAmazonNotificationMessageBody, { Type: 'not valid' })))
            .expect(204)
            .end(done);
    });

    it('should be ok to confirm sns message', (done) => {
        request(app)
            .post('/sns')
            .set(mockAmazonConfirmationHeaders)
            .send(JSON.stringify(mockAmazonConfirmationMessageBody))
            .expect(200)
            .end((err, res) => {
                setTimeout(() => {
                    expect(spyConfirm.called).to.be.true;
                    done(err);
                }, 100);
            });
    });

    it('should set basic authenbtication', (done) => {
        pipe.insert(0, { target: 'http://localhost:55555/auth', topics: '*', authorization: 'username:password' });
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(mockAmazonNotificationMessageBody))
            .expect(200)
            .end((err, res) => {
                console.log(res.body);
                done(err);
            });
    });

    it('should stop execution if topic is not allowed for entry point', (done) => {
        pipe.insert(0, { target: 'http://localhost:55555/index', topics: 'secured' });
        spy.reset();
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(mockAmazonNotificationMessageBody))
            .expect(204)
            .end((err, res) => {
                expect(spy.called).to.be.false;
                done(err);
            });
    });
    it('should stop execution if topics parameter is not set', (done) => {
        pipe.insert(0, { target: 'http://localhost:55555/index', topics: undefined });
        spy.reset();
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(mockAmazonNotificationMessageBody))
            .expect(204)
            .end((err, res) => {
                expect(spy.called).to.be.false;
                done(err);
            });
    });

    it('should allow topics only with proper ending', (done) => {
        pipe.insert(0, { target: 'http://localhost:55555/index', topics: 'MyTopic, NotMyTopic' });
        spy.reset();
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(mockAmazonNotificationMessageBody))
            .expect(200)
            .end((err, res) => {
                expect(spy.called).to.be.true;
                done(err);
            });
    });

    it('should allow topics only with proper ending', (done) => {
        pipe.insert(0, { target: 'http://localhost:55555/index', topics: 'NotSuchTopic' });
        spy.reset();
        request(app)
            .post('/sns')
            .set(mockAmazonNotificationMessageHeaders)
            .send(JSON.stringify(mockAmazonNotificationMessageBody))
            .expect(204)
            .end((err, res) => {
                expect(spy.called).to.be.false;
                done(err);
            });
    });
});
