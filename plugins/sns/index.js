"use strict";
const trace = require('debug')('trace')
    , logger = require('../../server/logger')
    , uuidV1 = require('uuid/v1')
    , fetch = require('node-fetch')
    , utils = require('./utils.js')()
    ;

module.exports = (function () {
    let cls = function (ctx) {

        this.handler = function (req, res, next) {
            const topics = ctx('$get:topics');
            const targetUrl = ctx('$get:target');
            const authorization = ctx('$get:authorization');
            trace('Handle sns query', { allowed: topics });
            if (!topics) {
                trace(`Allowed topics are not set. Can't confirm anything with this params`);
                return res.status(204).end();
            }
            const topicsArr = topics.split(',').map(p => p.trim());
            let messageType = req.headers['x-amz-sns-message-type'];

            if (messageType) {
                utils.parse(req, res)
                    .then((message) => utils.validateSignature(message))
                    .then((message) => {
                        let topicArn = message.TopicArn;
                        let allowed = topicsArr.filter((topic) => topicArn.endsWith(topic));
                        if (!(topics === '*' || allowed.length > 0)) {
                            trace(`Topic ${topicArn} permission denied, configured for topics:`, allowed);
                            logger.log('error', `Topic: ${topicArn} permission denied`);
                            return res.status(204).end();
                        }
                        trace(`Topic permitted`, topicArn);
                        if (message.Type === 'SubscriptionConfirmation') {
                            let subscribeUrl = message.SubscribeURL;

                            res.status(200).end();
                            fetch(subscribeUrl)
                                .then((targetRes) => {
                                    if (targetRes.ok) {
                                        logger.log('info', `Subscription for ${message.TopicArn} confirmed success`);
                                        trace(`Confirmed sqs subscription for: ${message.TopicArn} on ${subscribeUrl}`);
                                    } else {
                                        logger.log('error', `[SNS NOTIFICATION]: Error subsctiption on ${message.TopicArn}`, res.body);
                                        trace(`Error when confirm: ${message.TopicArn}`);
                                    }
                                })
                                .catch((err) => {
                                    logger.log(`Subscription for ${message.TopicArn} confirmed success`, err.message);
                                    trace(`Fail to sumscribe ${message.TopicArn}`, err.message);
                                    return next(err);
                                });
                        } else if (message.Type === 'Notification') {
                            try {
                                let payload = JSON.parse(message.Message);
                                trace(`Pass notification message for sns to ${targetUrl}`);
                                fetch(targetUrl, {
                                    method: 'POST',
                                    body: JSON.stringify(payload),
                                    headers: Object.assign({ 'Content-Type': 'application/json' }, utils.getAuthHeader(authorization))
                                })
                                    .then((targetRes) => {
                                        if (targetRes.ok) {
                                            trace(`Notification message passed success, transfered payload:`, payload);
                                            return res.status(200).end();
                                        } else {
                                            trace(`Notification message passed, but remote server respond unexpected`, {
                                                status: targetRes.status,
                                                message: targetRes.statusText
                                            });
                                            return res.status(targetRes.status || 503).end();
                                        }
                                    })
                                    .catch((err) => {
                                        trace(`Notification message passed ERROR`, err);
                                        res.status(500).end();
                                    });
                            } catch (error) {
                                trace(`Wrong JSON`, error);
                                logger.log('error', `[SNS NOTIFICATION]: Wrong JSON in SNS payload`, error);
                                return res.status(204).end();
                            }
                        } else {
                            trace(`Not expected query type`, message.Type);
                            logger.log('error', `[SNS NOTIFICATION]: Not expected query type: .${message.Type}.`);
                            return res.status(204).end();
                        }
                    })
                    .catch((err) => {
                        return next(err);
                    });
            } else {
                trace(`Try to confirm sqs entry, but no messageType header is provided`, req.headers);
                return res.status(204).end();
            }
        };
    };
    return cls;
})();
