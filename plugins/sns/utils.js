"use strict";
const MessageValidator = require('sns-validator')
    , fetch = require('node-fetch')
    , trace = require('debug')('trace')
    , logger = require('../../server/logger')
    ;

module.exports = function () {
    const validator = new MessageValidator();

    const __parse = (req, res) => new Promise((resolve, reject) => {
        var chunks = [];
        req.on('data', (chunk) => {
            chunks.push(chunk);
        });
        req.on('end', () => {
            var message;
            try {
                message = JSON.parse(chunks.join(''));
                return resolve(message);
            } catch (e) {
                return reject(e);
            }
        });
        req.on('error', (err) => reject(err));
    });

    const __validateSns = (message) =>
        new Promise((resolve, reject) => {
            if (process.env.NODE_ENV === 'test') {
                return resolve(message);
            }
            return validator.validate(message, (err, message) => err
                ? reject(err)
                : resolve(message));
        });

    const __getAuthHeader = (auth) => {
        if (!auth) {
            return {};
        }
        let up = auth.split(":");
        if (up.length !== 2) {
            return {};
        } else {
            return {
                "Authorization": "Basic " + new Buffer(auth).toString("base64")
            };
        }
    };


    return {
        parse: __parse,
        validateSignature: __validateSns,
        getAuthHeader: __getAuthHeader
    };
};