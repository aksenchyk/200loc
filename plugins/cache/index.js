"use strict";
const httpError = require('http-errors')
    , uuid = require('uuid').v1
    , axios = require('axios')
    , debug = require('debug')('plugins:cache')
    , chalk = require('chalk')
    , R = require('ramda')
    , UrlPattern = require('url-pattern')
    , generateEtag = require('etag')
    ;

module.exports = (function () {
    let cls = function (ctx) {
        const ttl = ctx('$get:ttl')
            , prefix = ctx('$get:prefix') || `${uuid()}`
            , endpoint = ctx('$get:endpoint')
            , redisService = ctx('$inject:redis')
            , filter = ctx('$get:filter') || "*"
            , etag = ctx('$get:etag')
            , matcher = new UrlPattern(filter)
            ;

        const request = R.curry(axios.create({ baseURL: endpoint, validateStatus: (status) => (status >= 200 && status < 300) || status === 304 }).get);
        const redisClient = redisService ? redisService.client : undefined;

        const __cacheResponse = (key, value) => new Promise((resolve, reject) => {
            if (redisClient && redisClient.connected) {
                debug(chalk.bgYellow('::::::::::::::::::::::: Caching in redis with: ', key));
                redisClient.setex(key, +ttl, JSON.stringify(value), (err) => err ? reject(err) : resolve(value));
            } else {
                resolve(value);
            }
        });

        const __getCached = (key) => new Promise((resolve, reject) => {
            if (redisClient && redisClient.connected) {
                redisClient.get(key, (err, result) => {
                    if (err) {
                        reject(err);
                    }
                    if (result) {
                        let parsed = JSON.parse(result);
                        resolve({
                            data: parsed.data,
                            headers: parsed.headers
                        });
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });

        const __getFromSourceAndCacheResultIf200 = (req, headers, key) =>
            request(`${req.url}`, { headers: headers })
                .then((proxyRes) => {
                    let cachableValue = {
                        data: proxyRes.data,
                        headers: proxyRes.headers
                    };
                    if (proxyRes.status === 200
                        && (/^text\/html/.test(proxyRes.headers['content-type'])
                            || /^application\/json/.test(proxyRes.headers['content-type']))) {

                        if (etag) {
                            let etagValue = generateEtag(req.url);
                            cachableValue.headers['etag'] = etagValue;
                        }
                        return __cacheResponse(key, Object.assign({}, cachableValue, { tl: Date.now() }))
                            .then(() => cachableValue);
                    } else {
                        return cachableValue;
                    }
                });

        this.handler = (req, res, next) => {
            //If-None-Match
            if (req.method == 'GET' && (filter == '*' || matcher.match(req.originalUrl))) {
                var inm = req.headers['if-none-match'] || req.headers['If-None-Match'];
                console.log(inm);
                if (inm) {
                    const key = `${prefix}:${req.originalUrl}${etag ? inm : ''}`;
                    console.log("KEY---------------", key);
                    __getCached(key)
                        .then((result) => {
                            console.log("FROM CACHE", result);
                            if (result) {
                                console.log(`Found in cache for etag: ${inm}`);
                                res.status(304);
                                res.end();
                                return;
                            } else {
                                console.log(req.headers);
                                let headers = {
                                    "content-type": req.headers['content-type'] || "application/json",
                                    "cache-control": "max-age=0",
                                    "accept": req.headers['accept'] || "application/json",
                                    "if-none-match": req.headers['if-none-match']
                                };
                                return __getFromSourceAndCacheResultIf200(req, headers, key)
                                    .then((result) => {
                                        res.set(result.headers);
                                        return res.send(result.data);
                                    });
                            }
                        })
                        .catch(function (error) {
                            if (error.response) {
                                console.log('Error when making responce to cache source', error.response);
                                // let underlying proxy handle it
                                return next();
                            } else if (error.request) {
                                return next(httpError(503, 'Service unreachable'));
                            } else {
                                console.log(error);
                                return next(httpError(500, error.message));
                            }
                        });
                } else if ((/^text\/html?/.test(req.headers.accept) || /^application\/json/.test(req.headers.accept))) {

                    __getCached(`${prefix}:${req.originalUrl}`)
                        .then((result) => {
                            if (result) {
                                console.log('--------------- RESULT FOUND');

                                res.set(result.headers);
                                return res.send(result.data);
                            } else {
                                let headers = req.headers;
                                return __getFromSourceAndCacheResultIf200(req, { headers: headers })
                                    .then((result) => {
                                        res.set(result.headers);
                                        return res.send(result.data);
                                    });
                            }
                        })
                        .catch(function (error) {
                            if (error.response) {
                                console.log('Error when making responce to cache source', error.response);
                                // let underlying proxy hanle it
                                return next();
                            } else if (error.request) {
                                return next(httpError(503, 'Service unreachable'));
                            } else {
                                console.log(error);
                                return next(httpError(500, error.message));
                            }
                        });
                } else {
                    next();
                }
            } else {
                next();
            }
        };
    };
    return cls;
})();

