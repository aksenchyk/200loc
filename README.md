## Oneopp microgateway
Api gateway stands in front of all infrastructure and serves as bridge between external network and inner perimeter services. API Gateway 
encapsulates the internal structure of the application. Put services behind gateway layer and add functionality by means of plugins. Use dashboard for managing plugins and request middleware. Plugins bear main responsibility for request authentication/authorization, caching, logging, rate limiting and any api extentions.

## Requirements
 - Node.js > 8.*
 - Docker > 1.12
 - MongoDB > 3.*

## Basics
Api gateway works in conjunction with Management dashboard for reducing downtime in development and shipping api to clients. Api routing, circuit breakers and healthchecks are manageble via dashboard, making the process for updating the API Gateway as lightweight as possible. Dashboard provides simple tools for pre-shipped testing of api endpoints and route middleware management.

### Plugins
Plugins allows you to transmogrify visitor request and server response data. Depending on the Plugin, it could add data to the backend request headers, change body content, or send custom log or debug information at the end of each request. To your application, this looks like end-to-end HTTPS, Geo IP headers, Google Authentication, Session Aware Routing and more.

#### Authentication plugin
The API Gateway might first need to validate the request by calling an authentication service (if route security policy require it), before routing the request to a backend service. E.g., to fetch information about the jobs in a recruiters’s list, the API Gateway must first retrieve the recruiters’s profile containing that information, and then proxy request to appropriate service for retrieving the data. That is what authentication plugin is used for. By settting this plugin on entry point one can restrict access to a service api before proxying external request to it.
* **Authentication server** is separated and works independendly within or out of infrastructure. Gateway provides healthcheching and, if the authentiucation service is offline will fetch 401 on every secured route. This service as dependency is injected in Authentication plugin and allow to use it's interface for checking permissions.
* Authentication policy is archived by means of RBAC model. User have a role, role - permissions. To have access to the api user's role must have appropriate permissions. There are three main roles in ONEOPP project - `candidate`, `recruiter`, `company`. Roles permissions are manageble.
* Authentication plugin provides access for any **authenticated** user if grant param is set to `$authenticated`, and to **everyone**, if set to `$anonymous`.
* Authentication plugin may or may not use **Redis server** for session storage. If redis is not available, the **Authentication server** will be called on every request, that require permissions. The point of using redis as session cache touching the DB on every request that should be authenticated. 

---

#### Amazon simple notification plugin

A microservices‑based application is a distributed system and must use an inter‑process communication mechanism. There are two styles of inter‑process communication. One option is to use an asynchronous, messaging‑based mechanism. In ONEOPP project Amazon SnS is used for that purpose. The other style of inter‑process communication is a synchronous mechanism such as HTTP. Amazon SnS client plugin works as a bridge and provides methods for validation of sns request and proxy it to inner-perimeter microservice. 

---

#### Simple tracer plugin

Any distributed system needs central logging. But distributing adds challanges to this process. To monitor how query is managed within perimeter Simple tracer plugin is used. It adds corellation token to the request header for watching and passing througth microservices to be consumed by the central logging software (Logz.io).


## Handling Partial Failures and Services Conceipt
Services are like stateholders of remote processes. For instance, Redis as remote process is wrapped to internal client, wich holds it's state, can gain metrics and try to recover it's state if any connection error. That conceipt and plugins-based upproach (plugin can inject service as dependency) any type of system state can be handled during api call and fetched appropriate, specific to that scenario respond without downtime. For masking services fault additional plugins can be implemented for responding with default data if underlying service healthcheck fails. 

## Security
The Api gateway provides inter-perimeter security by means of basic authentication passed in custom headers. Every service working within perimeter should handle it for giving access to it's api.

## Basic Usage
```bash
# clone the repo
$ git clone https://aksenchyk@bitbucket.org/aksenchyk/oneopp-gateway.git
$ cd oneopp-gateway

# install 
$ yarn install

# run development/production
$ yarn compose:deps #runs redis, auth service and admin dashboard in local network
$ yarn dev # nodemon

# testing
$ yarn test
```
## Run within Docker container:

```bash
# build image
$ yarn dockerize
# run via docker-compose with all dependencies
$ yarn compose:dev 
```
